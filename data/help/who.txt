Syntax: site who [-raw]

Lists all currently logged on users
[-raw] presents the data in a machine readable format
Requires: UserPermission.WHO