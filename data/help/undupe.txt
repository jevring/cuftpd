Syntax: site undupe pattern

Removes all dupes matching the pattern from the dupe database.

Requires: UserPermissions.UNDUPE