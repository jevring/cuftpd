Syntax: site deluser username
See also: site suspend username

Removes the user COMPLETELY from the system.

Requires: UserPermission.DELUSER