Syntax: site gadduser group username password [ident@ip]*

Adds the user 'username' to the server in the group 'group' 
with the password 'password', and the (possible) listed hostmasks

Requires: UserPermission.ADDUSER 
OR
Requires: user is gadmin for group 'group'