Syntax: site autg username groupname

Adds user 'username' to group 'groupname'
Requires: UserPermission.GROUPS
See also: rufg, addgroup, delgroup, groupchange