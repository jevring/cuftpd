Syntax: site reqfilled number

Marks a previously filed request as filled.
The number to supply can be gotten via the 
'site requests'-command