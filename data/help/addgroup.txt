Syntax: site addgroup groupname description

Creates a group.

NOTE: Adds a group with 0 slots.
Requires: UserPermission.GROUPS