1.5
CHANGES:
- Changed to maven
- Fixed a bug where we would sometimes get orphaned connections hung on executing PASV.
- Removed the use of policy files. They have proven too cumbersome, and most people see them only as a hassle.
  You can still use them, but then you have to specify their use manually on the command line.

1.4
BUGFIXES:
- cuftpd now translates line breaks (default) when transferring in ASCII mode. See the "fast_ascii_transfer" setting in
  cuftpd.xml to revert to the old way of transferring files.
- fixed a bug where some commands (site leechers, allotments, chown, undupe, deluser, group, groupchange, groups) would
  hang the user session (but not the ftpd) when the user had insufficient privileges.
- "site change username logins N" no longer returns "false" instead of "N" when issuing this command.
- the number of logins a user was allowed to have was never checked, this has been fixed.
- changed the "restrict" permission to correctly handle wildcards
- fixed a bug where uploads that failed the CRC check would still be added to the dupelog, making them un-re-uploadable.
- included a patch from pikeplop:
    - made LIST accept a path, making it RFR959 compliant
    - fixed some nuking bugs
    - fixed some request bugs
    - likely something else that I forget.
- Added non-standard EPSV response option to support FXP over ipv6

CHANGES:
- I updated the FAQ.txt file to mention the incredible speed increase that you get from using the unlimited strength
  policy files from sun. I observed a 3-fold increase in speed when using them, compared to using the default ones.
- Automatically creates the the directory to house the command log if it doesn't already exist.
- Sped up the new translating ascii transfer a good bit, but it is still about 4 times as slow as fast_ascii_transfer.
- Changed the reply when the remote userbase is down to 430, as it is transient (4**) and account related (*3*)
- Cleaned up the on-start error messages a bit
- Changed the permission needed for "site leechers" to UserPermission.VIEWUSER 
- Removed the <name> setting from the <main> site settings, since it was not used.

FEATURES:
- Added ident@host to the "site xwho" response. If you are parsing site xwho, you have to change your parser.
  This is introduced before the command, and is 40 characters long.
- I added the "shell" and "custom" zipscript types. Now you should be able to specify the individual paths to your
  scripts and binaries, as well as create your own full zipscript in java. See cuftpd.xml for details.
- Added option "0=off" to user statistics. This turns user statistics collection and presentation off.
- Added the /ftpd/main/fast_ascii_transfer option to let the user control whether line breaks should be translated
  while transferring data in ASCII mode or not. fast_ascii_transfer is about twice as fast as normal. See cuftpd.xml
  for details.
- Added uniqueness constraints to modules, commands and sections.
- Added two new logging entries to the security log, "site full" and "too many connections for user". These will help
    administrators get statistics on the full-ness (if that's even a word) of the server.

1.3
IMPORTANT: There have been substantial changes to the format of cuftpd.xml, so you should start with a fresh one and
           transfer your changes to the new format. 

BUGFIXES:
- Added a check for null on RNTO, in case RNFR had not been executed first
- Changed the response code from 500 to 521 when trying to create a directory that exists, as per RFC959
- Fixed a bug where the speed (and time and bytes transferred) for the first race announce using the internal zipscript
    would yield a speed of 0.0KB/s. This now displays the speed properly.
- Fixed the "speed" field in the log for the RACE event when using the internal zipscript
- Added a time update in the user statistics that may or may not have been the reason behind some bugs regarding statistics not updating
- Changed the way parameters are parsed for XCRC, XSHA1 and XMD5
- Changed xferlog to quote filenames with spaces in them.
- Changed the version echo format from "Majorversion.Minorversion.Build" to Majorversion.Minorversion (Build)"
- Changed the split for the dupechecker module so now entries can be separated using by spaces and commas
- SECURITY FIX: Some of the permissions were checking the wrong directories when asking for permissions. This has now been fixed)

CHANGES:
- Completely revamped the inner working of the event system. Events now carry properties that are key-value pairs.
    This removes the dependence on things like File objects, and makes it much simpler to use in cubnc.
    The external semantics stay the same (a few added parameters, but none removed). See README.txt
- Changed the README.txt file to clarify how events can be used externally.
- Changed the sort order of racers in the internal zipscript. it was previously sorted on the number of files uploaded,
    but is now instead sorted on the number of bytes uploaded
- Added the SFV event to the event log when using the internal zipscript
- Changed the response code for IOException in cwd to 550 instead of 500
- Added lastUpdated for user statistics to the stored files. Before this data would be lost (and the statistics subsequently reset)
    each time the server was restarted
- AnonymousUserbase now throws exceptions when trying to fetch or create groups, instead of returning a dummy group
- Restructured the modules of cuftpd quite a bit (including code ownership and module configuration in cuftpd.xml)
- The following subsystems have been turnedin to modules: xferlog, dirlog, dupelog, requests, nuke, zipscript
- Changed the invocation of the xferlog to an event handler
- Made upload and download emit events even when they failed/were terminated in mid-transfer.
    This comes with the variable transfer.state = FAILURE
- Changed cuftpd.xml to accomodate the module concept of configuration. Some fields also now only take letters only
    (including '-' and '_'), such as names
+ Changed configuration of modules to be dynamic. See cuftpd.xml and data/modules/*.xml and *.xsd for information
    This was done to make configuration cleaner. While some people might not like not having the settings in the same place
    I feel that cuftpd.xml becomes much easier to use this way. It also paves way for individual reconfiguration of modules
    without restarting the server, which some people have expressed an interest in.
- Strike that, we are back to having all the configuration in cuftpd.xml. I found a little XSD trick that lets us do just that.
- Changed the internal permission system to use the full path when asking for permission, as opposed to just the path
    of the parent directory.

FEATURES:
- Added a module that will log the execution of site-commands (see cuftpd.xml for details)
- Added a "-reason" parameter to "site kick"
- Added a command log feature for debugging and auditing (and big brother-esque snooping, if one wishes). see cuftpd.xml for details
- Added wildcards to the paths in the permission system. See the example at the bottom of data/permissions.acl for more information.
    To handle backwards compatibility with older permission.acl files, all difectories that do not already contain a
    wildcard (? or *), a * will be added to the end of that directory, so as to maintain the semantics.


1.2
BUGFIXES:
- removed a section name echo when requesting group stats
- added a check if the request id exists for reqfilled and approve
- added the group parameter to site help for allotments and leechers
- changed the layout of the file used for the default site help
- fixed a couple of NullPointerExceptions
    - one when things broke during LIST
    - one when things broke during PASS
- fixed a ConcurrentModificationException in Dirlog.save()
- updated README.txt to point to the pzs-ng installation instructions
- fixed a bug where EPSV would not work if no protocol number was specified.
- fixed a possible ConcurrentModificationException in dir dirlog by supplying a bound and doing a shallow copy of list
- IMPORTANT: fixed a security hole that could allow someone with an account to access files outside the ftproot.
    This required that a) the attacker already had an account, and
                       b) that the ftpd.policy file was set to allow access outside the root
    Luckily the defaults in the policy file disallow access outside the ftproot. This has been fixed in r147
- fixed a possible NullPointerException when using "c:\" or "/" as the ftproot
- changed the pattern used for MLST, MLST, MFTM, MFF, etc to use hours 0-24 instead of 1-24
     - this fixes the "fatal error: Invalid argument to time encode" error in ftprush (that I believe they could have handled nicer)
- ftprush sends quoted strings to xcrc, which we didn't handle, but now we do. we now try both. the standard didn't
    mention any quotes.
- added some code to keep multiple threads from trying to reconnect the remote userbase and statistics at the same time
- fixed a bug that caused the security log to be overwritten when cuftpd was started.
- fixed a couple of NullPointerExceptions when cuftpd fails to connect to the remote userbase
- fixed a bug where each uploaded file got added to the dirlog as a directory, instead of as a file in a directory
- fixed a bug where progressbar directories wouldn't get deleted when using the internal zipscript
- oops, forgot to create the error log, that might be why you were getting errors...

FEATURES:
- added the ability to omit "location" in allow, deny, hide & show rules, bringing them on line with restrict and unrestrict
- added the ability to hide files and directories from users in listings (that doesn't mean that they are not still there!
    if you want to prevent users from seeing AND entering, you have to both hide AND do "deny cwd dir...")
    See permissions.acl for more details
- added the ability to set the header offset in lines.txt. This is represented as a number on line 5. (default is 8)
- added the ability to run java classes as custom site commands. see cuftpd.xml for more info.
- added a HelloWorld class as a starting point for custom site commands in java
- added some support functions for [h0D]'s port of f00-pre to cuftpd
- removed references to cu.ftpd.Logging where there shouldn't be any
- when using the internal zipscript and uploading files before the sfv, these files will now show up with the correct
    username and groupname in race participation, instead of showing up as "unknown"

CHANGES:
- Most run-time stack traces should now be dumped to the error log instead
- readdedd the ability to control which ports the remote userbase and remote userbase should be exported on.
    specify 'statistics.port' and/or 'userbase.port' in data/userbase.conf to set this. 0 indicates any port
- added error messages indicating a partial failure when connecting to remote userbases and statistics.

1.1.0
BUGFIXES:
- removed a directory from dirlog on RMD, instead of just on SITE WIPE
- fixed a glitch in RaceFormatter that would cause the race responses when using cuftpd internal zipscript to not work in a certain case
- fixed a bug where 'localhost' would automatically get added as a bouncer, and this wait for IDNT on localhost connections
- fixed SITE WIPE to work on broken symlinks
- fixed a bug where it was impossible to remove allotments from the default group
- removed allotment and leech slots for a user in a group if a user is removed from that group
- fixed a bug where it was possible to arbitrarily alter credits by adding and removing allotments.
- create a dirlog entry for a directory when it is created rather than when the first file is updated
- made site commands and (most of) their parameters case insensitive
- changed the way site who and traffic evaluate hidden users
- fixed a bug in the dummy xferlog
- changed Dirlog.deleteDir() to have a recursive option for when "site wipe" is invoked on dirs that have children
    RMD will not use this, since it will not complete if the dirs have any children.
- changed Dirlog.moveDirectory() ro recursively move all the children in th dir in the dirlog as well, instead of just the dir itself
- fixed a bug where the parameters to EPRT were not parsed
- removed a possible NullPointerException from CommandSTOR and CommandRETR
- added a (probably unecessary) socket closing if something goes wrong during Connection.enterPassiveMode() that isn't a socket closing.
- fixed a NullPointerException in "site who" that would happen if "site who" was run before a connection was authenticated
- fixed a bug where MFF would NPE if it didn't get any parameters (since r130)
- fixed a bug in the remote userbase that would look for a "data.dir" setting instead of "data.directory"
- fixed a but where using * to denote a user for various site parameters would not work when using a remote userbase
- added parameter checking for addgroup (\w+)
- fixed a bug where MFMT would NPE if it didn't get any parameters (since r130)
- fixed a bug where MDTM would NPE if it didn't get any parameters
- added input checking for a bunch of extra commands (to avoid the same errors as in MFF, MFMT and MDTM)
- cleared the set of user permissions before loading a user from disk, to prevent the user from getting permissions
    that it shouldn't have
- fixed a possible NotSerializableException in "site adduser" and "site gadduser" with remote userbases
- fixed bugs where anything that operated on more than one user of group when using a remote userbase would never actually receive the updates.
- changed logcreator to not include .raceinfo and .metadata in the dupelog
- fixed a parameter order bug in the new dupelog.
- fixed an argument number bug in "site delip"
- fixed a problem when reading the race template for the internal zipscript. apparently I had written "is0", instead of "iso"
- fixed a line length bug in the race templates for the internal zipscript

FEATURES:
- cuftpd now supports K, M, and G when setting allotment, just like site take and give
- IMPORTANT: usernames and groupnames added via the site can now only contain chars: [a-zA-Z_0-9] (\w+)
    As such, cuftpd will rely on there not being users or groups with any other characters than that. If there are, the behavior is undefined.
- if a user does not have a primary group set, "default" will be displayed instead of nothing
- added a "-raw" flag to "site who" to get machine readable information suitable for scripts and the like.
- added a "-raw" flag to "site search" to get machine readable information suitable for scripts and the like.
- revamped the statistics system to take -group, -number, -raw and -section values (as applicable)
- added "REST STREAM" as a feature. It is the same as before, since the only transfer and structure type we support is stream and file
- added "TVFS" as a feature, since it basically describes a unix filesystem, with some filename restrictions (that are not hard restrictions in any case)
- added support for MLST and MLSD for listings
- added support for XCRC, XMD5 and XSHA1 (filename only so far)
- added start and end support for XCRC, XMD5, XSHA1
- added the ability to set time using MDTM, and added a new "settime" permission for it
- added support for the MFMT command. luckily SmartFTP defautls to this rather than the hacked MDTM.
- added support for the MFF command.
- added a list of pseudo-events that are handled by the zipscript(s), and thus do not warrant their own special event handlers
- added a before/after event system for selected events. See README.txt for details
- added a optional dirscript that uses the event system. It's useful and a good example of how to write event handlers
- added javadoc:ed api documentation in the cuftpd/api folder, as well as on http://cuftpd.sourceforge.net/api
    Granted, it is not as well filled as one would hope, but at least it is an overview.
- added "site xwho", which provides a connection id, and data in a parsable format. See "site help xwho" for details.
- added a "-id=X" flag to "site kick", so that you can kick connections by numbers. See "site help kick" for details.
- added a set of default permissions for newly created users (WHO, UPTIME, TRAFFIC, SEEN, STATISTICS, "site user" on yourself, view your own group)
- added the ability to use '*' to represent all groups in "site groupchange".
- added the ability to use '*' to represent all users in "site addpermissions" and "site delpermissions".
- added the ability to use '*' to represent all users in "site give" and "site take".
- added ranking positions to the statistics output (not the raw data, that is derivable anyway)
- made "site give" and "site take" accept "*@group" as a valid target
- added two new permissions, fxpupload and fxpdownload which control of the user is allowed to send or receive data
    to/from a host that isn't the same as the one the control connection is coming from
- added logcreator.jar. It is a utility that will scan a directory and create a dirlog and dupelog from it.
    Useful for migrating sites.
- added a display of total disk space in the statline
- added automatic formatting for terrabyte sizes (TB) (not speeds though. if anybody wants this, please let me know)
- fixed a parsing bug for the new dirlog, silly me...
- added the ability to add multiple IPs at one with "site addip"
- added the ability to remove multiple IPs at one with "site delip"
- made the (optional) dirscript ignore dirnames: cdN and discN, where N is a number.
- added a FAQ entry about broken symlinks
- made "site addpermissions" and "site delpermissions" accept "*@group" as a valid target

CHANGES:
- cuftpd now quits if any of the section dirs cannot be found at startup
- stopped the userbase from trying to load directories as users
- changed the bind time of the server socket to make the code cleaner
- moved the manifests to ./mf/ and updated build.xml to reflect this
- removed the socket unduper service on port n+5. Scripts should use "site undupe" instead (the format is easily parsable)
- removed preDirCheck and preFileCheck events from the zipscript in preparation for the event system (since they didn't do anything anyway)
- added a way to remove dupefiles in O(n) (basically just remove an exact filename from the map, instead of searching through it linearly)
- added some extra input validation for data connection commands
- removed the IllegalPathException, path outside the root now resolve to the root instead (no exceptions is thrown, yay!)
- same for FileSystem.resolvePath() (i.e. it returns "/" if the path was outside the root)
- moved a lot of integral invocation to the event handling system, such as much of the dirlog and dupelog etc.
- changed a permission exception from critical to error.
- changed "site who" to exclude connections that do not have any user information yet
- changed the behavior to terminate the connection when a user supplies a faulty password, rather than waiting for new data
- removed a superflous space in the xferlog
- added "-" as a valid character in user and group names
- changed the loading of the dirlog to be called by the creator, rather than the constructor
- changed the loading of the dupelog to be called by the creator, rather than the constructor
- changed Dupelog.addDupe() to take username instead of user as a parameter, so it could be used with the log population app
- IMPORTANT: the file format for the dirlog and dupelog has been changed! This means that you MUST delete your current
    dirlog and dupelog to avoid getting strange and faulty results. However, to re-create these logs, there is now
    a logcreator.jar that you can run to make a new dirlog and dupelog out of a directory
- added some notes to the readme mentioning that cuftpd.jar needs to be in the same directory as the jar you are running
- removed "pre" as a valid event, since we don't have an internal pre-engine anyway
- "site delip" now first checks if the user has the ip added before deleting.
- gave the dirscript its own HashMap of DirlogEntries (links to the original ones in the dirlog, so it won't use up
    too much extra memory), so that lookups will be O(1) instead of O(n).
    NOTE: This will use up more memory, and if this is a problem for you, you can easily revert the dirscript to
    a version that doesn't use up the extra memory, but is slower.
- changed the date display for "site user" for someone who has never been online to "Never".    

1.0-FINAL (build 0112)
- Added this changelog (about time, too)
- altered some methods to make the Connection class easier to reuse in other projects
- fixed a problem with ABOR
- moved some file operations into the FileSystem, from the Connection (to make it more transparent)
- changed the reply to PBSZ to not just accept anything, but actually adhere to the standard in RFC2228
- fixed "site wipe" to work on nuked dirs
- removed some error printouts
- added a "total 1337" line at the beginning of listing commands, so that pftp will understand them
- actually shut down when we get a server socket error, rather than just saying it.
- fixed isUploading() and isDownloading() to check if dataConnection != null, before checking if it is closed
- renamed 'client.policy' to 'ftpd.policy' and 'server.policy' to 'userbase.policy' to avoid confusion.
- added information on how to get stronger crypto to FAQ.txt
- added support for custom site commands. See README.txt
- changed how the dupelog is saved after undupe
- changed the stream closing for all opened streams
- fixed a bunch of 'Socket closed' errors. Hopefully I have gotten them all.
- changed the way the data connection is handled to fix some other possible ABOR errors.
- added an 'add_response_code' setting to custom site commands. See cuftpd.xml for explanation.
- added 7bit display templates for those who wants them, and instructions (README.txt and FAQ.txt) on how to change.
- fixed a bug where setting leech for a user in a specific group to false wouldn't remove the leech for that user, and
  also would not even try if the group used all its leech slots.
- cleaned up some error messages for the "site change" command, and did some input sanitizing.
- took care of some numeric inoput sanitazion for other site commands as well
- made the output of "site who" a bit more understandable for transfers
- added info on how to compile cuftpd with ant in README.txt
- fixed a problem where it would hangif an upload failed before it got started
- fixed a bug where allotments were not registered as having been awarded (i.e. that user had gotten their credits)

1.0rc2
- fixed some documentation issues
- fixed directory listings using "stat -la" (they now don't show ghost files, but they also do not show the statline at the end)
- made the path echoed to the eventlog by MKD and RMD be absolute instead of root-relative
- cleaned up some input data verification and moved it to the edges
- started using setting namespaces (to make it easier to reuse code in cubnc)
- cleaned up some access to the settings object
- changed the inheritance and access methods for the settings. functionality remains the same though

