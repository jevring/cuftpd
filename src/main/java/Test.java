/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

import cu.ftpd.logging.Formatter;
import cu.ftpd.logging.ErrorLog;
import cu.settings.ConfigurationException;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.security.MessageDigest;
import java.security.Security;
import java.security.Provider;

import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import sun.misc.ClassLoaderUtil;

/**
 * My test file, for testing..
 * @author Markus Jevring <markus@jevring.net>
 * @version 2007-aug-24 : 21:08:03
 */
public class Test {
    public static void main(String[] args) {
        System.out.println(Integer.parseInt(null));
        //SetUserProperty rc = new SetUserProperty<Boolean>("captain", "suspended", false);
        
        /*
        Pattern p = Pattern.compile("/");
        Matcher m = p.matcher("/");
        System.out.println("/: " + m.matches());
        m = p.matcher("/dude");
        System.out.println("/dude: " + m.matches());
        */
/*

        System.out.println("SSLSocketFactory");
        int i = 0;
        for (String s : ((SSLSocketFactory)SSLSocketFactory.getDefault()).getDefaultCipherSuites()) {
            System.out.println("default: " + s);
            i++;
        }
        System.out.println(i + " default");
        i = 0;
        for (String s : ((SSLSocketFactory)SSLSocketFactory.getDefault()).getSupportedCipherSuites()) {
            System.out.println("supported: " + s);
            i++;
        }
        System.out.println(i + " supported");
        //System.out.println(System.getProperty("ssl.SocketFactory.rovider"));
*/
/*
        File f = new File("c:\\hello\\nonexistantfilename.file");
        try {
            System.out.println(f.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
/*
        String splitterPattern = "\\s+\"|\"\\s+|\\s+";
        String parameters = "hello \"you there\" how are \"you\"";
        String[] s = parameters.split(splitterPattern);
        for (int i = 0; i < s.length; i++) {
            String s1 = s[i];
            System.out.println(i + " = " + s1);
        }
*/
/*
        Pattern p = Pattern.compile("/mp3/.* /.* /");
        String s = "/mp3/tomte/file.mp3";
        Matcher m = p.matcher(s);
        if (m.matches()) {
            System.out.println("matches");
        } else {
            System.out.println("doesn't match");
        }
*/
/*
        Document document;
        File xml = new File("C:\\!data\\download\\!documents\\code\\Java\\cuftpd\\cuftpd\\data\\cuftpd.xml");
        if (xml.exists()) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            try {
                Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema();
                dbf.setNamespaceAware(true);
                dbf.setSchema(schema);
                Validator validator = schema.newValidator();
                try {
                    validator.validate(new StreamSource(xml));
                } catch (SAXException e) {
                    document = dbf.newDocumentBuilder().parse(xml); // do this here instead of just writing out e.getMessage(), since we want to have the line and char number as well
                    // since this has such lovely line numbers, we're going to do it like this, rather than throwing the exception all the way out.
                    System.out.println("0");
                    e.printStackTrace();
                }
                //document = dbf.newDocumentBuilder().parse(xml);

            } catch (SAXException e) {
                System.out.println("1");
                e.printStackTrace();
            } catch (IOException e) {
                System.out.println("2");
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                System.out.println("3");
                e.printStackTrace();
            }
        }
*/
/*
        Pattern xcrc = Pattern.compile("(.+?)(?:\\s(\\d+))?(?:\\s(\\d+))?");
        String[] test = {"\"573373610Cool est_Picture_Ever.jpg\"", "filename.txt", "filename.txt 123987", "filename.txt 12312 1231213", "filename is a filename 123123", "\"filename has spaces\"", "\"filename has spaces\" 123 123", "\"filename has spaces" , "\"filename has spaces 123 123"};
        for (String t : test) {
            System.out.println("Testing: " + t);
            Matcher m = xcrc.matcher(t);
            if (m.matches()) {
                for (int i = 0; i <= m.groupCount(); i++) {
                    System.out.print(i + " = '" + m.group(i) + "'");
                    if (m.group(i) == null) {
                        System.out.println(" [was null]");
                    } else {
                        System.out.println();
                    }

                }
            } else {
                System.out.println("no match");
            }
            System.out.println("----------------------------------");
        }
*/









        //URLClassLoader c = new URLClassLoader(new URL[]{});
/*
        String name = "/a/basdf/asdfa/lllkjsdf/ttt/";
        if (name.endsWith("/")) {
            name = name.substring(0, name.length() - 1);
        }
        name = name.substring(0, name.lastIndexOf("/"));
        System.out.println(name);
*/



        //final Pattern progressbarDeletePattern = Pattern.compile("\\[.*\\] - \\[.*\\] - \\[.*\\]");
//        System.out.println(progressDeletionPattern.matcher("[CU] - [ INCOMPLETE 1 of 10 files] - [CU]").matches());
/*
        File[] files = new File("/e:/data/music/").listFiles();
        long l = 0;
        long s = System.currentTimeMillis();
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            if (file.isDirectory()) {
                l += simpleSize(file);
            }
        }
        long s1 = System.currentTimeMillis();
        System.out.println("time: " + (s1 - s));
        System.out.println(Formatter.size(l));
*/
/*
        final Object o = new Object();
        double d = 0.0d;
        int l = 1;
        long s = System.nanoTime();
        for (int i = 0; i < l; i++) {
           //synchronized (o) {
               d = Math.random();
           //}
        }
        long s1 = System.nanoTime();
        System.out.println((s1 - s));
        System.out.println(d);
        s = System.currentTimeMillis();
        for (int i = 0; i < l; i++) {
           synchronized (o) {
               d = Math.random();
           }
        }
        s1 = System.currentTimeMillis();
        System.out.println((s1 - s));
        System.out.println(d);
*/
/*
        try {
            System.out.println(new File("/cuftpd/site").getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
/*
        synchronized (o) {
            try {
                Thread.sleep(15000);
                System.out.println("1");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("2");
        }
*/
/*
        try {
            ErrorLog e = new ErrorLog("/c:/test.txt");
            try {
                throw new Exception("kiss!");
            } catch (Exception e1) {
                e.reportException("test", e1);
            }
            e.shutdown();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
*/
/*
        File f = new File("c:\\ftproot");
        File f1 = new File(f, "..");
        System.out.println(f1);
        System.exit(-1);

        final XPath xPath = XPathFactory.newInstance().newXPath();
        Document document;
        try {
            File xml = new File("data/cuftpd.xml");
            File xsd = new File("data/cuftpd.xsd");
            if (xml.exists() && xsd.exists()) {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(xsd);
                dbf.setNamespaceAware(true);
                dbf.setSchema(schema);
                Validator validator = schema.newValidator();
                try {
                    validator.validate(new StreamSource(xml));
                } catch (SAXException e) {
                    document = dbf.newDocumentBuilder().parse(xml); // do this here instead of just writing out e.getMessage(), since we want to have the line and char number as well
                    // since this has such lovely line numbers, we're going to do it like this, rather than throwing the exception all the way out.
                    throw new ConfigurationException("Failed to load settings");
                }
                // if we make it down here, the xmlPath is correct
                document = dbf.newDocumentBuilder().parse(xml);
                int i = 0;
                try {
                    System.out.println("start");
                    for (; i < 100000; i++) {
                        String s = xPath.evaluate("/ftpd/ssl/mode", document).trim();
                    }
                    System.out.println("end");
                } catch (Exception e) {
                    // Note: this shouldn't happen, since we have a Schema to make sure it's all there
                    System.out.println("i = " + i);
                    e.printStackTrace();

                }
            } else {
                throw new FileNotFoundException(xml.getAbsolutePath());
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
*/
/*
        // we could change their "*" to ".+"
        Pattern p = Pattern.compile("/section/.* /.*");
        Matcher m = p.matcher("/section//file");

        if (m.matches()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                System.out.println(i + " = " + m.group(i));
            }
        } else {
            System.out.println("no match");
        }
*/
        /*

        so, "/" means just in the root and nowhere else
        "/+" means the root recursively
        "/+/" meansjust dirs in the root
        "/+/+" means dirs in the root, but no files, and recursive after that
        cwd /+/ means enter dirs in the root, but not recursively
        upload /+/ has weird semantics, becasue you can't upload a directory
        upload /+/+ means upload only in dirs one dir below the root

        affects files: delete, deleteown, wipe, upload, download, fxpup, fxpdown, rename, renameown, resume, resumeown, dupecheck, settime, all
        affects dirs:  mkdir, rmdir, cwd, list, rename, renameown, nuke, settime, all

        the things that affect dirs will have ok semantics for everything, i.e. "mkdir /+/" is the same as "mkdir /+"
        however, the things that affect files have broken semantics for things like "upload /+/"
        therefore, the permissions would have to be separated into dir-affecting and file-affecting permissions
        when putting together aliases, otherwise the semantics would be weird

        and the things that affect both files and dirs would have to be in both rules

        suggestion: only check permission for directories, i.e. if we are doing file-related things, check permission for file.getParentDir()
        how would this affect things?

        rule: mkd /+/
        execution:
        mkd /a OK
        mkd /a/b FAIL

        rule: mkd /+/+
        execution:
        mkd /a OK
        mkd /a/b OK

        rule: upload /+/
        execution:
        stor /a.rar FAIL
        stor /a/b.rar FAIL

        NO, /a/b.rar should succeed, since we only pass in directories to the permission system

        rule: upload /+/+
        stor /a.rar FAIL
        stor /a/b.rar OK

        and we can still keepit recursive by adding "/.*" to the end of each path...
        no, we don't want to do that, because then "deny /mp3" would not work

        deny upload in "/mp3"
        allow upload in "/mp3/+/"
        allow upload in "/mp3/+" <- what does this mean then? same as above? in that case we need to add a "/.*" to every pattern, in which case "/mp3" won't work, AGAIN!
        no, that should probably mean the same as "/mp3", because if it does, then it is the last "/" that counts. I could work with that

        check(upload, user, "/mp3") -> fail
        check(upload, user, "/mp3/hobos") -> succeed

         */
/*
        // WORKS:
        //Pattern p = Pattern.compile("restrict (quick)?\\s*(\\$.+?|\".+\"|\\{.+\\})\\s*(?:in (.+))?\\s*(?:between (.+) and (.+))? \\s*to (.+)");
        Pattern p = Pattern.compile("restrict (quick)?\\s*(\\$.+?|\".+\"|\\{.+\\})\\s*(?:in (\\$.+?|\".+\"|\\{.+\\}))?\\s*(?:between (.+) and (.+))? \\s*to (.+)");
        //Matcher m = p.matcher("restrict quick{user:captain, group:loosers} in \"/dummy\" between 12:00 and 18:00 to 12k up and 12k down");
        //Matcher m = p.matcher("restrict quick$users in $locations between 12:00 and 18:00 to 12k up and 12k down");
        Matcher m = p.matcher("restrict quick$users between 12:00 and 18:00 to 12k up and 12k down");
        //Matcher m = p.matcher("restrict quick$users in $locations  to 12k up and 15k down");
        //Matcher m = p.matcher("restrict quick$users in $locations  to 12k up ");

        if (m.matches()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                System.out.println(i + " = " + m.group(i));
            }
        } else {
            System.out.println("no match");
        }
        System.out.println("-------- speed ------");
        Pattern speed = Pattern.compile("(\\d+(?:k|K|m|) up)?\\s*(?:and)?\\s*(\\d+(?:k|K|m|) down)?");
        m = speed.matcher(m.group(6));
        if (m.matches()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                System.out.println(i + " = " + m.group(i));
            }
        } else {
            System.out.println("no match");
        }
*/
        /*
        for (int i = 0; i < 100; i++) {
            long s = System.nanoTime();
            try {
                Thread.sleep(37);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            long e = System.nanoTime();
            System.out.println("time: " + ((e - s)/1000000));
        }
        */
        /*
        File f = new File("c:/ftproot/");
        File f1 = new File(f, "/windows");
        try {
            System.out.println(f1.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
        /*
        File f = new File("c:/ftproot");
        File f1 = new File(f, "../windows/../windows/../ghubbe/");
        System.out.println(f1.getAbsolutePath());
        System.out.println(f1.isDirectory());
        */
        /*
        T t = new T();
        t.s = "hej";
        T b = t;
        t.s = "tomte";
        System.out.println(b.s);
        try {
            Class c = Class.forName("t");
            String s = (String)c.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
*/
        /*
        File f = new File("c:\\kiss.txt");
        f.setLastModified(234234);
        */
        /*
        String[] a = "a".split("=",2);
        System.out.println(a.length);
        */
        /*
        long l = Long.parseLong("F1B118C", 16);
        long l1 = Long.parseLong("0F1B118C", 16);
        System.out.println(l);
        System.out.println(l1);
        */
        /*
        Pattern namePattern = Pattern.compile("\\w+");
        System.out.println(namePattern.matcher("hellop").matches());
        System.out.println(namePattern.matcher("hellop::asd:\\").matches());
        */
        /*
        final Object o = new Object();
        Object o2 = o;
        synchronized (getObject()) {
            System.out.println(Thread.holdsLock(Test.o));
        }
        */
        /*
        long start = System.currentTimeMillis();
        File root = new File("/e:/data/");
        System.out.println(Formatter.size(size(new File(root, "dvdr\\The.Machinist.2004.LiMiTED.NORDiC.DD5.1.AND.DTS.PAL.DVDR-DNA"))));
        long end = System.currentTimeMillis();
        System.out.println("time: " + Formatter.duration((end - start) / 1000));
        System.out.println("time: " + (end - start) + " milliseconds");
        */
    }

    private static long size(File f) {
        long size = 0;
        System.out.println(f);

        for (File file : f.listFiles()) {
            if (file.isFile()) {
                size += file.length();
            } else {
                size += size(file);
            }
        }
        return size;
    }


    public void x2() {
/*
        ProcessBuilder pb = new ProcessBuilder("c:\\windows\\system32\\ipconfig.exe");
        try {
            Process p = pb.start();
            // first try waiting for it to end first
            BufferedReader stdout = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder sb = new StringBuilder();
            char[] cs = new char[8192];
            int len;
            while((len = stdout.read(cs)) != -1) {
                // this way we don't have to do readline, since we'll split it up in later processing anyway
                // this is the fastest way to read, plus we're using the same buffer size as the BufferedReader
                sb.append(cs,0, len);
            }
            System.out.println(sb.toString());
            p.waitFor();
*/
        /*
        BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
        */
/*
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
*/
    //System.out.println(cu.ftpd.logging.Formatter.datetime(System.currentTimeMillis()));
/*
            Thread t = new Thread() {
                public void run() {
                    int i = 0;
                    PrintWriter b;
                    try {
                        b = new PrintWriter(new OutputStreamWriter(new FileOutputStream("test.txt", true)));
                        while (i++ < 15) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            b.println("ghubbe !");
                        }
                        b.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                }

            };
            t.start();
            Thread t1 = new Thread() {
                public void run() {
                    PrintWriter a;
                    try {
                        int i = 0;
                        while (i++ < 30) {
                            a = new PrintWriter(new OutputStreamWriter(new FileOutputStream("test.txt")));
                            a.println("herro!");
                            a.println("herro!");
                            a.println("herro!");
                            a.println("herro!");
                            a.println("herro!");
                            a.println("herro!");
                            a.println("herro!");
                            a.println("herro!");
                            a.println("herro!");
                            a.close();
                            try {
                                Thread.sleep(500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            };
            t1.start();
*/

    }

    public void x() {
        //Semaphore s = new Semaphore(15, true);
        /*
        File f1 = new File("c:\\ftproot");
        File f2 = new File(args[0]);
        System.out.println("equals " + f1.equals(f2));
        System.out.println("path equals: " + f1.getAbsolutePath().equals(f2.getAbsolutePath()));
        System.out.println("paths: " + f1.getAbsolutePath() + " ; " + f2.getAbsolutePath());
        try {
            System.out.println("canonical paths: " + f1.getCanonicalPath() + " : " + f2.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

/*
        // this will give us the matching things in separate groups
        // Pattern allow = Pattern.compile("allow (quick)?\\s?(.+?|\\$.+?|\\{.+\\}) in (?:(\\$.+?)|\"(.+)\"|(\\{.+\\}))\\s?(?:by (.+))?");
        // this will give us the matching thing as one group, then we can check match.chatAt(0) == '$' eller '{' eller '"'
        String matchEntity = "(.+?|\\$.+?|\\{.+\\})";
        Pattern allow = Pattern.compile("allow (quick)?\\s?" + matchEntity + " in (\\$.+?|\".+\"|\\{.+\\})\\s?(?:by (.+))?");
        Matcher m = allow.matcher("allow quick $dir_actions in $sections by {user:tomte, group:schepe}");
        if (m.matches()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                System.out.println(i + ": " + m.group(i));
            }
        } else {
            System.out.println("no match for: allow dir_actions in $sections by admins");
        }

        m = allow.matcher("allow dir_actions in \"/mp3\" by group:admins");
        if (m.matches()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                System.out.println(i + ": " + m.group(i));
            }
        } else {
            System.out.println("no match for: allow dir_actions in \"/mp3\" by group:admins");
        }

        m = allow.matcher("allow dir_actions in {\"/mp3\", \"/tv\"} by admins");
        if (m.matches()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                System.out.println(i + ": " + m.group(i));
            }
        } else {
            System.out.println("no match for: allow dir_actions in {\"/mp3\", \"/tv\"} by admins");
        }

        m = allow.matcher("allow $dir_actions in {\"/mp3\", \"/tv\"}");
        if (m.matches()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                System.out.println(i + ": " + m.group(i));
            }
        } else {
            System.out.println("no match for: allow dir_actions in {\"/mp3\", \"/tv\"}");
        }

*/
/*
        try {
            System.out.println("default charset= " + Charset.defaultCharset().name());

//            ServerSocket ss = new ServerSocket(2000);
//            Socket s = ss.accept();
            File ascii = new File("ascii");
            File iso = new File("iso");
            Writer asciiWriter = new OutputStreamWriter(new FileOutputStream(ascii));
            Writer isoWriter = new OutputStreamWriter(new FileOutputStream(iso), "ISO-8859-1");

//            Writer out = new OutputStreamWriter(s.getOutputStream(), "ISO-8859-1");
//            out = new PrintWriter(out);
            for (int i = 0; i <= 255; i++) {
                asciiWriter.write(i);
                asciiWriter.write(10);
                asciiWriter.write(13);
            }
            asciiWriter.flush();
            asciiWriter.close();
            for (int i = 0; i <= 255; i++) {
                isoWriter.write(i);
                isoWriter.write(10);
                isoWriter.write(13);
            }
            isoWriter.flush();
            isoWriter.close();

            for (Map.Entry<String, Charset> e : Charset.availableCharsets().entrySet()) {
                System.out.println(e.getKey() + "=" + e.getValue().name());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
        /*
        try {

            FileReader in = new FileReader("data/cubnc.nfo");
            int c;
            while ((c = in.read()) != -1) {
                if (c > 32) {
                    System.out.println(c + ": " + (char)c);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

/*
        System.out.println("stream, input stream");
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\!data\\download\\!documents\\code\\Java\\cuftpd\\cuftpd\\data\\logs\\dirlog")));

            //PrintWriter out = new PrintWriter(new FileWriter("/dev/null"));
            String line;

            int count = 0;
            long start = System.currentTimeMillis();

            while ((line = in.readLine()) != null) {
                if (line.contains("Day")) {
                    count++;
                }
            }
            System.out.println("Time taken: " + (System.currentTimeMillis() - start));
            System.out.println("Found " + count + " matches");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("stream, contains");
        try {
            BufferedReader in = new BufferedReader(new FileReader("C:\\!data\\download\\!documents\\code\\Java\\cuftpd\\cuftpd\\data\\logs\\dirlog"));
            //PrintWriter out = new PrintWriter(new FileWriter("/dev/null"));
            String line;

            int count = 0;
            long start = System.currentTimeMillis();
            while ((line = in.readLine()) != null) {
                if (line.contains("Day")) {
                    count++;
                }
            }
            System.out.println("Time taken: " + (System.currentTimeMillis() - start));
            System.out.println("Found " + count + " matches");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("stream, input stream");
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("C:\\!data\\download\\!documents\\code\\Java\\cuftpd\\cuftpd\\data\\logs\\dirlog")));

            //PrintWriter out = new PrintWriter(new FileWriter("/dev/null"));
            String line;

            int count = 0;
            long start = System.currentTimeMillis();

            while ((line = in.readLine()) != null) {
                if (line.contains("Day")) {
                    System.out.println(line);
                    count++;
                }
            }
            System.out.println("Time taken: " + (System.currentTimeMillis() - start));
            System.out.println("Found " + count + " matches");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("stream, contains");
        try {
            BufferedReader in = new BufferedReader(new FileReader("C:\\!data\\download\\!documents\\code\\Java\\cuftpd\\cuftpd\\data\\logs\\dirlog"));
            //PrintWriter out = new PrintWriter(new FileWriter("/dev/null"));
            String line;

            int count = 0;
            long start = System.currentTimeMillis();
            while ((line = in.readLine()) != null) {
                if (line.contains("Day")) {
                    count++;
                }
            }
            System.out.println("Time taken: " + (System.currentTimeMillis() - start));
            System.out.println("Found " + count + " matches");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
/*
        System.out.println("RandomAccessFile, contains");
        try {
            RandomAccessFile in = new RandomAccessFile("C:\\!data\\download\\!documents\\code\\Java\\cuftpd\\cuftpd\\data\\logs\\dirlog", "r");
            String line;

            int count = 0;
            long start = System.currentTimeMillis();
            while ((line = in.readLine()) != null) {
                if (line.contains("Day")) {
                    count++;
                }
            }
            System.out.println("Time taken: " + (System.currentTimeMillis() - start));
            System.out.println("Found " + count + " matches");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("stream, matches");
        try {
            BufferedReader in = new BufferedReader(new FileReader("C:\\!data\\download\\!documents\\code\\Java\\cuftpd\\cuftpd\\data\\logs\\dirlog"));
            //PrintWriter out = new PrintWriter(new FileWriter("/dev/null"));
            String line;

            int count = 0;
            long start = System.currentTimeMillis();
            while ((line = in.readLine()) != null) {
                if (line.matches(".*Day.*")) {
                    count++;
                }
            }
            System.out.println("Time taken: " + (System.currentTimeMillis() - start));
            System.out.println("Found " + count + " matches");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
        /*
        try {
            BufferedReader in = new BufferedReader(new FileReader("/dev/null"));
            PrintWriter out = new PrintWriter(new FileWriter("/dev/null"));
            String line = "hej";
            line = in.readLine();
            if (line == null) {
                System.out.println("we got null from /dev/null");
            } else {
                System.out.println("we got this from /dev/null " + line);
            }
            out.println("something");
            System.out.println("printed stuff to /dev/null");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

    }
    /*
    public static InetAddress[] localHostAddresses()
	throws UnknownHostException {
	InetAddress localhost = InetAddress.getLocalHost();
        System.out.println(localhost);
    InetAddress addresses[] = InetAddress.getAllByName(localhost.getHostName());
	return addresses;
    }

    static List<InetAddress> localHostInterfaceAddresses()
	throws SocketException {
        LinkedList<InetAddress> list = new LinkedList<InetAddress>();

        Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
        if (interfaces == null)
            return list;

        while (interfaces.hasMoreElements()) {
            NetworkInterface card = (NetworkInterface) interfaces.nextElement();
            Enumeration addresses = card.getInetAddresses();
            if (addresses == null)
                continue;

            while (addresses.hasMoreElements()) {
                InetAddress address = (InetAddress) addresses.nextElement();
                list.add(address);
            }
        }

        return list;
    }

    public static void main2(String args[])
	throws Exception
    {
	System.out.println(Arrays.toString(localHostAddresses()));
	System.out.println(Arrays.toString(localHostInterfaceAddresses().toArray()));
    }

    static Object o = new Object();
    static Object getObject() {
        return o;
    }

    private static long simpleSize(File file) {
        File[] files = file.listFiles();
        long s = 0;
        for (File f : files) {
            if (f.isFile()) {
                s += f.length();
            }
        }
        return s;
    }
*/
}
