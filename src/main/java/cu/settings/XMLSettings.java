/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.settings;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.namespace.NamespaceContext;
import javax.xml.transform.dom.DOMSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Iterator;

/**
 * Takes an XML document and makes it available as settings via an xPath notation.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-dec-21 - 20:25:20
 * @version $Id: XMLSettings.java 258 2008-10-26 12:47:23Z jevring $
 */
public class XMLSettings implements Settings {
    protected final XPath xPath = XPathFactory.newInstance().newXPath();
    protected final Document document;

    public XMLSettings(Node node) throws ConfigurationException {
        // This MOVES the node, because Document.importNode() doesn't want to work.
        try {
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Node n = node.cloneNode(true).getChildNodes().item(1);
            document.adoptNode(n);
            document.insertBefore(n, null);
        } catch (ParserConfigurationException e) {
            throw new ConfigurationException(e.getMessage(), e);
        }

        /* PRINTING XML:
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.setOutputProperty(OutputKeys.METHOD,"xml");
            transformer.setOutputProperty(OutputKeys.INDENT,"yes");
            DOMSource source = new DOMSource(d);
            transformer.transform(source, new StreamResult(System.out)); // or to a StringWriter, for instance

         */
    }

    public XMLSettings(String xmlPath, String schemaPath) throws IOException, ConfigurationException {
        try {
            File xml = new File(xmlPath);
            if (xml.exists()) {
	            Schema schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(getClass().getResource(schemaPath));
	            Validator validator = schema.newValidator();
	            validator.validate(new StreamSource(xml));
                // if we make it down here, the xmlPath is correct
	            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	            // if it's namespace aware, it doesn't complain about the parsing, but finds nothing.
	            // if it is NOT namespace aware, it doesn't complain, but finds nothing.
	            // we're solving this in a naughty way. We verify separately BEFORE, then treat the parsed document
	            // as something completely devoid of schema and not namespace aware...
	            //dbf.setNamespaceAware(false);
	            //dbf.setSchema(schema);
	            document = dbf.newDocumentBuilder().parse(xml);

            } else {
                throw new FileNotFoundException(xml.getAbsolutePath());
            }
        } catch (SAXParseException e) {
	        throw new ConfigurationException("Failed to load settings: " + e.getSystemId() + ":" + e.getLineNumber() + ":" + e.getColumnNumber() + ": " + e.getMessage());
        } catch (SAXException | ParserConfigurationException e) {
	        throw new IOException("Failed to read configuration: " + e.getMessage(), e);
        }
    }

	private void walk(Node e) {
		System.out.println("e = " + e);
		final NodeList childNodes = e.getChildNodes();
		for (int i = 0; i < childNodes.getLength(); i++) {
			walk(childNodes.item(i));
		}
	}

    public Node getNode(String attributePath) {
        try {
            return (Node)xPath.evaluate(attributePath, document, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the value specified by the XPath expression.
     * @param attributePath XPath expression.
     * @return the value specified by the XPath expression.
     */
    public String get(String attributePath) {
        try {
            return xPath.evaluate(attributePath, document).trim();
        } catch (XPathExpressionException e) {
            // Note: this shouldn't happen, since we have a Schema to make sure it's all there
            e.printStackTrace();
            return "";
        }
    }

    public int getInt(String attributePath) {
        // this works fine, since we have an xsd checking the values for us
        return Integer.parseInt(get(attributePath));
    }

    public boolean getBoolean(String attributePath) {
        String value = get(attributePath);
        return "true".equalsIgnoreCase(value) || "1".equals(value) || "on".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value);
    }
    
}
