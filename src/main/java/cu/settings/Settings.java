package cu.settings;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-dec-21 - 20:24:06
 * @version $Id: Settings.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface Settings {
    public String get(String setting);
    public int getInt(String setting);
    public boolean getBoolean(String setting);
}
