/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.local;

import cu.ftpd.logging.Logging;
import cu.ftpd.filesystem.*;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This class keeps track of the users that have ratio 0, or "leech".
 * It keeps a list of { group, user } pairs, indicating that user has leech in group.
 * A user can have leech in more than one group at the same time.
 * When the number of groups a user has leech in falls to 0, the user gets the default ratio of the current section.
 *
 * The class is backed by <code>data/leechers.conf</code>, which gets updated every time something in the class changes. 
 *
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-sep-26 : 18:00:29
 * @version $Id: Leechers.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Leechers {
    private final File file;
    private final List<LeechEntry> leechers = Collections.synchronizedList(new LinkedList<LeechEntry>());

    public Leechers(File file) throws IOException {
        this.file = file;
        synchronized(leechers) {
            BufferedReader in = null;

            if (!file.exists()) {
                System.out.println("leechers.conf not found, creating...");
                if (!file.createNewFile()) {
                    throw new IOException("Unable to create new leechers.conf.");
                }
                return; // we might as well return here since we don't want to read an empty file anyway
            }
            try {
                // NOTE: don't move this to the user object, since the user object doesn't know about the userdir
                in = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                String line;
                while((line = in.readLine()) != null) {
                    if (line.startsWith("#")) {
                        continue; // this was a comment
                    }
                    String[] lp = line.split(";", 2);
                    // username;group
                    if (lp.length == 2) {
                        leechers.add(new LeechEntry(lp[0], lp[1]));
                    } else {
                        // something wrong with the line
                        System.err.println("Skipping erronenous leecher line: " + line);
                    }
                }
            } catch (IOException e) {
                Logging.getErrorLog().reportCritical("Error while loading leechers.conf: " + e.getMessage());
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void save() {
        synchronized(leechers) {
            FileSystem.fastBackup(file);
            PrintWriter out = null;
            try {
                out = new PrintWriter(new FileOutputStream(file));
                for (LeechEntry le : leechers) {
                    out.println(le.getUsername() + ';' + le.getGroupname());
                }
                out.flush();
            } catch (FileNotFoundException e) {
                Logging.getErrorLog().reportError("Failed to write leechers to disk: " + e.getMessage());
            } finally {
                if (out != null) {
                    out.close();
                }
            }

        }
    }

    public void addLeecher(LeechEntry le) {
        leechers.add(le);
    }

    public void removeLeecher(LeechEntry le) {
        leechers.remove(le);
    }

    /**
     * Gets the list of the leech-users.
     * NOTE: when iterating over this list, make sure to lock on the returned list, as that is the mutex for accessing the list itself.
     *
     * @return the list of users with ratio 0
     */
    public List<LeechEntry> getLeechers() {
        return leechers;
    }

    public List<LeechEntry> getLeechersForGroup(String groupname) {
        List<LeechEntry> l = new LinkedList<LeechEntry>();
        synchronized(leechers) {
            for (LeechEntry le : leechers) {
                if (le.getGroupname().equalsIgnoreCase(groupname)) {
                    l.add(le);
                }
            }
        }
        return l;
    }

    public boolean hasLeech(String username) {
        // look and see if the user exists in the list
        synchronized(leechers) {
            for (LeechEntry le : leechers) {
                if (le.getUsername().equals(username)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean userUsesLeechSlotForGroup(String username, String groupname) {
        return leechers.contains(new LeechEntry(username, groupname));
    }

    /**
     * Removes all entries in the leecher database that matches this username.
     * @param username the username of the user for whom we wish to delete leecher entries.
     */
    public void removeAllLeechForUser(String username) {
        removeAllLeech(username, false);
    }

    public void remoteAllLeechForGroup(String groupname) {
        removeAllLeech(groupname, true);
    }

    private void removeAllLeech(String entity, boolean group) {
        synchronized(leechers) {
            Iterator<LeechEntry> i = leechers.iterator();
            while (i.hasNext()) {
                LeechEntry leechEntry = i.next();
                if (leechEntry.getUsername().equals(entity) || (group && leechEntry.getGroupname().equals(entity))) {
                    i.remove();
                }
            }
        }
    }
}
