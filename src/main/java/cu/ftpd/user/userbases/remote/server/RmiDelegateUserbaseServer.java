/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.remote.server;

import cu.authentication.AuthenticationRequest;
import cu.ftpd.logging.Logging;
import cu.ftpd.user.User;
import cu.ftpd.user.groups.Group;
import cu.ftpd.user.groups.GroupExistsException;
import cu.ftpd.user.groups.NoSuchGroupException;
import cu.ftpd.user.statistics.local.LocalUserStatistics;
import cu.ftpd.user.statistics.remote.server.RmiUserStatistics;
import cu.ftpd.user.statistics.remote.server.RmiUserStatisticsServer;
import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.user.userbases.Userbase;
import cu.ftpd.user.userbases.local.Allotment;
import cu.ftpd.user.userbases.local.LeechEntry;
import cu.ftpd.user.userbases.local.LocalUserbase;
import cu.ftpd.user.userbases.UserExistsException;
import cu.ftpd.user.userbases.GroupLimitReachedException;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.rmi.ssl.SslRMIServerSocketFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.rmi.AccessException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

/**
 * This is the RMI userbase. It has a Userbase as a delegate that it invokes methods on. This is usually an instance of
 * the standard CuftpdUserbase. Thus, this is the last step in the wrapper process.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-sep-04 : 13:12:56
 * @version $Id: RmiDelegateUserbaseServer.java 258 2008-10-26 12:47:23Z jevring $
 */
public class RmiDelegateUserbaseServer implements RmiDelegateUserbase {
    private final Userbase userbase; // this way we can delegate to any userbase we want

    public void takeCredits(String username, long credits) throws RemoteException {
        try {
            userbase.getUser(username).takeCredits(credits);
        } catch (NoSuchUserException e) {
            Logging.getErrorLog().reportCritical("Trying to take credits from a user that does not exist: " + username);
        }
    }

    public void giveCredits(String username, long credits) throws RemoteException {
        try {
            userbase.getUser(username).giveCredits(credits);
        } catch (NoSuchUserException e) {
            Logging.getErrorLog().reportCritical("Trying to take credits from a user that does not exist: " + username);
        }
    }

    public long getCredits(String username) throws RemoteException {
        try {
            return userbase.getUser(username).getCredits();
        } catch (NoSuchUserException e) {
            Logging.getErrorLog().reportCritical("Trying to get credits for a user that does not exist: " + username);
            return 0;
        }
    }

    public void setProperty(String username, String property, String value) throws RemoteException {
        try {
            User user = userbase.getUser(username);
            // setting will happen VERY rarely, so the speed of this isn't particularly important.
            if ("lastlog".equals(property)) {
                user.setLastlog(Long.parseLong(value));
            } else if ("primarygroup".equals(property)) {
                user.setPrimaryGroup(value);
            } else if ("addip".equals(property)) {
                user.addIp(value);
            } else if ("delip".equals(property)) {
                user.delIp(value);
            } else if ("addgadminforgroup".equals(property)) {
                user.addGadminForGroup(value);
            } else if ("removegadminforgroup".equals(property)) {
                user.removeGadminForGroup(value);
            } else if ("hidden".equals(property)) {
                user.setHidden(Boolean.parseBoolean(value));
            } else if ("password".equals(property)) {
                user.passwd(value);
            } else if ("tagline".equals(property)) {
                user.setTagline(value);
            } else if ("logins".equals(property)) {
                user.setLogins(Integer.parseInt(value));
            } else if ("suspended".equals(property)) {
                user.setSuspended(Boolean.parseBoolean(value));
            } else if ("addpermission".equals(property)) {
                user.addPermission(Integer.parseInt(value));
            } else if ("delpermission".equals(property)) {
                user.delPermission(Integer.parseInt(value));
            } else if ("autg".equals(property)) {
                // we check that the group exists in the site-command
                user.addGroup(value);
            } else if ("rufg".equals(property)) {
                user.removeGroup(value);
            } else {
                throw new IllegalArgumentException("Tried to find property that does not exist: [" + username + ':' + property + ':' + value + ']');
            }
        // NOTE: we can be guaranteed to not get any NumberFormatExceptions here, since the caller of this method does a conversion FROM the appropriate formate before sending it over the wire
        } catch (NoSuchUserException e) {
            Logging.getErrorLog().reportCritical("Trying to set property '" + property + "' for a user that does not exist: " + username);
        }
    }

    public String getProperty(String username, String property) throws RemoteException {
        try {
            User user = userbase.getUser(username);
            // this is where it would be nice to have a HashMap with function pointers
            if ("leech".equals(property)) {
                return String.valueOf(user.hasLeech());
            } else if ("hidden".equals(property)) {
                return String.valueOf(user.isHidden());
            } else if ("created".equals(property)) {
                return String.valueOf(user.getCreated());
            } else if ("tagline".equals(property)) {
                return user.getTagline();
            } else if ("logins".equals(property)) {
                return String.valueOf(user.getLogins());
            } else if ("lastlog".equals(property)) {
                return String.valueOf(user.getLastlog());
            } else if ("password".equals(property)) {
                return user.getHashedPassword();
            } else if ("suspended".equals(property)) {
                return String.valueOf(user.isSuspended());
            } else if ("permissions".equals(property)) {
                return user.getPermissions();
            } else {
                throw new IllegalArgumentException("Tried to find property that does not exist: [" + username + ':' + property + ']');
            }
        } catch (NoSuchUserException e) {
            Logging.getErrorLog().reportCritical("Trying to get property '" + property + "' for a user that does not exist: " + username);
        }
        return null;
    }

    public String getGroupProperty(String groupname, String property) throws RemoteException {
        try {
            Group g = userbase.getGroup(groupname);
            // this is where it would be nice to have a HashMap with function pointers
            if ("getgroupslots".equals(property)) {
                return String.valueOf(g.getSlots());
            } else if ("getleechslots".equals(property)) {
                return String.valueOf(g.getLeechSlots());
            } else if ("getdescription".equals(property)) {
                return g.getDescription();
            } else if ("getmaxallotment".equals(property)) {
                return String.valueOf(g.getMaxAllotment());
            } else if ("getallotmentslots".equals(property)) {
                return String.valueOf(g.getAllotmentSlots());
            } else {
                throw new IllegalArgumentException("Tried to find property that does not exist: [" + groupname + ':' + property + ']');
            }
        } catch (NoSuchGroupException e) {
            Logging.getErrorLog().reportCritical("Trying to get property '" + property + "' for a group that does not exist: " + groupname);
        }
        return null;
    }

    public void setGroupProperty(String groupname, String property, String value) throws RemoteException {
        try {
            Group g = userbase.getGroup(groupname);
            // this is where it would be nice to have a HashMap with function pointers
            if ("setgroupslots".equals(property)) {
                g.setSlots(Integer.parseInt(value));
            } else if ("setleechslots".equals(property)) {
                g.setLeechSlots(Integer.parseInt(value));
            } else if ("setdescription".equals(property)) {
                g.setDescription(value);
            } else if ("setmaxallotment".equals(property)) {
                g.setMaxAllotment(Long.parseLong(value));
            } else if ("setallotmentslots".equals(property)) {
                g.setAllotmentSlots(Integer.parseInt(value));
            } else {
                throw new IllegalArgumentException("Tried to find property that does not exist: [" + groupname + ':' + property + ']');
            }
        // NOTE: we can be guaranteed to not get any NumberFormatExceptions here, since the caller of this method does a conversion FROM the appropriate formate before sending it over the wire
        } catch (NoSuchGroupException e) {
            // NOTE: we can be guaranteed to not get this exception thrown, since the caller of this method does a conversion FROM the appropriate formate before sending it over the wire
            Logging.getErrorLog().reportCritical("Trying to get property '" + property + "' for a group that does not exist: " + groupname);
        }
    }


    public Collection getCollection(String username, String collection) throws RemoteException {
        if (username != null) {
            try {
                User user = userbase.getUser(username);
                if ("groups".equals(collection)) {
                    return user.getGroups();
                } else if ("ips".equals(collection)) {
                    return user.getIps();
                } else if ("gadmingroups".equals(collection)) {
                    return user.getGadminGroups();
                } else {
                    throw new IllegalArgumentException("CRITICAL: Trying to get non-existant collection '" + collection + '\'');
                }
            } catch (NoSuchUserException e) {
                Logging.getErrorLog().reportCritical("Trying to get collection '" + collection + "' for a user that does not exist: " + username);
            }
        } else {
            if ("users".equals(collection)) {
                return new LinkedList<String>(userbase.getUsers().keySet());
            } else if ("allgroups".equals(collection)) {
                return new LinkedList<String>(userbase.getGroups().keySet());
            }
        }
        return null;
    }

    public String getPrimaryGroup(String username) throws RemoteException {
        try {
            return userbase.getUser(username).getPrimaryGroup();
        } catch (NoSuchUserException e) {
            // NOTE: This should NEVER happen, because we are calling this from inside a RemoteUser object, so we know we already have the user
            Logging.getErrorLog().reportCritical("Trying to get property 'primarygroup' for a user that does not exist: " + username);
        }
        return null;
    }

    /**
     * Provides with an easy way of examining 2-arg boolean predicates in the remote userbase.
     * Examples in infix form:<br>
     *  - username isMemberOfGroup groupname<br>
     *  - username isGadminForGroup groupname<br>
     *  - username hasPermission permission<br>
     *  - username userExists null<br>
     *<br>
     * @param predicate the boolean predicate we are after.
     * @param arg1 the first argiment.
     * @param arg2 the second argument.
     * @return the boolean value for the predicate with the provided arguments
     */
    public boolean getBoolean(String predicate, String arg1, String arg2) throws RemoteException, NoSuchUserException {

        User user;
        if ("isMemberOfGroup".equals(predicate)) {
            user = userbase.getUser(arg1);
            return user.isMemberOfGroup(arg2);
        } else if ("isGadminForGroup".equals(predicate)) {
            user = userbase.getUser(arg1);
            return user.isGadminForGroup(arg2);
        } else if ("hasPermission".equals(predicate)) {
            user = userbase.getUser(arg1);
            return user.hasPermission(Integer.parseInt(arg2));
        } else if ("userExists".equals(predicate)) {
            try {
                userbase.getUser(arg1);
                return true;
            } catch (NoSuchUserException e) {
                return false;
            }
        } else {
            throw new IllegalArgumentException("Unknown predicate: " + predicate);
        }
    }


// RmiDelegateUserbase:
    public RmiDelegateUserbaseServer(Userbase userbase) {
        this.userbase = userbase;
    }

    public void deleteUser(String username) throws RemoteException, NoSuchUserException {
        userbase.deleteUser(username);
    }

    public String createHashedPassword(String password) throws RemoteException {
        return userbase.createHashedPassword(password);
    }

    public int authenticate(AuthenticationRequest auth) throws RemoteException {
        return userbase.authenticate(auth);
    }

    public void deleteGroup(String groupname) throws NoSuchGroupException, RemoteException {
        userbase.deleteGroup(groupname);
    }

    public Group getGroup(String name) throws NoSuchGroupException, RemoteException {
        return userbase.getGroup(name);
    }

    public void renameGroup(String oldName, String newName) throws NoSuchGroupException, GroupExistsException, RemoteException {
        userbase.renameGroup(oldName, newName);
    }

    public Group createGroup(String name, String description) throws GroupExistsException, RemoteException {
        return userbase.createGroup(name, description);
    }

    /**
     * Creates a user and adds it to the specified group.
     *
     * @param groupname the name of the group in which to add the user.
     * @param username the username of the user to add.
     * @param password the (cleartext) password of the user.
     * @param checkSpace if set to <tt>true</tt> the space in the group will be checked before adding.
     * @return the newly created user, or <tt>null</tt> if <tt>checkSpace</tt> was set to <tt>true</tt> and the group was already full.
     * @throws cu.ftpd.user.groups.NoSuchGroupException thrown if the specified group doesn't exist.
     * @throws UserExistsException thrown if there is already a user with the specified username.
     */
    public String gAddUser(String groupname, String username, String password, boolean checkSpace) throws NoSuchGroupException, UserExistsException, RemoteException, GroupLimitReachedException {
        User user = userbase.gAddUser(groupname, username, password, checkSpace);
        return user.getUsername();
    }

    public String addUser(String username, String password) throws UserExistsException, RemoteException {
        User user = userbase.addUser(username, password);
        return user.getUsername();
    }

    public boolean isUp() throws RemoteException {
        // used to see if we are back up after a disconnect;
        return true;
    }

    public boolean setLeechForUser(boolean leech, String username, String groupname, boolean checkAvailability) throws RemoteException, NoSuchGroupException, NoSuchUserException {
        return userbase.setLeechForUser(leech, username, groupname, checkAvailability);
    }

    public boolean setAllotmentForUser(long allotment, String username, String groupname, boolean checkAvailability) throws RemoteException, NoSuchGroupException, NoSuchUserException {
        return userbase.setAllotmentForUser(allotment, username, groupname, checkAvailability);
    }

    public boolean userUsesLeechSlotForGroup(String username, String groupname) throws RemoteException {
        return userbase.userUsesLeechSlotForGroup(username, groupname);
    }

    public boolean userUsesAllotmentSlotForGroup(String username, String groupname) throws RemoteException {
        return userbase.userUsesAllotmentSlotForGroup(username, groupname);
    }

    public List<Allotment> getAllotments(String groupname) throws RemoteException {
        return userbase.getAllotments(groupname);
    }

    public void saveGroups() throws RemoteException {
        userbase.saveGroups();
    }

    public List<LeechEntry> getLeechers(String groupname) throws RemoteException {
        return userbase.getLeechers(groupname);
    }

    // See explanation below on why this is needed
    public static RmiDelegateUserbaseServer rmidus;
    public static RmiUserStatisticsServer rmiuss;
    /** I put this whole thing in javadoc, so that it could be folded. I couldn't stand seeing it clutter my code, hehe
    This is done to prevent the server from terminating prematurely.
    The suggestion in http://forum.java.sun.com/thread.jspa?threadID=699630&messageID=4060381 (and in http://forum.java.sun.com/thread.jspa?threadID=5155806&messageID=9659873), i.e. keeping a static
    reference to the (in our case Userbase) object did not work.
    UPDATE: Apparently, when we keep a static reference to the RmiDelegateUserbaseServer rmidus, it does work.
    Just in case, however, we will keep this code around to highlight the problem, since it is just wonky GC behavior
    Object forever = new Object();
    synchronized(forever) {
        try {
            forever.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
     CHANGE: Since we might be faced with "smart" garbage collectors, it's been recommended that these fields be public instead of private, so that they don't get GC'ed (http://osdir.com/ml/java.sun.rmi/2004-08/msg00038.html)
    */
    public static void main(String[] args) {
        Properties p;
        try {
            p = new Properties();
            String location = "data/userbase.conf";
            if (args.length == 1) {
                location = args[0];
            }
            p.load(new FileInputStream(location));


            System.setProperty("javax.net.ssl.trustStore", p.getProperty("ssl.truststore.location"));
            System.setProperty("javax.net.ssl.trustStorePassword", p.getProperty("ssl.truststore.password"));
            System.setProperty("javax.net.ssl.keyStore", p.getProperty("ssl.keystore.location"));
            System.setProperty("javax.net.ssl.keyStorePassword", p.getProperty("ssl.keystore.password"));


            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new SecurityManager());
            }
            final String dataDirectory = p.getProperty("data.directory", "data/");

            Logging.initializeErrorLog(dataDirectory + "logs/error.log"); // the error log is needed by the userbase

            // SOCKET FACTORIES
            // Because these things don't throw any exceptions that can be caught when they crash and burn, there's little we can do about usability.
            SslRMIClientSocketFactory csf = new SslRMIClientSocketFactory();
            SslRMIServerSocketFactory ssf = new SslRMIServerSocketFactory(null, null, true); // if we need client authentication, we should pass "true" here as the last argument (hopefully it will work with the other values set to null)

            // REGISTRY
            // w00t, since we use .createRegistry(..) we don't need to have the rmiregistry app running in the background. this is more user friendly
            // this is also the only way, since we are using ssl sockets for the registry
            // export first, then get, to avoid getting that nasty no object found exception
            // _todo: try without .getRegistry() first
            // NOTE: we SHOULD NOT DO THIS, as this will cause the client socket factory to be initialized with the ssl context from the server, which is wrong
            Registry registry = LocateRegistry.createRegistry(Integer.parseInt(p.getProperty("port", "1099")), csf, ssf);
            //registry = LocateRegistry.getRegistry("localhost", Integer.parseInt(p.getProperty("port", "1099")), csf);
            /*
            If we use .getRegistry(), we need to add teh following permissions to userbase.policy:
            permission java.util.PropertyPermission "javax.rmi.ssl.client.enabledCipherSuites", "read";
            permission java.util.PropertyPermission "javax.rmi.ssl.client.enabledProtocols", "read";
            permission java.util.PropertyPermission "javax.net.ssl.trustStore", "read,write";
            permission java.util.PropertyPermission "javax.net.ssl.trustStorePassword", "read,write";
            permission java.util.PropertyPermission "javax.net.ssl.keyStore", "read,write";
            permission java.util.PropertyPermission "javax.net.ssl.keyStorePassword", "read,write";

             */
            // http://www.nabble.com/java.rmi.NoSuchObjectException:-no-such-object-in-table-td729138.html
            // http://www.nabble.com/What-causes-an-object-to-be-removed-from-the-ObjectTable--td1982425.html

            // USERBASE
            // this is done to appease the statistics engine so that it can do group-related statistics
            rmidus = new RmiDelegateUserbaseServer(new LocalUserbase(new File(dataDirectory), true, null)); // todo: we have to do something here to avoid passing null. more on this later

            RmiDelegateUserbase userbaseStub = (RmiDelegateUserbase) UnicastRemoteObject.exportObject(rmidus, Integer.parseInt(p.getProperty("userbase.port", "0")), csf, ssf);
            //registry.rebind("userbase", userbaseStub); 
            // maybe binding the rmidus instead of the stub keeps it from being GCed (http://saloon.javaranch.com/cgi-bin/ubb/ultimatebb.cgi?ubb=get_topic&f=4&t=002159)
            // changed to export the right object to prevent it from being garbage collected (i.e. don't export the stub)
            registry.rebind("userbase", rmidus);

            // STATISTICS
            rmiuss = new RmiUserStatisticsServer(new LocalUserStatistics(new File(dataDirectory, "/logs/userstatistics")));
            RmiUserStatistics statisticsStub = (RmiUserStatistics) UnicastRemoteObject.exportObject(rmiuss, Integer.parseInt(p.getProperty("statistics.port", "0")), csf, ssf);
            //registry.rebind("statistics", statisticsStub);
            // changed to export the right object to prevent it from being garbage collected (i.e. don't export the stub)
            registry.rebind("statistics", rmiuss);

            System.out.println("Remote userbase and statistics started " + getVersion()); // maybe this could be cleaner, but at least this way we don't have to instantiate the server class, but we can override it nicely in cubnc
        } catch (AccessException e) {
            System.err.println("Permission exception: " + e.getMessage());
        } catch (FileNotFoundException e) {
            System.err.println("Could not find configuration file: " + e.getMessage());
        } catch (RemoteException e) {
            System.err.println("Error exporting objects: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Could not read configuration file: " + e.getMessage());
        }
    }

    public static String getVersion() {
        final Package pkg = RmiDelegateUserbaseServer.class.getPackage();
        return pkg.getSpecificationVersion() + " (" + pkg.getImplementationVersion() + ", " + pkg.getImplementationTitle() + ")";
    }
}
// caching:
// this saver should run as often as possible, provided no data transfer subsystem is running on it. We want things saved as soon as possible, without incurring a performance loss of doing it with each operation
// maybe we should have a queue with updated objects. Whenever an object gets updated, it is put last in the queue, and a process goes over the queue to save things as quickly as possible, all the while leaving the RMI call to execute as quickly as possible
// read about disk caching strategies/implementations
// see if we can't do this store via channels or something (some NIO class to hold the data?) so that we don't have to switch context all the time
// the general idea is to keep stuff caches until it expires in the cache (is pushed out by something else), but since we can have a sizeof(RAM) cache, then things will rarely, possibly never, be pushed out, so they would need to be saved more often to ensure crash protection (cache synchrnoization)
// we should have similar policies for a cache backing of objects. Maybe we can write a generic cache implementation that we use for all cached writes (metadata and userfiles and user statistics)
// we could even use read-fetching for users (it wouldn't actually improve memory usage, since users don't take up that much space)
// we could have a common file-cache, both for reading and writing, so that all reads and writes (that we specify, as above) are done, they are done through the cache
// have a setting to use write-caching for the above things (easily implementable if we have a centralized cache)
// OR we just rely in the underlying disk-caching algorithm to do all the work for us. This is much simpler (essentially making it a write-through cache, since the changes still exist in memory))
// we could use optimistic replication, and store the additions and subtractions to the credits, and send them as a batch every so often, but the result would be that users see different values on each host, which is bad.
/*
What we will do is use a write-through cache.
This will enable us to always have the data in memory available for a read, and always on the disk to prevent against crashes
In turn we will rely on the underlying disk-cache system of the OS to perform optimized writes for us
This will apply to all disk-writing activities, like userfiles, statistics and metadata.
Upload/Download will come later.
We will enter this into the design decisions

This means that we will get rid of a lot of savers, leading to fewer tasks and possibly less complexity.

 */

