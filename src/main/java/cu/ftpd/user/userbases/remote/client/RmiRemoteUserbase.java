/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.remote.client;

import cu.authentication.AuthenticationRequest;
import cu.ftpd.Server;
import cu.ftpd.logging.Logging;
import cu.ftpd.user.User;
import cu.ftpd.user.groups.Group;
import cu.ftpd.user.groups.GroupExistsException;
import cu.ftpd.user.groups.NoSuchGroupException;
import cu.ftpd.user.groups.RemoteGroup;
import cu.ftpd.user.userbases.local.Allotment;
import cu.ftpd.user.userbases.local.LeechEntry;
import cu.ftpd.user.userbases.*;
import cu.ftpd.user.userbases.remote.RemoteUser;
import cu.ftpd.user.userbases.remote.server.RmiDelegateUserbase;

import javax.rmi.ssl.SslRMIClientSocketFactory;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;
import java.io.IOException;

/**
 * This userbase uses a socket connection to set, update and get values for different properties of different users.
 * This is more or less only a delegate wrapper for the remote methods, such that it handles the RemoteExceptions thrown
 * from them and re-connects if that happens and tries again.
 *
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-10 : 11:45:37
 * @version $Id: RmiRemoteUserbase.java 282 2008-11-26 18:15:43Z jevring $
 */
public class RmiRemoteUserbase implements RemoteUserbase {
    private final String host;
    private final int port;
    protected final Random random = new Random();
    private RmiDelegateUserbase rmi;
    private final int retry;
    private Registry registry;
    private volatile boolean reinitializing;
    private final Object reconnectCompletedLock = new Object();

    public RmiRemoteUserbase(String host, int port, int retry) throws IOException, NotBoundException {
        this.host = host;
        this.port = port;
        this.retry = retry;
        registry = LocateRegistry.getRegistry(host, port, new SslRMIClientSocketFactory());
        rmi = (RmiDelegateUserbase) registry.lookup("userbase");
        if (rmi.isUp()) {
            System.out.println("Located remote userbase at " + host + ':' + port);
        } else {
            //System.err.println("Failed to connect to userbase. (Found registry)");
            throw new IOException("Failed to connect to userbase. (Found registry but not userbase)");
        }
    }

    public String getStringProperty(String username, String property) {
        try {
            return rmi.getProperty(username, property);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getStringProperty(username, property);
        }
    }
    public boolean getBooleanProperty(String username, String property) {
        return Boolean.parseBoolean(getStringProperty(username, property));

    }
    public long getLongProperty(String username, String property) {
        try {
            return Long.parseLong(getStringProperty(username, property));
        } catch (NumberFormatException e) {
            Logging.getErrorLog().reportCritical("Userfile error for user '" + username + "', value '" + property + "': Was not a number: " + e.getMessage());
            return 0;
        }
    }
    public int getIntProperty(String username, String property) {
        try {
            return Integer.parseInt(getStringProperty(username, property));
        } catch (NumberFormatException e) {
            Logging.getErrorLog().reportCritical("Userfile error for user '" + username + "', value '" + property + "': Was not a number: " + e.getMessage());
            return 0;
        }
    }
    public void setProperty(String username, String property, String value) {
        try {
            rmi.setProperty(username, property, value);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            setProperty(username, property, value);
        }
    }
    public void setProperty(String username, String property, long value) {
        setProperty(username, property, String.valueOf(value));
    }
    public void setProperty(String username, String property, int value) {
        setProperty(username, property, String.valueOf(value));
    }
    public void setProperty(String username, String property, boolean value) {
        setProperty(username, property, String.valueOf(value));
    }

    public void setGroupProperty(String groupname, String property, String value) {
        try {
            rmi.setGroupProperty(groupname, property, value);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            setGroupProperty(groupname, property, value);
        }
    }

    public String getGroupProperty(String groupname, String property) {
        try {
            return rmi.getGroupProperty(groupname, property);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getGroupProperty(groupname, property);
        }
    }

    public void takeCredits(String username, long credits) {
        try {
            rmi.takeCredits(username, credits);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            takeCredits(username, credits);
        }
    }
    public void giveCredits(String username, long credits) {
        try {
            rmi.giveCredits(username, credits);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            giveCredits(username, credits);
        }
    }
    public long getCredits(String username) {
        try {
            return rmi.getCredits(username);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getCredits(username);
        }
    }

    public Collection getCollection(String username, String collection) {
        try {
            return rmi.getCollection(username, collection);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getCollection(username, collection);
        }
    }

    public String getPrimaryGroup(String username) {
        try {
            return rmi.getPrimaryGroup(username);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getPrimaryGroup(username);
        }
    }

    /**
     * Provides with an easy way of examining 2-arg boolean predicates in the remote userbase.
     * Examples in infix form:
     *  - username isMemberOfGroup groupname
     *  - username isGadminForGroup groupname
     *  - username hasPermission permission
     *
     * @param predicate the boolean predicate we are after.
     * @param arg1 the first argiment.
     * @param arg2 the second argument.
     * @return the boolean value for the predicate with the provided arguments
     */
    public boolean getBoolean(String predicate, String arg1, String arg2) throws NoSuchUserException{
        try {
            return rmi.getBoolean(predicate, arg1, arg2);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getBoolean(predicate, arg1, arg2);
        }
    }


    public User getUser(String username) throws NoSuchUserException {
        try {
            if (rmi.getBoolean("userExists", username, null)) {
                return new RemoteUser(username, this);
            } else {
                // this will be thrown anyway, but what the heck...
                throw new NoSuchUserException(username);
            }
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getUser(username);
        }
    }

    public User addUser(String username, String password) throws UserExistsException {
        String user;
        try {
            user = rmi.addUser(username, password);
            // otherwise we return a local user, and that's bad
            return new RemoteUser(user, this);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return addUser(username, password);
        }
    }
    public void deleteUser(String username) throws NoSuchUserException {
        try {
            rmi.deleteUser(username);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            deleteUser(username);
        }
    }

    /**
     * Creates a user and adds it to the specified group.
     *
     * @param groupname the name of the group in which to add the user.
     * @param username the username of the user to add.
     * @param password the (cleartext) password of the user.
     * @param checkSpace if set to <tt>true</tt> the space in the group will be checked before adding.
     * @return the newly created user, or <tt>null</tt> if <tt>checkSpace</tt> was set to <tt>true</tt> and the group was already full.
     * @throws cu.ftpd.user.groups.NoSuchGroupException thrown if the specified group doesn't exist.
     * @throws UserExistsException thrown if there is already a user with the specified username.
     */
    public User gAddUser(String groupname, String username, String password, boolean checkSpace) throws UserExistsException, NoSuchGroupException, GroupLimitReachedException {
        String user;
        try {
            user = rmi.gAddUser(groupname, username, password, checkSpace);
            // otherwise we return a local user, and that's bad
            return new RemoteUser(user, this);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return gAddUser(groupname, username, password, checkSpace);
        }
    }

    // user authentication
    public String createHashedPassword(String password) {
        byte[] salt = new byte[4];
        random.nextBytes(salt);
        String hashedPassword = PasswordHasher.cuftpdHash(password.toCharArray(), salt);
        return '$' + new String(Hex.bytesToHex(salt)) + '$' + hashedPassword;
    }

    public void save() {
        // do nothing, this is triggered on the server side for remote userbases
    }

    public void renameGroup(String oldName, String newName) throws NoSuchGroupException, GroupExistsException {
        try {
            rmi.renameGroup(oldName, newName);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            renameGroup(oldName, newName);
        }
    }

    /**
     * Sets the ratio for the specified user in the specified group.
     * If a group is provided and <code>checkAvailability</code> is set to <code>true</code> and <code>ratio</code> is set to <code>0</code> then a check is performed.
     * This check counts the number of "ratio 0" users currently in the group, and compares this to the max number of allowed "ratio 0" users.
     * If the current number is less than the allowed, the ratio for the user is changed.
     * If not, then this will return false.
     * <p/>
     * NOTE: This function MUST lock on some object to prevent simultaneous update of the users (which could be exploited to grant more leech slots than intended)
     *
     * @param leech             true if the user is supposed to have leech in the specified group.
     * @param username          the user to set the ratio for.
     * @param groupname         the group from which to use possible leech slots.
     * @param checkAvailability true if we check if there are any leech slots available in the group, if applicable. @return true if the ratio was successfully set for the user, false otherwise. (See above) @throws cuftpd.user.authentication.userbases.NoSuchUserException
     *          thrown if the specified user can't be found
     * @throws cu.ftpd.user.groups.NoSuchGroupException
     *          thrown if the specified group can't be found
     */
    public boolean setLeechForUser(boolean leech, String username, String groupname, boolean checkAvailability) throws NoSuchUserException, NoSuchGroupException {
        try {
            return rmi.setLeechForUser(leech, username, groupname, checkAvailability);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return setLeechForUser(leech, username, groupname, checkAvailability);
        }
    }

    /**
     * Sets the allotment for the specified user in the specified group.
     * If a group is provided and <code>checkAvailability</code> is set to <code>true</code> then a check is performed.
     * This check counts the number of allotment users already present in the group, and if that is more than the number of allowed allotment users, it will return <code>false</code>.
     * It also checks that the allotment is within the accpetable range set for the group in question.
     * <p/>
     * NOTE: This function MUST lock on some object to prevent simultaneous update of the users (which could be exploited to grant more allotment slots than intended)
     *
     * @param allotment         the amount of credits to allot every week.
     * @param username          the username to which to allot the credits.
     * @param groupname         the groupname for which we are checking the slots.
     * @param checkAvailability true if we are doing checks as per above, false otherwise.
     * @return true if the allotment was set to the user properly, false otherwise.
     * @throws cu.ftpd.user.userbases.NoSuchUserException
     *          if the specified user can't be found
     * @throws cu.ftpd.user.groups.NoSuchGroupException
     *          if the specified group can't be found
     */
    public boolean setAllotmentForUser(long allotment, String username, String groupname, boolean checkAvailability) throws NoSuchUserException, NoSuchGroupException {
        try {
            return rmi.setAllotmentForUser(allotment, username, groupname, checkAvailability);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return setAllotmentForUser(allotment, username, groupname, checkAvailability);
        }
    }

    public Map<String, Group> getGroups() {
        // NOTE: for this return the REAL set of users.
        // since this is (so far) only called by Userbase.getUsers(), it's more efficient to transfer the whole structure in one go, than to subrequest userinfo for each user.
        List<String> groupnames = (List<String>)getCollection(null, "allgroups");
        Map<String, Group> groups = new HashMap<>();
        for (String groupname : groupnames) {
            groups.put(groupname, new RemoteGroup(groupname, this));
        }
        return groups;
    }

    /**
     * Checks the Leechers object to see if this user uses a leech slot for the specified group.
     */
    public boolean userUsesLeechSlotForGroup(String username, String groupname) {
        try {
            return rmi.userUsesLeechSlotForGroup(username, groupname);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return userUsesLeechSlotForGroup(username, groupname);
        }
    }

    /**
     * Checks the Allotments object to see if this user users an allotment slot for the specified group.
     */
    public boolean userUsesAllotmentSlotForGroup(String username, String groupname) {
        try {
            return rmi.userUsesAllotmentSlotForGroup(username, groupname);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return userUsesAllotmentSlotForGroup(username, groupname);
        }
    }

    public List<Allotment> getAllotments(String groupname) {
        try {
            return rmi.getAllotments(groupname);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getAllotments(groupname);
        }
    }

    public void saveGroups() {
        try {
            rmi.saveGroups();
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            saveGroups();
        }

    }

    public List<LeechEntry> getLeechers(String groupname) {
        try {
            return rmi.getLeechers(groupname);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getLeechers(groupname);
        }
    }

    public Group createGroup(String name, String description) throws GroupExistsException {
        try {
            Group g = rmi.createGroup(name, description);
            // otherwise we get a group of the wrong type
            return new RemoteGroup(g.getName(), this);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return createGroup(name, description);
        }
    }

    public void deleteGroup(String groupname) throws NoSuchGroupException {
        try {
            rmi.deleteGroup(groupname);
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            deleteGroup(groupname);
        }
    }

    public Map<String,User> getUsers() {
        // NOTE: for this return the REAL set of users.
        // since this is (so far) only called by Userbase.getUsers(), it's more efficient to transfer the whole structure in one go, than to subrequest userinfo for each user.
        LinkedList<String> usernames = (LinkedList<String>)getCollection(null, "users");
        HashMap<String, User> users = new HashMap<String, User>();
        for (String username : usernames) {
            users.put(username, new RemoteUser(username, this));
        }
        return users;
/*
        try {
            return rmi.getUsers();
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getUsers();
        }
*/
    }

    public Group getGroup(String name) throws NoSuchGroupException {
        try {
            if (rmi.getGroup(name) != null) {
                return new RemoteGroup(name, this);
            } else {
                // this will never be thrown, since the if-statement causes it to be thrown first
                throw new NoSuchGroupException(name);
            }
        } catch (RemoteException e) {
            System.err.println(e.getMessage()); reinitialize();
            return getGroup(name);
        }
    }

    public int authenticate(AuthenticationRequest auth) {
        try {
            return rmi.authenticate(auth);
        } catch (RemoteException e) {

            System.err.println(e.getMessage());
            reinitialize();
            return authenticate(auth);
        }
    }

    private void reinitialize() {
        // _todo: Se till att vi inte har flera tradar som forsoker reconnecta userbase eller statistics nar dom gar ner, utan att det bara ar en som gor det.
        // done, in the sense that we will check 'reinitializing', which is volatile

        // _todo: spawn a new thread for this, so that if a user disconnects while we wait, it doesn't die?????
        // that's not needed. This thread keeps on going even if the user disconnects the socket

        // we can't use an if-statement. if we do, then two threads could examine the statment and enter before it is set.
        // ideally we want things waiting here for the lock to not have to reconnect after our recommection is done
        // but then again we can't be 100% sure that it didn't disconnect again
        // one "issue" here is that the threads will still be alive, waiting for the lock, even if a user disconnects or gets kicked, but that's ok, because that just means that
        // the command still gets issued when the server comes back up
        synchronized (reconnectCompletedLock) {
            // suspend the server
            // try to re-connect the rmi
            // when re-connected, unsuspend the server
            // loop forever until re-connected

            // do a quick check here. if we entered just after it got fixed by some other thread, we don't have to do the whole dance again
            try {
                if (rmi.isUp()) {
                    return;
                }
            } catch (RemoteException e) {
                // this just means that it is actually down, continue to do the reconnect
            }

            long start = System.currentTimeMillis();
            Server.getInstance().suspend();
            int i = 0;
            while (true) {
                i++;
                Logging.getErrorLog().reportError("Trying to re-connect to remote userbase, tries: " + i);
                System.err.println("Trying to re-connect to remote userbase, tries: " + i);
                try {
                    registry = LocateRegistry.getRegistry(host, port, new SslRMIClientSocketFactory());
                    rmi = (RmiDelegateUserbase) registry.lookup("userbase");
                    if (rmi.isUp()) {
                        break; // if we get here, then we are reconnected (since it didn't throw any exceptions)
                    }
                } catch (RemoteException e) {
                } catch (NotBoundException e) {
                } catch (Exception e) {
                    Logging.getErrorLog().reportCritical("Something unexpected went wrong when trying to connect to the userbase: " + e.getClass() + ':' + e.getMessage());
                }
                try {
                    Thread.sleep(retry * 1000);
                } catch (InterruptedException e) {
                    System.err.println(e.getMessage());
                    Logging.getErrorLog().reportError(e.getMessage());
                }
            }
            Server.getInstance().unsuspend();
            System.err.println("Remote userbase reconnected. time taken: " + ((System.currentTimeMillis() - start) / 1000) + " seconds");
            Logging.getErrorLog().reportError("Remote userbase reconnected. time taken: " + ((System.currentTimeMillis() - start) / 1000) + " seconds");
        }
    }


}
