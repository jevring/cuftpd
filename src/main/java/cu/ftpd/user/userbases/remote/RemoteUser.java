/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.remote;

import cu.ftpd.user.User;
import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.user.userbases.remote.client.RemoteUserbase;

import java.util.Collection;
import java.util.Set;

/**
 * @author Markus Jevring
 * @since 2007-maj-13 : 23:31:45
 * @version $Id: RemoteUser.java 258 2008-10-26 12:47:23Z jevring $
 */
public class RemoteUser implements User {
    private final String username;
    private final RemoteUserbase userbase;

    // this user object will always ask the userbase for the property it is after. always.
    public RemoteUser(String username, RemoteUserbase userbase) {
        this.userbase = userbase;
        this.username = username;
    }

    public void setTagline(String tagline) {
        userbase.setProperty(username, "tagline", tagline);
    }
    public void setSuspended(boolean suspended) {
        userbase.setProperty(username, "suspended", suspended);
    }
    public void setLogins(int logins) {
        userbase.setProperty(username, "logins", logins);
    }
    public void setLeech(boolean hasLeech) {
        userbase.setProperty(username, "ratio", hasLeech);
    }
    public void setLastlog(long lastlog) {
        userbase.setProperty(username, "lastlog", lastlog);
    }
    public void setHidden(boolean hidden) {
        userbase.setProperty(username, "hidden", hidden);
    }

    public String getHashedPassword() {
        return userbase.getStringProperty(username, "password");
    }
    public String getUsername() {
        return username;
    }
    public String getTagline() {
        return userbase.getStringProperty(username, "tagline");
    }
    public int getLogins() {
        return userbase.getIntProperty(username, "logins");
    }
    public boolean hasLeech() {
        return userbase.getBooleanProperty(username, "leech");
    }
    public long getCreated() {
        return userbase.getLongProperty(username, "created");
    }
    public long getLastlog() {
        return userbase.getLongProperty(username, "lastlog");
    }
    public boolean isHidden() {
        return userbase.getBooleanProperty(username, "hidden");
    }
    public boolean isSuspended() {
        return userbase.getBooleanProperty(username, "suspended");
    }

    // credits:
    public void setCredits(long credits) {
        throw new IllegalStateException("Can't do a hard set of credits on a remote userbase");
    }
    public void takeCredits(long credits) {
        // the (remote) user-object takes care of synchronizing access to the credits
        userbase.takeCredits(username, credits);
    }
    public void giveCredits(long credits) {
        // the (remote) user-object takes care of synchronizing access to the credits
        userbase.giveCredits(username, credits);
    }
    public long getCredits() {
        // the (remote) user-object takes care of synchronizing access to the credits
        return userbase.getCredits(username);
    }

    // ips:
    public void addIp(String ip) {
        userbase.setProperty(username, "addip", ip);
    }
    public void delIp(String pattern) {
        userbase.setProperty(username, "delip", pattern);
    }
    @SuppressWarnings("unchecked") // we know what we're returning
    public Collection<String> getIps() {
        return (Collection<String>)userbase.getCollection(username, "ips");
    }

    // permissions
    public void addPermission(int permission) {
        userbase.setProperty(username, "addpermission", permission);
    }
    public void delPermission(int permission) {
        userbase.setProperty(username, "delpermission", permission);
    }
    public String getPermissions() {
        return userbase.getStringProperty(username, "permissions");
    }
    public boolean hasPermission(int permission) {
        try {
            return userbase.getBoolean("hasPermission", username, String.valueOf(permission));
        } catch (NoSuchUserException e) {
            return false;
        }
    }

    // gadmin:
    public void addGadminForGroup(String group) {
        userbase.setProperty(username, "addgadminforgroup", group);
    }
    public void removeGadminForGroup(String group) {
        userbase.setProperty(username, "removegadminforgroup", group);
    }
    @SuppressWarnings("unchecked") // we know what we're returning
    public Set<String> getGadminGroups() {
        return (Set<String>)userbase.getCollection(username, "gadmingroups");
    }
    public boolean isGadminForGroup(String group) {
        // this will only be called in 'site gadduser' and 'site group change ...', which happens rarely enough for performance to not be a problem.
        try {
            return userbase.getBoolean("isGadminForGroup", username, group);
        } catch (NoSuchUserException e) {
            return false;
        }
    }

    // groups:
    public void addGroup(String group) {
        // we use the groups as tags or labels, to give the user properties
        userbase.setProperty(username, "autg", group);
    }
    public void removeGroup(String group) {
        userbase.setProperty(username, "rufg", group);
    }
    public boolean isMemberOfGroup(String group) {
        // this is rarely called, so execution is allowed to be marginally slower
        try {
            return userbase.getBoolean("isMemberOfGroup", username, group);
        } catch (NoSuchUserException e) {
            return false;
        }
    }
    @SuppressWarnings("unchecked") // we know what we're returning
    public Set<String> getGroups() {
        return (Set<String>)userbase.getCollection(username, "groups");
    }
    public String getPrimaryGroup() {
        return userbase.getPrimaryGroup(username);
    }
    public void setPrimaryGroup(String primaryGroup) {
        userbase.setProperty(username, "primarygroup", primaryGroup);
    }

    public void passwd(String password) {
        userbase.setProperty(username, "password", password);
    }
}
