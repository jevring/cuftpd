/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.remote.client;

import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.user.userbases.Userbase;

import java.util.Collection;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-10 : 11:11:38
 * @version $Id: RemoteUserbase.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface RemoteUserbase extends Userbase {

    // generic properties
    public String getStringProperty(String username, String property);
    public boolean getBooleanProperty(String username, String property);
    public long getLongProperty(String username, String property);
    public int getIntProperty(String username, String property);
    public void setProperty(String username, String property, String value);
    public void setProperty(String username, String property, long value);
    public void setProperty(String username, String property, int value);
    public void setProperty(String username, String property, boolean value);

    public void setGroupProperty(String groupname, String property, String value);
    public String getGroupProperty(String groupname, String property);

    // NOTE: these three must use the same lock at the server. (No need for set credit, as it will never be used from the ftpd anyway, just for filling)
    // credits
    public void takeCredits(String username, long credits);
    public void giveCredits(String username, long credits);
    public long getCredits(String username);

    public Collection getCollection(String username, String collection);
    public String getPrimaryGroup(String username);
    /**
     * Provides with an easy way of examining 2-arg boolean predicates in the remote userbase.<br>
     * Examples in infix form:<br>
     *  - username isMemberOfGroup groupname<br>
     *  - username isGadminForGroup groupname<br>
     *  - username hasPermission permission<br>
     *
     * @param predicate the boolean predicate we are after.
     * @param arg1 the first argiment.
     * @param arg2 the second argument.
     * @throws cu.ftpd.user.userbases.NoSuchUserException if the specified user can't be found.
     * @return the boolean value for the predicate with the provided arguments.
     */
    public boolean getBoolean(String predicate, String arg1, String arg2) throws NoSuchUserException;
}
