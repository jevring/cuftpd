/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.remote.server;

import cu.authentication.AuthenticationRequest;
import cu.ftpd.user.User;
import cu.ftpd.user.groups.Group;
import cu.ftpd.user.groups.GroupExistsException;
import cu.ftpd.user.groups.NoSuchGroupException;
import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.user.userbases.local.Allotment;
import cu.ftpd.user.userbases.local.LeechEntry;
import cu.ftpd.user.userbases.UserExistsException;
import cu.ftpd.user.userbases.GroupLimitReachedException;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-sep-04 : 13:17:27
 * @version $Id: RmiDelegateUserbase.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface RmiDelegateUserbase extends Remote {
    public static final int DEFAULT = 1;
    public static final int RMI = 2;
    public static final int ANONYMOUS = 3;
    public static final int SQL = 6;

    public void takeCredits(String username, long credits) throws RemoteException;
    public void giveCredits(String username, long credits) throws RemoteException;
    public long getCredits(String username) throws RemoteException;

    public void setProperty(String username, String property, String value) throws RemoteException;
    public String getProperty(String username, String property) throws RemoteException;
    public Collection getCollection(String username, String collection) throws RemoteException;

    public String getPrimaryGroup(String username) throws RemoteException;
    /**
     * Provides with an easy way of examining 2-arg boolean predicates in the remote userbase.
     * Examples in infix form:<br>
     *  - username isMemberOf groupname<br>
     *  - username isGadminForGroup groupname<br>
     *  - username hasPermission permission<br>
     *  - null userExists username<br>
     *  - null groupExists groupname<br>
     *<br>
     * @param predicate the boolean predicate we are after.
     * @param arg1 the first argiment.
     * @param arg2 the second argument.
     * @return the boolean value for the predicate with the provided arguments
     * @throws java.rmi.RemoteException if something goes wrong with the RMI call
     * @throws cu.ftpd.user.userbases.NoSuchUserException if the user in question is not found
     */
    public boolean getBoolean(String predicate, String arg1, String arg2) throws RemoteException, NoSuchUserException;

    public void deleteUser(String username) throws RemoteException, NoSuchUserException;

    public String createHashedPassword(String password) throws RemoteException;
    public int authenticate(AuthenticationRequest auth) throws RemoteException;
    public void deleteGroup(String groupname) throws NoSuchGroupException, RemoteException;
    public Group getGroup(String name) throws NoSuchGroupException, RemoteException;

    // these must all be synchronized
    /**
     * Creates a user and adds it to the specified group.
     *
     * @param groupname the name of the group in which to add the user.
     * @param username the username of the user to add.
     * @param password the (cleartext) password of the user.
     * @param checkSpace if set to <tt>true</tt> the space in the group will be checked before adding.
     * @return the newly created user, or <tt>null</tt> if <tt>checkSpace</tt> was set to <tt>true</tt> and the group was already full.
     * @throws cu.ftpd.user.groups.NoSuchGroupException if the specified group doesn't exist.
     * @throws UserExistsException if there is already a user with the specified username.
     * @throws java.rmi.RemoteException if something goes wrong with the RMI connection
     * @throws cu.ftpd.user.userbases.GroupLimitReachedException if the group did not have any slots left.
     */
    public String gAddUser(String groupname, String username, String password, boolean checkSpace) throws NoSuchGroupException, UserExistsException, RemoteException, GroupLimitReachedException;
    public void renameGroup(String oldName, String newName) throws NoSuchGroupException, GroupExistsException, RemoteException;
    public Group createGroup(String name, String description) throws GroupExistsException, RemoteException;
    public String addUser(String username, String password) throws UserExistsException, RemoteException;

    public String getGroupProperty(String groupname, String property) throws RemoteException;

    public void setGroupProperty(String groupname, String property, String value) throws RemoteException;

    /**
     * This method is polled if the remote userbase goes down. Failure to invoke means we loop again.
     * @return always true.
     * @throws java.rmi.RemoteException if the connection isn't working. equivalent to returning false, since this is what we want to test.
     */
    public boolean isUp() throws RemoteException;

    public boolean setLeechForUser(boolean leech, String username, String groupname, boolean checkAvailability) throws RemoteException, NoSuchGroupException, NoSuchUserException;

    public boolean setAllotmentForUser(long allotment, String username, String groupname, boolean checkAvailability) throws RemoteException, NoSuchGroupException, NoSuchUserException;

    public boolean userUsesLeechSlotForGroup(String username, String groupname) throws RemoteException;

    public boolean userUsesAllotmentSlotForGroup(String username, String groupname) throws RemoteException;

    public List<Allotment> getAllotments(String groupname) throws RemoteException;

    public List<LeechEntry> getLeechers(String groupname) throws RemoteException;

    public void saveGroups() throws RemoteException;
}
