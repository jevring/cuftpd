package cu.ftpd.user.userbases;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-nov-23 : 22:12:02
 * @version $Id: AuthenticationResponses.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface AuthenticationResponses {
    public static final int OK = 0;
    public static final int BAD_PASSWORD = -2;
    public static final int BAD_IDENT_OR_HOST = -3;
    public static final int NO_SUCH_USER = -4;
    public static final int USER_SUSPENDED = -5;
    public static final int USERBASE_NOT_AVAILABLE = -6;
}
