/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-14 : 00:06:16
 * @version $Id: PasswordHasher.java 286 2008-12-06 00:36:20Z jevring $
 */
public class PasswordHasher {
    public static String cuftpdHash(char[] password, byte[] salt) {
        return jce_hmac_sha1(password, salt);
    }

/*
    public static String hmac_sha1(char[] password, byte[] salt) {
        // they key to getting it to work is this PDKDF2 deal. I must admit, I didn't read up 100% on how it works,
        // I just know that it does, and that it's verified. I got the information from the supplied PassChk.c, and tool
        // my hints on how this should be solved from there, and traces the OpenSSL calls.
        // I had to use the gnu-crypt library, because they had already built a function that implements this system.
        // Thank you gnu! =)
        IMac mac = MacFactory.getInstance("HMAC-SHA1");

        // Password Based Key Derivation Function 2
        PBKDF2 kdf = new PBKDF2(mac);
        HashMap attributes = new HashMap();

        // iterate 100 times.
        attributes.put(IPBE.ITERATION_COUNT, new Integer(100));
        attributes.put(IPBE.PASSWORD, password);
        attributes.put(IPBE.SALT, salt);
        kdf.init(attributes);
        try {
            byte[] result = new byte[20];
            kdf.nextBytes(result, 0, result.length);
            return new String(Hex.bytesToHex(result));
        } catch (LimitReachedException e) {
            Logging.getErrorLog().reportError("Error found when hashing password: " + e.getMessage());
        }
        return null;
    }
*/

    public static String jce_hmac_sha1(char[] password, byte[] salt) {
        //System.out.println("password: " + new String(password));
        //System.out.println("salt: " + new String(Hex.bytesToHex(salt)));
        try {
            PBEKeySpec keySpec = new PBEKeySpec(password, salt, 100, 160);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            SecretKey secretKey = keyFactory.generateSecret(keySpec);
            return new String(Hex.bytesToHex(secretKey.getEncoded()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main(String[] args) {
        //Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        // $d40d044b$afc740c13f84398f3de7919f0ecd78474cf509b8
        //           ffb61bf94c41c9a364ae4104ac201dd5e820b7da

        //final String password = "tomte";
        //final String salt = "d40d044b";
        final String password = "test";
        final String salt = "cf10d49d";
        // ideally, these should deliver the same output, but they don't!
//        String gnu = hmac_sha1(password.toCharArray(), Hex.hexToBytes(salt));
//        System.out.println("gnu: " + gnu);
        String jce = jce_hmac_sha1(password.toCharArray(), Hex.hexToBytes(salt));
        System.out.println("jce: " + jce);
        /*
        String bc = bc_hmac_sha1(password.toCharArray(), Hex.hexToBytes(salt), 160);
        System.out.println("bc:  " + bc);
          */
        /*
        for (int i = 8; i <= 4096; i += 8) {
            String jce = jce_hmac_sha1(password.toCharArray(), Hex.hexToBytes(salt), i);
            System.out.println(i + "=" + jce);
            if (jce.equals(gnu)) {
                System.out.println("OK");
                return;
            }
        }
        */
        //System.out.println(hmac_sha1(password.toCharArray(), Hex.hexToBytes(salt)));
/*
        for (int i = 0; i < Security.getProviders().length; i++) {
            Provider provider = Security.getProviders()[i];
            System.out.println(provider.getName() + ":" + provider.getInfo() + ":" + provider.toString());
        }
*/        
    }
}
