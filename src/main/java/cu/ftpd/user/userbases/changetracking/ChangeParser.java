package cu.ftpd.user.userbases.changetracking;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author markus@jevring.net
 */
public class ChangeParser {
    private static final Pattern userChange = Pattern.compile("UserChange\\{user=(.+), modification=(.+), property=(.+), value=(.+)\\}");
    private static final Pattern userbaseChange = Pattern.compile("UserbaseChange\\{property=(.+), modification=(.+), parameters=\\[(.+)\\]}");
    private static final Pattern groupChange = Pattern.compile("GroupChange\\{property=(.+), group=(.+), parameter=(.+)\\}");

    /**
     * Parses the string and recreates the corresponding change.
     *
     * @param representation the string representation.
     * @param peer the name of the peer from where this change comes.
     * @see #toString()
     * @return a change corresponding to the string representation
     */
    public Change parse(String representation, String peer) {
        Change c;
        c = parseUserChange(representation, peer);
        if (c == null) {
            c = parseUserbaseChange(representation, peer);
            if (c == null) {
                c = parseGroupChange(representation, peer);
                if (c == null) {
                    throw new IllegalArgumentException("Could not parse '" + representation + "'");
                }
            }
        }

        // parse the string here and return either a UserChange or a UserbaseChange or some other kind of change...
        return c;
    }

    private Change parseUserChange(String representation, String peer) {
        Matcher m = userChange.matcher(representation);
        if (m.matches()) {
            String username = m.group(1);
            Modification modification = Modification.valueOf(m.group(2));
            UserChange.Property property = UserChange.Property.valueOf(m.group(3));
            String value = m.group(4);
            return new UserChange(username, modification, property, value, peer);
        } else {
            return null;
        }
    }

    private Change parseGroupChange(String representation, String peer) {
        Matcher m = groupChange.matcher(representation);
        if (m.matches()) {
            GroupChange.Property property = GroupChange.Property.valueOf(m.group(1));
            String group = m.group(2);
            String parameter = m.group(3);
            return new GroupChange(property, group, parameter, peer);
        } else {
            return null;
        }
    }

    private Change parseUserbaseChange(String representation, String peer) {
        Matcher m = userbaseChange.matcher(representation);
        if (m.matches()) {
            UserbaseChange.Property property = UserbaseChange.Property.valueOf(m.group(1));
            Modification modification = Modification.valueOf(m.group(2));
            String[] parameters = m.group(3).split(",\\s*");
            return new UserbaseChange(peer, property, modification, parameters);
        } else {
            return null;
        }
    }
}
