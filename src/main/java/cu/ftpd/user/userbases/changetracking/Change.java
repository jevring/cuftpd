package cu.ftpd.user.userbases.changetracking;

/**
 * @author markus@jevring.net
 */
public interface Change {
    public String getPeer();
    @Override
    public String toString();
}
