package cu.ftpd.user.userbases.changetracking;

/**
 * @author markus@jevring.net
 */
public interface ChangeTracker {
    void addChange(Change change);
}
