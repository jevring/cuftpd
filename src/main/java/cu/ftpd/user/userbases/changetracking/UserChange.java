package cu.ftpd.user.userbases.changetracking;

/**
 * @author markus@jevring.net
 */
public class UserChange implements Change {
    public static enum Property {
        Credits,
        Tagline,
        Suspended,
        Logins,
        Leech,
        LastLog,
        Hidden,
        Ip,
        Permission,
        GroupAdmin,
        Group,
        PrimaryGroup,
        HashedPassword
    }

    private final String username;
    private final Modification modification;
    private final Property property;
    private final String value;
    private final String peer;

    UserChange(String username, Modification modification, Property property, Object value, String peer) {
        this.username = username;
        this.modification = modification;
        this.property = property;
        this.value = String.valueOf(value);
        this.peer = peer; // this doesn't come from the outside.
    }

    public UserChange(String username, Modification modification, Property property, Object value) {
        this(username, modification, property, String.valueOf(value), null);
    }

    public String getUsername() {
        return username;
    }

    public Modification getModification() {
        return modification;
    }

    public Property getProperty() {
        return property;
    }

    public String getValue() {
        return value;
    }

    public String getPeer() {
        return peer;
    }

    @Override
    public String toString() {
        return "UserChange{" + "user=" + username + ", modification=" + modification + ", property=" + property + ", value=" + value + '}';
    }
}
