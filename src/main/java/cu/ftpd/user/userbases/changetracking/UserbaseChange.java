package cu.ftpd.user.userbases.changetracking;

import java.util.Arrays;

/**
 * @author markus@jevring.net
 */
public class UserbaseChange implements Change {
    public static enum Property {
        User, Group
        // NOTE: GAddUser is handled by adduser+addgroup+setprimarygroup
    }

    private final Property property;
    private final Modification modification;
    private final String peer;
    private final String[] parameters;

    public UserbaseChange(String peer, Property property, Modification modification, String... parameters) {
        this.property = property;
        this.modification = modification;
        this.peer = peer;
        this.parameters = parameters;
    }

    public UserbaseChange(Property property, Modification modification, String... parameters) {
        this(null, property, modification, parameters);
    }

    public Property getProperty() {
        return property;
    }

    public Modification getModification() {
        return modification;
    }

    public String getPeer() {
        return peer;
    }

    public String[] getParameters() {
        return parameters;
    }

    @Override
    public String toString() {
        return "UserbaseChange{property=" + property + ", modification=" + modification + ", parameters=" + (parameters == null ? null : Arrays.asList(
                parameters)) + '}';
    }
}
