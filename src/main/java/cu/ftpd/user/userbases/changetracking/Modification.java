package cu.ftpd.user.userbases.changetracking;

/**
 * @author markus@jevring.net
 */
public enum Modification {
    Set,
    Add,
    Remove
}
