/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.Sort;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Formatter;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.NoSuchUserException;

import java.util.Set;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-27 : 00:02:29
 * @version $Id: Who.java 300 2010-03-09 20:48:19Z jevring $
 */
public class Who extends UserbaseAction {
    private static final String lineFormat = "200- %-1s %-16s %-12s %-40s  %-1s";

    public Who() {
        super("who");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (user.hasPermission(UserPermission.WHO)) {
            boolean raw = "-raw".equalsIgnoreCase(parameterList[parameterList.length - 1]);
            Set<Connection> conns = Server.getInstance().getConnections();
            // _todo: this lock is useless for some reason. it should point to the same object that locks un the server
            // tests show that we probably do hold the lock after all, which makes this NullPointerException that much more difficult to understand
            synchronized(conns) {
                if (raw) {
                    connection.respond("200- SITE WHO RAW (username;group;status;speed_in_kbps;idletime;command)");
                } else {
                    connection.respond(createHeader());
                    connection.respond(String.format(lineFormat, Formatter.getBar(), "User", "Group", "Action", Formatter.getBar()));
                    connection.respond(Formatter.createLine(200));
                }
	            for (Connection c : Sort.connections(conns)) {
                    User u = c.getUser();

                    // not listing users that are not logged in might cause problems if people connect and then are orphaned. the site might look empty
                    // but people still can't get in
                    // we will list these connections in xwho (or whatever we'll call it)
                    // so, the NullPointerException we got here was when the connection was being reconnected automatically, and
                    // we did a "site who" before there was a User object available, and thus we got a NullpointerException
                    
                    if (u != null) { // don't list connections where the user hasn't logged in yet
                        if ((u.isHidden() && !user.hasPermission(UserPermission.SEE_HIDDEN)) || !ServiceManager.getServices().getPermissions().canSee(user, u, c.getCurrentDir())) {
                            // hide user if user has the property hidden, or the directory in which the user operates is hidden
                            continue;
                        }
                        if (c.isUploading()) {
                            if (raw) {
                                connection.respond(u.getUsername() + ";" + u.getPrimaryGroup() + ";uploading;" + c.getCurrentSpeed() + ";" + c.getIdleTime() + ";" + escape(c.getLastCommand()));
                            } else {
                                connection.respond(String.format(lineFormat, Formatter.getBar(), u.getUsername(), u.getPrimaryGroup(), c.getLastCommand() + " @ " + Formatter.speedFromKBps(c.getCurrentSpeed()), Formatter.getBar()));
                            }
                        } else if (c.isDownloading()) {
                            if (raw) {
                                connection.respond(u.getUsername() + ";" + u.getPrimaryGroup() + ";downloading;" + c.getCurrentSpeed() + ";" + c.getIdleTime() + ";" + escape(c.getLastCommand()));
                            } else {
                                connection.respond(String.format(lineFormat, Formatter.getBar(), u.getUsername(), u.getPrimaryGroup(), c.getLastCommand() + " @ " + Formatter.speedFromKBps(c.getCurrentSpeed()), Formatter.getBar()));
                            }
                        } else {
                            if (raw) {
                                connection.respond(u.getUsername() + ";" + u.getPrimaryGroup() + ";idle;" + c.getCurrentSpeed() + ";" + c.getIdleTime() + ";" + escape(c.getLastCommand()));
                            } else {
                                connection.respond(String.format(lineFormat, Formatter.getBar(), u.getUsername(), u.getPrimaryGroup(), "idle for " + Formatter.shortDuration(c.getIdleTime()), Formatter.getBar()));
                            }
                        }
                    }
                }
                if (raw) {
                    connection.respond("200 SITE WHO RAW");
                } else {
                    connection.respond(Formatter.createFooter(200));
                }
            }
         } else {
             connection.respond("531 Permission denied.");
         }
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
