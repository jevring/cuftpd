/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.logging.Formatter;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.NoSuchUserException;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:46:31
 * @version $Id: ModifyPermissions.java 258 2008-10-26 12:47:23Z jevring $
 */
public class ModifyPermissions extends UserbaseAction {
    protected final boolean add;

    public ModifyPermissions(boolean add) {
        this.add = add;
        if (add) {
            name = "addpermissions";
        } else {
            name = "delpermissions";
        }
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (user.hasPermission(UserPermission.USEREDIT)) {
            LinkedList<Integer> permissions = new LinkedList<Integer>();
            String wrongPermissions = "";
            String correctPermissions = "";
            try {
                permissions.add(Integer.parseInt(parameterList[2]));
                correctPermissions = parameterList[2] + ' ';
            } catch (NumberFormatException e) {
                // wasn't a number, try to get a list from the command
            }
            if (permissions.isEmpty()) {
                // this means that we got something that was not a number
                String perms = Formatter.join(parameterList, 2, parameterList.length, "");
                String[] numbers = perms.split("\\s?,\\s?");
                for (int i = 0; i < numbers.length; i++) {
                    String number = numbers[i].trim();
                    try {
                        permissions.add(Integer.parseInt(number));
                        correctPermissions += number + ' ';
                    } catch (NumberFormatException e) {
                        wrongPermissions += number + ' ';
                    }
                }
            }
            int loc;
            List<User> users = new LinkedList<User>();
            if ("*".equals(parameterList[1])) {
                users.addAll(ServiceManager.getServices().getUserbase().getUsers().values());
            } else if (parameterList[1].startsWith("*") && (loc = parameterList[1].indexOf('@')) > -1) {
                String groupname = parameterList[1].substring(loc+1);
                for (User u : ServiceManager.getServices().getUserbase().getUsers().values()) {
                    if (u.isMemberOfGroup(groupname)) {
                        users.add(u);
                    }
                }
            } else {
                try {
                    users.add(ServiceManager.getServices().getUserbase().getUser(parameterList[1]));
                } catch (NoSuchUserException e) {
                    connection.respond("500 " + e.getMessage());
                    return;
                }
            }
            for (User u : users) {
                for (int permission : permissions) {
                    // NOTE: even though the site command can handle adding multiple permissions, the user object still only accepts one permission at a time, so there are no changes to the userbase
                    if (add) {
                        u.addPermission(permission);
                    } else {
                        u.delPermission(permission);
                    }
                }
            }
            if (wrongPermissions.length() > 0) {
                connection.respond("200- The following permissions were not numbers, and as such were not added: " + wrongPermissions);
            }
            connection.respond("200 Permissions " + correctPermissions + (add ? "given." : "removed."));
        } else {
            connection.respond("531 (Your) Permission denied.");
        }
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
