/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.logging.Formatter;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.userbases.NoSuchUserException;

import java.util.List;
import java.util.LinkedList;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-27 : 00:00:01
 * @version $Id: Credits.java 299 2009-03-09 20:39:48Z jevring $
 */
public class Credits extends UserbaseAction {
    protected final boolean give;
    public Credits(boolean give) {
        super((give ? "give" : "take"));
        this.give = give;
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (user.hasPermission(UserPermission.CREDITS)) {
            int loc;
            List<User> users = new LinkedList<User>();
            if ("*".equals(parameterList[1])) {
                users.addAll(ServiceManager.getServices().getUserbase().getUsers().values());
            } else if (parameterList[1].startsWith("*") && (loc = parameterList[1].indexOf('@')) > -1) {
                String groupname = parameterList[1].substring(loc+1);
                for (User u : ServiceManager.getServices().getUserbase().getUsers().values()) {
                    if (u.isMemberOfGroup(groupname)) {
                        users.add(u);
                    }
                }
            } else {
                try {
                    users.add(ServiceManager.getServices().getUserbase().getUser(parameterList[1]));
                } catch (NoSuchUserException e) {
                    connection.respond("500 " + e.getMessage());
                    return;
                }
            }
            long credits;
            try {
                credits = Formatter.size(parameterList[2]);
            } catch (NumberFormatException e) {
                connection.respond("500 Please supply a number smaller than " + Long.MAX_VALUE);
                return;
            }
            for (User u : users) {
                if (give) {
                    u.giveCredits(credits);
                } else {
                    u.takeCredits(credits);
                }
            }
            connection.respond("200 Credits " + (give ? "given." : "removed."));
        } else {
            connection.respond("531 Permission denied.");
        }
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
