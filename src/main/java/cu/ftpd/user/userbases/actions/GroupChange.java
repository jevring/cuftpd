/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Formatter;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import cu.ftpd.user.groups.Group;
import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.user.groups.NoSuchGroupException;

import java.util.List;
import java.util.LinkedList;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:50:33
 * @version $Id: GroupChange.java 280 2008-11-24 18:52:18Z jevring $
 */
public class GroupChange extends UserbaseAction {
    public GroupChange() {
        super("groupchange");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (user.hasPermission(UserPermission.GROUPS)) {
            List<Group> groups = new LinkedList<Group>();
            if ("*".equals(parameterList[1])) {
                groups.addAll(ServiceManager.getServices().getUserbase().getGroups().values());
            } else {
                try {
                    groups.add(ServiceManager.getServices().getUserbase().getGroup(parameterList[1]));
                } catch (NoSuchGroupException e) {
                    connection.respond("500 " + e.getMessage());
                    return;
                }
            }
            String description = "";
            for (Group g : groups) {
                try {
                    if ("slots".equals(parameterList[2])) {
                        g.setSlots(Integer.parseInt(parameterList[3]));
                    } else if ("leechslots".equals(parameterList[2])) {
                        g.setLeechSlots(Integer.parseInt(parameterList[3]));
                    } else if ("allotmentslots".equals(parameterList[2])) {
                        g.setAllotmentSlots(Integer.parseInt(parameterList[3]));
                    } else if ("maxallotment".equals(parameterList[2])) {
                        g.setMaxAllotment(Long.parseLong(parameterList[3]));
                    } else if ("description".equals(parameterList[2])) {
                        description = Formatter.join(parameterList, 3, parameterList.length, " ");
                        g.setDescription(description);

                    } else {
                        connection.respond("500 Trying to change unknown property, accepted properties are: slots, leechslots, allotmentslots, maxallotment, description.");
                        return;
                    }
                } catch (NumberFormatException e) {
                    connection.respond("500 " + parameterList[3] + " is not a number");
                    return; // we want this to stop on the first error
                }
            }
            connection.respond("200 Group property '" + parameterList[2] + "' set to '" + ("description".equals(parameterList[2]) ? description : parameterList[3]) + "' for group '" + parameterList[1] + '\'');
            ServiceManager.getServices().getUserbase().saveGroups();
        } else {
            connection.respond("531 Permission denied.");
        }
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
