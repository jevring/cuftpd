package cu.ftpd.user.userbases.actions;

import cu.ftpd.commands.site.actions.Action;
import cu.ftpd.user.User;
import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;

/**
 * @author captain
 * @version $Id: UserbaseModification.java 258 2008-10-26 12:47:23Z jevring $
 * @since 2008-okt-24 - 23:20:17
 */
public abstract class UserbaseAction extends Action {
    //private final Userbase userbase;

    protected UserbaseAction(String name) {
        super(name);
    }

    protected UserbaseAction() {
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
