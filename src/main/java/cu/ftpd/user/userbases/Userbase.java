/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases;

import cu.authentication.Authenticator;
import cu.ftpd.user.User;
import cu.ftpd.user.groups.Group;
import cu.ftpd.user.groups.GroupExistsException;
import cu.ftpd.user.groups.NoSuchGroupException;
import cu.ftpd.user.userbases.local.Allotment;
import cu.ftpd.user.userbases.local.LeechEntry;
import cu.ftpd.user.userbases.UserExistsException;

import java.util.List;
import java.util.Map;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-07 : 20:51:46
 * @version $Id: Userbase.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface Userbase extends Authenticator {
    public static final int DEFAULT = 1;
    public static final int RMI = 2;
    public static final int ANONYMOUS = 3;
    public static final int ASYNCHRONOUS = 4;
    public static final int SQL = 6;

    public User getUser(String username) throws NoSuchUserException;
    public Group getGroup(String name) throws NoSuchGroupException;
    public Map<String, ? extends User> getUsers();
    public String createHashedPassword(String password);
    public void deleteGroup(String groupname) throws NoSuchGroupException;
    public void deleteUser(String username) throws NoSuchUserException;

    // these must all be synchronized
    /**
     * Creates a user and adds it to the specified group.
     * The userbase takes care of hashing the password. It's safe to send if here in cleartext, because we are either
     * in memory or on an encrypted socket.
     *
     * @param groupname the name of the group in which to add the user.
     * @param username the username of the user to add.
     * @param password the (CLEARTEXT) password of the user.
     * @param checkSpace if set to <tt>true</tt> the space in the group will be checked before adding.
     * @return the newly created user, or <tt>null</tt> if <tt>checkSpace</tt> was set to <tt>true</tt> and the group was already full.
     * @throws cu.ftpd.user.groups.NoSuchGroupException if the specified group doesn't exist.
     * @throws UserExistsException if there is already a user with the specified username.
     * @throws cu.ftpd.user.userbases.GroupLimitReachedException if the user does not have space for more users.
     */
    public User gAddUser(String groupname, String username, String password, boolean checkSpace) throws NoSuchGroupException, UserExistsException, GroupLimitReachedException;

    /**
     * Creates a user.
     * The userbase takes care of hashing the password. It's safe to send if here in cleartext, because we are either
     * in memory or on an encrypted socket.
     *
     * @param username the username of the user
     * @param password the (CLEARTEXT) password for the user.
     * @return the newly created user.
     * @throws UserExistsException if the specified user already exists.
     */
    public User addUser(String username, String password) throws UserExistsException;

    /**
     * Creates a group with 0 slots.
     *
     * @param name        the name of the group.
     * @param description the description of the group.
     * @throws cu.ftpd.user.groups.GroupExistsException thrown if the group we are trying to create already exists
     * @return The created group.
     */
    public Group createGroup(String name, String description) throws GroupExistsException;
    public void renameGroup(String oldName, String newName) throws NoSuchGroupException, GroupExistsException;
    public Map<String, ? extends Group> getGroups();

    /**
     * Sets the ratio for the specified user in the specified group.
     * If a group is provided and <code>checkAvailability</code> is set to <code>true</code> and <code>ratio</code> is set to <code>0</code> then a check is performed.
     * This check counts the number of "ratio 0" users currently in the group, and compares this to the max number of allowed "ratio 0" users.
     * If the current number is less than the allowed, the ratio for the user is changed.
     * If not, then this will return false.
     *
     * NOTE: This function MUST lock on some object to prevent simultaneous update of the users (which could be exploited to grant more leech slots than intended)
     *
     * @param leech true if the user should be able to download without loosing credits, false otherwise.
     * @param username the user to set the ratio for.
     * @param groupname the group from which to use possible leech slots.
     * @param checkAvailability true if we check if there are any leech slots available in the group, if applicable. @return true if the ratio was successfully set for the user, false otherwise. (See above) @throws NoSuchUserException thrown if the specified user can't be found
     * @throws cu.ftpd.user.groups.NoSuchGroupException thrown if the specified group can't be found
     * @return true if the operation was succesful, false otherwise
     * @throws cu.ftpd.user.userbases.NoSuchUserException if the specified user could not be found
     */
    public boolean setLeechForUser(boolean leech, String username, String groupname, boolean checkAvailability) throws NoSuchUserException, NoSuchGroupException;

    /**
     * Sets the allotment for the specified user in the specified group.
     * If a group is provided and <code>checkAvailability</code> is set to <code>true</code> then a check is performed.
     * This check counts the number of allotment users already present in the group, and if that is more than the number of allowed allotment users, it will return <code>false</code>.
     * It also checks that the allotment is within the accpetable range set for the group in question.
     *
     * NOTE: This function MUST lock on some object to prevent simultaneous update of the users (which could be exploited to grant more allotment slots than intended)
     *
     * @param allotment the amount of credits to allot every week.
     * @param username the username to which to allot the credits.
     * @param groupname the groupname for which we are checking the slots.
     * @param checkAvailability true if we are doing checks as per above, false otherwise.
     * @return true if the allotment was set to the user properly, false otherwise.
     * @throws cu.ftpd.user.userbases.NoSuchUserException thrown if the specified user can't be found
     * @throws cu.ftpd.user.groups.NoSuchGroupException thrown if the specified group can't be found
     */
    public boolean setAllotmentForUser(long allotment, String username, String groupname, boolean checkAvailability) throws NoSuchUserException, NoSuchGroupException;

    /**
     * Checks the Leechers object to see if this user uses a leech slot for the specified group.
     *
     * @param username the name of the user.
     * @param groupname the group in which to check.
     * @return true if the user uses a leech slot for the specified group.
     */
    public boolean userUsesLeechSlotForGroup(String username, String groupname);

    /**
     * Checks the Allotments object to see if this user users an allotment slot for the specified group.
     * 
     * @param username the username of the user.
     * @param groupname the group in which to check.
     * @return true if the user uses an allotment slot for the specified group.
     */
    public boolean userUsesAllotmentSlotForGroup(String username, String groupname);

    public List<Allotment> getAllotments(String groupname);

    public List<LeechEntry> getLeechers(String groupname);

    public void saveGroups();
}
