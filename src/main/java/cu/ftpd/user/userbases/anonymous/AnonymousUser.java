/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.anonymous;

import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;

import java.util.*;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-nov-23 : 16:59:13
 * @version $Id: AnonymousUser.java 258 2008-10-26 12:47:23Z jevring $
 */
public class AnonymousUser implements User {
    private final String username;
    private static final List<String> ips = new ArrayList<>(1);
    private static final Set<Integer> permissions = new TreeSet<>();
    static {
        ips.add("*@*");
        permissions.add(UserPermission.WHO);
        permissions.add(UserPermission.UPTIME);
        permissions.add(UserPermission.TRAFFIC);
        permissions.add(UserPermission.SEEN);
        permissions.add(UserPermission.STATISTICS);
    }
    public AnonymousUser(String username) {
        this.username = username;
    }

    public void setCreated(long created) {

    }

    public void setTagline(String tagline) {

    }

    public void setSuspended(boolean suspended) {

    }

    public void setLogins(int logins) {

    }

    public void setLeech(boolean hasLeech) {

    }

    public void setLastlog(long lastlog) {

    }

    public void setHidden(boolean hidden) {

    }

    public String getHashedPassword() {
        return null;
    }

    public String getUsername() {
        return username;
    }

    public String getTagline() {
        return "I'm an anonymous user";
    }

    public int getLogins() {
        return Integer.MAX_VALUE;
    }

    public boolean hasLeech() {
        return true;
    }

    public long getCreated() {
        return 0L;
    }

    public long getLastlog() {
        return 0L;
    }

    public boolean isHidden() {
        return false;
    }

    public boolean isSuspended() {
        return false;
    }
    // credits:
    public void setCredits(long credits) {

    }

    public void takeCredits(long credits) {

    }

    public void giveCredits(long credits) {

    }

    public long getCredits() {
        return 0;
    }
    // ip:
    public void addIp(String ip) {

    }

    public void delIp(String pattern) {
        
    }

    public Collection<String> getIps() {
        return ips;
    }
    // permissions:
    public void setPermissions(String permissions) {

    }

    public void addPermission(int permission) {

    }

    public void delPermission(int permission) {

    }

    public String getPermissions() {
        return "";
    }

    public boolean hasPermission(int permission) {
        return permissions.contains(permission);
    }

    // gadmin:
    public void addGadminForGroup(String group) {

    }

    public void removeGadminForGroup(String group) {

    }

    public Set<String> getGadminGroups() {
        return Collections.EMPTY_SET;
    }

    public boolean isGadminForGroup(String group) {
        return false;
    }
    // groups:
    public void addGroup(String group) {

    }

    public void removeGroup(String group) {

    }

    public boolean isMemberOfGroup(String group) {
        return false;
    }

    public Set<String> getGroups() {
        return Collections.EMPTY_SET;
    }

    public String getPrimaryGroup() {
        return "";
    }

    public void setPrimaryGroup(String primaryGroup) {

    }

    public void passwd(String hashedPassword) {

    }
}
