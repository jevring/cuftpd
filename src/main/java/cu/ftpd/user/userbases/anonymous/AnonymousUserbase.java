/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.userbases.anonymous;

import cu.authentication.AuthenticationRequest;
import cu.ftpd.user.User;
import cu.ftpd.user.groups.Group;
import cu.ftpd.user.groups.NoSuchGroupException;
import cu.ftpd.user.groups.GroupExistsException;
import cu.ftpd.user.userbases.NoSuchUserException;
import cu.ftpd.user.userbases.Userbase;
import cu.ftpd.user.userbases.UserExistsException;
import cu.ftpd.user.userbases.local.Allotment;
import cu.ftpd.user.userbases.local.LeechEntry;

import java.util.*;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-15 : 01:21:36
 * @version $Id: AnonymousUserbase.java 286 2008-12-06 00:36:20Z jevring $
 */
public class AnonymousUserbase implements Userbase {
    // _todo: make sure anonymous users can't break things, default to not let them delete things
    // no, this is up to the permission system to decide

    public AnonymousUserbase() {
        // removes this so extending userbases didn't have to suffer from it.
    }

    public User getUser(String username) throws NoSuchUserException {
        return new AnonymousUser(username);
    }

    public void deleteUser(String username) {}

    public String createHashedPassword(String password) {
        return null;
    }

    public User addUser(String username, String password) {
	    return new AnonymousUser(username);
    }

    public boolean setLeechForUser(boolean leech, String username, String groupname, boolean checkAvailability) {
        return false;
    }

    public boolean setAllotmentForUser(long allotment, String username, String groupname, boolean checkAvailability) {
        return false;
    }

    public Map<String, Group> getGroups() {
        return Collections.emptyMap();
    }

    public boolean userUsesLeechSlotForGroup(String username, String groupname) {
        return false;
    }

    public boolean userUsesAllotmentSlotForGroup(String username, String groupname) {
        return false;
    }

    public List<Allotment> getAllotments(String groupname) {
        return Collections.emptyList();
    }

    public void saveGroups() {}

    public List<LeechEntry> getLeechers(String groupname) {
        return Collections.emptyList();
    }

    public void renameGroup(String oldName, String newName) {}

    public Group createGroup(String name, String description) throws GroupExistsException {
        throw new GroupExistsException("<cannot add groups to an anonymous userbase>");
    }

    public void deleteGroup(String groupname) {}

    public Map<String,User> getUsers() {
        return Collections.emptyMap();
    }

    public Group getGroup(String name) throws NoSuchGroupException {
        throw new NoSuchGroupException("Anonymous userbases do not have groups: " + name);
    }
    public User gAddUser(String groupname, String username, String password, boolean checkSpace) throws NoSuchGroupException, UserExistsException {
        throw new UserExistsException("<cannot add users to an anonymous userbase>");
    }

    /**
     * Authenticates this request against some database of users, or according to some algorithm.
     *
     * @param auth the AuthenticationRequest to authenticate
     * @return 0 if the authentication was OK, any other number represents failure. The meaning of this number is dependent on the context in which is was used, i.e. up to the rest of the framework.
     */
    public int authenticate(AuthenticationRequest auth) {
        return 0;
    }
}
