/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.groups;

import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Logging;
import cu.ftpd.user.userbases.changetracking.ChangeTracker;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-aug-19 : 17:23:46
 * @version $Id: GroupManager.java 258 2008-10-26 12:47:23Z jevring $
 */
public class GroupManager {
    private final Map<String, LocalGroup> groups = Collections.synchronizedMap(new HashMap<String, LocalGroup>());
    private final File groupfile;
    private final ChangeTracker changeTracker;

    public GroupManager(File groupfile, ChangeTracker changeTracker) throws IOException {
        this.groupfile = groupfile;
        this.changeTracker = changeTracker;
        BufferedReader in = null;
        if (!groupfile.exists()) {
            System.out.println("Groupfile not found, creating...");
            if (!groupfile.createNewFile()) {
                throw new IOException("Could not create new groupfile!");
            }
            return; // we might as well return here since we don't want to read an empty file anyway
        }
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(groupfile)));
            String line;
            while ((line = in.readLine()) != null) {
                if (line.startsWith("#")) continue;
                String[] parts = line.split(";", 6);
                if (parts.length == 6) {
                    try {
                        groups.put(parts[0], new LocalGroup(parts[0], Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), Integer.parseInt(parts[3]), Long.parseLong(parts[4]), parts[5],
                                                            changeTracker));
                    } catch (NumberFormatException e) {
                        Logging.getErrorLog().reportCritical("Parsing of group line failed because something that was supposed to be a number wasn't: " + e.getMessage() + '(' + line + ')');
                    }
                } else {
                    System.err.println("Skipping erroneous group line; " + line);
                }
            }
        } catch (IOException e) {
            Logging.getErrorLog().reportCritical("Error while loading group file: " + e.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public LocalGroup getGroup(String name) throws NoSuchGroupException {
        LocalGroup g = groups.get(name);
        if (g != null) {
            return g;
        } else {
            throw new NoSuchGroupException(name);
        }
    }
    public Group createGroup(int slots, int leechSlots, int allotmentSlots, long maxAllotment, String name, String description) throws GroupExistsException {
        synchronized(groups) {
            if (groups.containsKey(name)) {
                throw new GroupExistsException(name);
            }
            LocalGroup group = new LocalGroup(name, slots, leechSlots, allotmentSlots, maxAllotment, description, changeTracker);
            groups.put(name, group);
            save();
            return group;
        }
    }

    public void renameGroup(String oldName, String newName) throws NoSuchGroupException, GroupExistsException {
        synchronized(groups) {
            if (groups.keySet().contains(newName)) {
                throw new GroupExistsException(newName);
            } else {
                LocalGroup group = groups.remove(oldName);
                if (group != null) {
                    group.setName(newName);
                    groups.put(newName, group);
                } else {
                    throw new NoSuchGroupException(oldName);
                }
            }
            save();
        }
    }

    public void deleteGroup(String groupname) throws NoSuchGroupException {
        Group g = groups.remove(groupname);
        if (g == null) {
            throw new NoSuchGroupException(groupname);
        }
        save();
    }

    public void save() {
        synchronized(groups) {
            FileSystem.fastBackup(groupfile);
            try (PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(groupfile))))) {
                for (Group g : groups.values()) {
                    out.println(g);
                }
                out.flush();
            } catch (IOException e) {
                Logging.getErrorLog().reportCritical("Failed to save groupfile: " + e.getMessage());
            }

        }
    }

    public Map<String, LocalGroup> getGroups() {
        return groups;
    }
}
