/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user;

import java.util.Collection;
import java.util.Set;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-09 : 22:23:47
 * @version $Id: User.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface User {
    public void setTagline(String tagline);
    public void setSuspended(boolean suspended);
    public void setLogins(int logins);
    public void setLeech(boolean hasLeech);
    public void setLastlog(long lastlog);
    public void setHidden(boolean hidden);
    
    public String getHashedPassword();
    public String getUsername();
    public String getTagline();
    public int getLogins();
    public boolean hasLeech();
    public long getCreated();
    public long getLastlog();
    public boolean isHidden();
    public boolean isSuspended();

    // credits:
    public void setCredits(long credits);
    public void takeCredits(long credits);
    public void giveCredits(long credits);
    public long getCredits();

    // ip:
    public void addIp(String ip);
    public void delIp(String pattern);
    public Collection<String> getIps();

    // permissions:
    public void addPermission(int permission);
    public void delPermission(int permission);
    public String getPermissions();
    public boolean hasPermission(int permission);

    // gadmin:
    public void addGadminForGroup(String group);
    public void removeGadminForGroup(String group);
    public Set<String> getGadminGroups();
    public boolean isGadminForGroup(String group);

    // groups:
    public void addGroup(String group);
    public void removeGroup(String group);
    public boolean isMemberOfGroup(String group);
    public Set<String> getGroups();
    public String getPrimaryGroup();
    public void setPrimaryGroup(String primaryGroup);

    /**
     * This requires a 'finished' password, i.e. hashed or scrambled in some way.
     * @param hashedPassword the scrambled password.
     */
    public void passwd(String hashedPassword);
}
