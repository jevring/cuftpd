package cu.ftpd.user.statistics.none;

import cu.ftpd.user.statistics.UserStatistics;
import cu.ftpd.user.statistics.StatisticsEntry;

import java.util.TreeMap;

/**
 * This is a dummy statistics engine that neither collects nor contains any statistics. Useful for turning statistics off.
 *
 * @author captain
 * @version $Id: NoUserStatistics.java 293 2009-03-04 20:07:38Z jevring $
 * @since 2008-okt-30 - 23:12:50
 */
public class NoUserStatistics implements UserStatistics {
    public void upload(String username, String section, long bytes, long time) {}

    public void download(String username, String section, long bytes, long time) {}

    public StatisticsEntry getUserStatistics(String username, String section) {
        return new StatisticsEntry(username, section);
    }

    public void store(StatisticsEntry statisticsEntry) {}

    public TreeMap<Long, StatisticsEntry> get(int statistics, String section) {
        return new TreeMap<Long, StatisticsEntry>();
    }

    public TreeMap<Long, StatisticsEntry> getGroupStatistics(String groupname, int statistics, String section) {
        return new TreeMap<Long, StatisticsEntry>();
    }

    public void deleteUser(String username) {}
}
