/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.user.statistics.remote.server;

import cu.ftpd.user.statistics.StatisticsEntry;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.TreeMap;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-11 : 19:35:10
 * @version $Id: RmiUserStatistics.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface RmiUserStatistics extends Remote {
    public void upload(String username, String section, long bytes, long time) throws RemoteException;

    public void download(String username, String section, long bytes, long time) throws RemoteException;

    public void deleteUser(String username) throws RemoteException;

    public TreeMap<Long, StatisticsEntry> get(int statistics, String section) throws RemoteException;

    public TreeMap<Long, StatisticsEntry> get(String groupname, int statistics, String section) throws RemoteException;

    public StatisticsEntry getUserStatistics(String username, String section) throws RemoteException;

    public boolean isUp() throws RemoteException;
}
