package cu.ftpd;

/**
 * @author markus.jevring@petercam.corp
 *         Created: 2011-02-28
 */
public class NoCompatibleNetworkInterfaceFoundException extends Exception {
    public NoCompatibleNetworkInterfaceFoundException() {
        super("No interface of version that found");
    }
}
