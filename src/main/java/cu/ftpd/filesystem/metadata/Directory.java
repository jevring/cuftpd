/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.filesystem.metadata;

import cu.ftpd.logging.Logging;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-12 : 21:40:19
 * @version $Id: Directory.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Directory {
    private final Map<String, Metadata> files = Collections.synchronizedMap(new HashMap<String, Metadata>());
    private File file;
    private long lastUpdate = 0;
    private final Object countLock = new Object();
    private int visitors = 0;
    private long lastStored;

    public Directory(File directory) {
        this.file = directory;
    }

    public Metadata getMetadata(String filename) {
        return files.get(filename);
    }

    public void setOwnership(File file, String username, String groupname) {
        this.lastUpdate = System.currentTimeMillis();
        Metadata metadata = files.get(file.getName());
        if (metadata == null) {
            metadata = new Metadata(file.getName());
            files.put(file.getName(), metadata);
        }
        /*
        System.out.println("we now have the following files in " + this.file.getAbsolutePath());
        for (Map.Entry<String, Metadata> e : files.entrySet()) {
            System.out.println(e.getKey() + "=" + e.getValue().toString());
        }
        */
        // we do this so that we can choose to change only one property and leave the other to what it was originally
        if (username != null) {
            metadata.setUsername(username);
        }
        if (groupname != null) {
            metadata.setGroupname(groupname);
        }

        store(); // it's better to do this after each update, so we don't loose any data in case of a crash. the performance impact isn't that big (a couple of extra ms on my computer)
    }

    public Metadata delete(File file) {
        this.lastUpdate = System.currentTimeMillis();
        Metadata m = files.remove(file.getName());
        store();
        return m;
    }

    public synchronized void load() {
        if (!files.isEmpty()) {
            throw new IllegalStateException("Can only load metadata for a directory once");
        }
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(new File(file, ".metadata"))));
            //System.out.println("loading " + new File(file, ".metadata").getAbsolutePath());
            String line;
            while ((line = in.readLine()) != null) {
                //System.out.println(line);
                String[] parts = line.split(";",3);
                if (parts.length == 3) {
                    Metadata m = new Metadata(parts[0]);
                    m.setUsername(parts[1]);
                    m.setGroupname(parts[2]);
                    files.put(m.getFilename(), m);
                }
            }
        } catch (IOException e) {
            // failed to read the metadata for these files, but that's no biggie.
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Logging.getErrorLog().reportException("Failed to close buffered reader", e);
                    //e.printStackTrace();
                }
            }
        }
    }

    public void store() {
        synchronized(countLock) {
            if (lastUpdate > lastStored) {
                //System.err.println("STORING changed directory: " + file.getAbsolutePath());
                // it's been updated after it's been saved
                // NOTE: always check if the directory that we want to save in exists. if it doesn't, that means that it got moved, we ran the saver (which locks), and THEN the directory move got registered
                PrintWriter out = null;
                try {
                    out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(file, ".metadata")))));
                    for (Metadata m : files.values()) {
                        out.println(m.toString());
                        //System.out.println("wrote " + m.toString() + " to file " + new File(file, ".metadata").getAbsolutePath());
                    }
                    out.flush();
                } catch (IOException e) {
                    Logging.getErrorLog().reportError("Failed to create metadata for directory: " + file.getAbsolutePath() + " because: " + e.getMessage());
                    //System.err.println(e.getMessage());
                } finally {
                    if (out != null) {
                        out.close();
                    }
                }
                lastStored = System.currentTimeMillis();
            } else {
                //System.err.println("NOT storing directory, because it wasn't changed: " + file.getAbsolutePath());
            }

        }
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setFile(File directory) {
        this.file = directory;
        // Note: even though this is a change to the data, we do NOT store it here.
        // when this is invoked, the whole directory has already been moved anyway
    }

    void userEnter() {
        synchronized(countLock) {
            visitors++;
        }
    }

    void userExit() {
        synchronized(countLock) {
            visitors--;
        }
    }
    // yay for package local
    int visitors() {
        synchronized(countLock) {
            return visitors;
        }
    }

    public File getFile() {
        return file;
    }
}
