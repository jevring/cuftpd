/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.filesystem.filters;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashSet;import java.util.Arrays;
import java.util.Set;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-aug-29 : 14:47:37
 * @version $Id: ForbiddenFilesFilter.java 258 2008-10-26 12:47:23Z jevring $
 */
public class ForbiddenFilesFilter implements FilenameFilter {
    private static Set<String> forbiddenFiles = new HashSet<String>();

    /**
     * Tests if a specified file should be included in a file list.
     *
     * @param dir  the directory in which the file was found.
     * @param name the name of the file.
     * @return <code>true</code> if and only if the name should be
     *         included in the file list; <code>false</code> otherwise.
     */
    public boolean accept(File dir, String name) {
        return !forbiddenFiles.contains(name);
    }

    public static Set<String> getForbiddenFiles() {
        return forbiddenFiles;
    }

    private static Set<String> createForbiddenFiles(String forbiddenFilesString) {
        Set<String> files = new HashSet<String>();
        // Note: this can be .contains() for check, since we don't deal with patterns
        files.add(".raceinfo");
        files.add(".metadata");
        // this will return "" if it is empty or doesn't exist, which makes this work anyway
        String[] ff = forbiddenFilesString.split(" ");
        files.addAll(Arrays.asList(ff));
        return files;
    }

    public static void initialize(String forbiddenFilesString) {
        forbiddenFiles = createForbiddenFiles(forbiddenFilesString);
    }
}
