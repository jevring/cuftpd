/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.filesystem.permissions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-aug-01 : 11:59:35
 * @version $Id: ActionPermission.java 258 2008-10-26 12:47:23Z jevring $
 */
public class ActionPermission extends UserAndGroupPermission {

    // permissions
    public static final int DELETE = 10;
    public static final int DELETEOWN = 11;
    public static final int MKDIR = 12;
    public static final int RMDIR = 13;
    public static final int WIPE = 14;
    public static final int CWD = 15;
    public static final int LIST = 16;
    public static final int UPLOAD = 17;
    public static final int DOWNLOAD = 18;
    public static final int RENAME = 19;
    public static final int RENAMEOWN = 20;
    public static final int RESUME = 21;
    public static final int RESUMEOWN = 22;
    public static final int NUKE = 23;
    public static final int DUPECHECK = 25;
    public static final int SETTIME = 26;
    public static final int FXPDOWNLOAD = 27;
    public static final int FXPUPLOAD = 28;
	public static final int OVERWRITE = 29;
	public static final int OVERWRITEOWN = 30;

    // logging permission/requirement
    public static final int DIRLOG = 200;
    public static final int DUPELOG = 201;
    public static final int XFERLOG = 202;

    // special user/group entity identifier
    public static final int ID_ANY = 100;

    private static final HashMap<String, Integer> permissionNameToNumberMapping = new HashMap<String, Integer>();

    static {
        permissionNameToNumberMapping.put("delete", ActionPermission.DELETE);
        permissionNameToNumberMapping.put("deleteown", ActionPermission.DELETEOWN);
        permissionNameToNumberMapping.put("mkdir", ActionPermission.MKDIR);
        permissionNameToNumberMapping.put("rmdir", ActionPermission.RMDIR);
        permissionNameToNumberMapping.put("wipe", ActionPermission.WIPE);
        permissionNameToNumberMapping.put("cwd", ActionPermission.CWD);
        permissionNameToNumberMapping.put("list", ActionPermission.LIST);
        permissionNameToNumberMapping.put("upload", ActionPermission.UPLOAD);
        permissionNameToNumberMapping.put("download", ActionPermission.DOWNLOAD);
        permissionNameToNumberMapping.put("rename", ActionPermission.RENAME);
        permissionNameToNumberMapping.put("renameown", ActionPermission.RENAMEOWN);
        permissionNameToNumberMapping.put("resume", ActionPermission.RESUME);
        permissionNameToNumberMapping.put("resumeown", ActionPermission.RESUMEOWN);
        permissionNameToNumberMapping.put("nuke", ActionPermission.NUKE);
        permissionNameToNumberMapping.put("dupecheck", ActionPermission.DUPECHECK);
        permissionNameToNumberMapping.put("settime", ActionPermission.SETTIME);
        permissionNameToNumberMapping.put("fxpdownload", ActionPermission.FXPDOWNLOAD);
        permissionNameToNumberMapping.put("fxpupload", ActionPermission.FXPUPLOAD);
	    permissionNameToNumberMapping.put("overwrite", ActionPermission.OVERWRITE);
	    permissionNameToNumberMapping.put("overwriteown", ActionPermission.OVERWRITEOWN);
        permissionNameToNumberMapping.put("dirlog", ActionPermission.DIRLOG);
        permissionNameToNumberMapping.put("dupelog", ActionPermission.DUPELOG);
        permissionNameToNumberMapping.put("xferlog", ActionPermission.XFERLOG);
        permissionNameToNumberMapping.put("all", ActionPermission.ID_ANY);
    }
    
    public static Integer resolve(String permissionName) throws UnknownPermissionException {
        Integer i = permissionNameToNumberMapping.get(permissionName);
        if (i == null) {
            throw new UnknownPermissionException(permissionName);
        } else {
            return i;
        }
    }

    public static boolean isReservedWord(String word) {
        return permissionNameToNumberMapping.containsKey(word);
    }


    protected final int type;
    protected final int permission; // delete, upload, download, cwd, etc.
/*
    protected final Set<String> users;
    protected final Set<String> groups;
*/
    public ActionPermission(int type, int permission, String directory, boolean quick) {
        super(quick, directory);
        this.type = type;
        this.permission = permission;
    /*
        users = new HashSet<String>();
        groups = new HashSet<String>();
    */
    }

    public int getType() {
        return type;
    }

    public int getPermission() {
        return permission;
    }
/*
    public boolean appliesToUser(String username) {
        return users.contains(username) || users.contains("*");
    }

    public boolean appliesToGroup(String group) {
        return groups.contains(group) || groups.contains("*");
    }

    public boolean appliesToGroups(Set<String> groups) {
        if (this.groups.contains("*")) {
            return true;
        } else {
            for (String group : groups) {
                if (this.groups.contains(group)){
                    // if the user is in ANY group for which this applies, return true
                    return true;
                }
            }
        }
        return false;
        //return groups.containsAll(groups); // how fitting that we are returning the user's groups in a usable format.
    }

    public void addGroup(String group) {
        groups.add(group);
    }

    public void addUser(String username) {
        users.add(username);
    }
*/

    public String toString() {
        return '[' + (type == 1 ? "allow" : "deny") +
                super.toString() +
                ";p:" + permission +
                ']';
    }
}
