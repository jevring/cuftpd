/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.filesystem.permissions;

import cu.ftpd.logging.Formatter;

import java.util.Set;
import java.util.HashSet;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-29 - 22:04:32
 * @version $Id: SpeedPermission.java 258 2008-10-26 12:47:23Z jevring $
 */
public class SpeedPermission extends UserAndGroupPermission {
    public static final int UPLOAD = 7;
    public static final int DOWNLOAD = 8;
    
    // We have both up and download restrictions in the same rule. (one or both may be 0, as in the ulimit case)
    private long downloadRestriction = 0;
    private long uploadRestriction = 0;

    private final int type; // RESTRICT or UNRESTRICT

    private final TimeOfDay start;
    private final TimeOfDay end;
/*
    private final Set<String> users = new HashSet<String>();
    private final Set<String> groups = new HashSet<String>();
*/

    /**
     * Creates a speed restriction.
     *
     * @param type the type of permission, RESTRICT or UNRESTRICT
     * @param directory the directory in which it applies
     * @param quick if it is 'quick'
     * @param start when the restrictions begin (can be null)
     * @param end when the restrictions begin (can be null)
     */
    public SpeedPermission(int type, String directory, boolean quick, TimeOfDay start, TimeOfDay end) {
        super(quick, directory);
        this.type = type;
        this.start = start;
        this.end = end;
        if ((start == null && end != null) || start != null && end == null) {
            throw new IllegalArgumentException("Both 'start' and 'end' are null, or none of them are!");
        }
    }

    public int getType() {
        return type;
    }
/*
    public boolean appliesToUser(String username) {
        return users.contains(username) || users.contains("*");
    }

    public boolean appliesToGroup(String group) {
        return groups.contains(group) || groups.contains("*");
    }

    public boolean appliesToGroups(Set<String> groups) {
        if (this.groups.contains("*")) {
            return true;
        } else {
            for (String group : groups) {
                if (this.groups.contains(group)){
                    // if the user is in ANY group for which this applies, return true
                    return true;
                }
            }
        }
        return false;
        //return groups.containsAll(groups); // how fitting that we are returning the user's groups in a usable format.
    }

    public void addGroup(String group) {
        groups.add(group);
    }

    public void addUser(String username) {
        users.add(username);
    }
*/
    public boolean appliesToTime(TimeOfDay now) {
        // if we are between the two times :
        // if we are after start and before end :
        // if end is before start, check: if we are after start and before midnight or after midnight and before end
        if (end == null && start == null) {
            // if we don't have a time, it always applies
            return true;
        } else {
            // this won't throw an NPE, since we check that either both or none of start and end are null in the constructor
            if (end.isBefore(start)) {
                // account for midnight
                return ((start.isBefore(now) && now.isBefore(TimeOfDay.MIDNIGHT)) || (TimeOfDay.MIDNIGHT.isBefore(now) && now.isBefore(end)));
            } else {
                return (start.isBefore(now) && now.isBefore(end));
            }
        }
    }

    public long getLimit(int direction) {
        if (direction == UPLOAD) {
            return uploadRestriction;
        } else if (direction == DOWNLOAD) {
            return downloadRestriction;
        } else {
            System.err.println("applied for a speed restriction direction that doesn't exist: " + direction);            
            return 0;
        }
    }

    public void setUploadRestriction(long uploadRestriction) {
        this.uploadRestriction = uploadRestriction;
    }

    public void setDownloadRestriction(long downloadRestriction) {
        this.downloadRestriction = downloadRestriction;
    }

    public String toString() {
        String timeRestriction = "";
        if (start != null && end != null) {
            timeRestriction = start + " -> " + end;
        }
        String speedRestriction = "";
        if (type == RESTRICT) {
            speedRestriction = " to " + Formatter.size(uploadRestriction) + " up and " + Formatter.size(downloadRestriction) + " down";
        }
        return '[' + (type == RESTRICT ? "restrict" : "unrestrict") +
                super.toString() +
                ";time:" + timeRestriction + 
                ";speed:" + speedRestriction + ']';
    }
}
