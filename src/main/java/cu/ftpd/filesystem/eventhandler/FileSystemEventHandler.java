/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.filesystem.eventhandler;

import cu.ftpd.events.AfterEventHandler;
import cu.ftpd.events.Event;
import cu.ftpd.logging.Logging;
import cu.ftpd.user.User;

import java.text.MessageFormat;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-26 - 10:50:29
 * @version $Id: FileSystemEventHandler.java 258 2008-10-26 12:47:23Z jevring $
 */
public class FileSystemEventHandler implements AfterEventHandler {
    // the REAL ABSOLUTE path of the directory removed, i.e. not chroot ftp-root
    // path, username, groupname, tagline
    private final MessageFormat newdir = new MessageFormat("NEWDIR: \"{0}\" \"{1}\" \"{2}\" \"{3}\"");
    // path, username, groupname, tagline
    private final MessageFormat deldir = new MessageFormat("DELDIR: \"{0}\" \"{1}\" \"{2}\" \"{3}\"");

    public void handleEvent(Event event) {
        String path = event.getProperty("file.path.real");
        User user = event.getUser();
        // we always log these events, so we don't need to check for permission
        if (event.getType() == Event.CREATE_DIRECTORY) {
            Logging.getEventLog().log(newdir.format(new Object[] {path, user.getUsername(), user.getPrimaryGroup(), user.getTagline()}));
        } else if (event.getType() == Event.REMOVE_DIRECTORY) {
            Logging.getEventLog().log(deldir.format(new Object[] {path, user.getUsername(), user.getPrimaryGroup(), user.getTagline()}));
        }
    }
}
