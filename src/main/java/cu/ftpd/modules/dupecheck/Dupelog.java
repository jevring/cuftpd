/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.dupecheck;

import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.persistence.Persistent;
import cu.ftpd.logging.Logging;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-aug-24 : 16:14:05
 * @version $Id: Dupelog.java 292 2009-03-04 19:44:36Z jevring $
 */
public class Dupelog implements Persistent {
    private File dupelogfile;
    /**
     * We are using a LinkedHashMap instead of a HashMap here to preserve the ordering of the entities
     * without having to use a TreeMap (which apparently has some overhead).
     * This should help us iterate over it faster when we dump it to disk.
     */
    private final Map<String, Dupe> dupes = Collections.synchronizedMap(new LinkedHashMap<String, Dupe>(4096));
    private int days;
    private boolean caseSensitive;
    private String[] allowedFilePatterns;
    private PrintWriter out;

    public Dupelog(String dupefile, int days, boolean caseSensitive, String filePatternsToInclude) throws FileNotFoundException {
        this.days = days;
        this.caseSensitive = caseSensitive;
        dupelogfile = new File(dupefile);
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dupelogfile, true))));
        if (filePatternsToInclude != null) {
            allowedFilePatterns = filePatternsToInclude.split("\\s+|,");
            for (int i = 0; i < allowedFilePatterns.length; i++) {
                allowedFilePatterns[i] = allowedFilePatterns[i].replace(".","\\.").replace("*",".*").replace("?",".");
            }
        } else {
            allowedFilePatterns = new String[0];
        }
    }

    protected Dupelog() {
    }

    /**
     * Checks if the supplied filename is allowed in the dupelog. If not, it will not be added to the dupelog.
     * @param filename the filename to check.
     * @return true if the filename is allowed, false otherwise.
     */
    private boolean isAllowed(String filename) {
        for (String allowedFilePattern : allowedFilePatterns) {
            if (filename.matches(allowedFilePattern)) {
                //System.out.println(filename + " matches a pattern that's allowed in the dupelog: " + allowedFilePattern);
                return true;
            }
        }
        return false;
    }

    public void removeDupe(String filename) {
        dupes.remove(filename);
    }

    /**
     * Removes all files matching the pattern from the dupelog.
     *
     * @param pattern the regular expression that the files to be removed has to match.
     * @return a list of the removed dupes that matched the supplied pattern.
     */
    public List<Dupe> undupe(String pattern) {
        LinkedList<Dupe> undupes = new LinkedList<Dupe>();
        synchronized(dupes) {
            final Pattern p = Pattern.compile(pattern.toLowerCase());
            Iterator<Dupe> i = dupes.values().iterator();
            while (i.hasNext()) {
                Dupe dupe = i.next();
                if (p.matcher(dupe.getFilename()).matches()) {
                    i.remove();
                    undupes.add(dupe);
                }
            }
            if (undupes.size() > 0) {
                save();
            }
        }
        return undupes;
    }

    public void addDupe(String username, String filename) {
        // this must be fast, because it is done after each transfer, and we need to keep the inter-transfer time low.
        if (isAllowed(filename)) {
            Dupe d = new Dupe(System.currentTimeMillis(), username, filename);
            if (caseSensitive) {
                dupes.put(d.getFilename(), d);
            } else {
                dupes.put(d.getFilename().toLowerCase(), d);
            }
            append(d);
        }
    }

    private void append(Dupe dupe) {
        synchronized(dupes) {
            out.println(dupe);
            out.flush();
        }
    }

    /**
     * Checks the dupe database to see if this file is a dupe or not.
     * If it is a dupe, it returns the dupe information, if it isn't, returns null;
     * @param filename the filename to check.
     * @return a <code>Dupe</code> object with information if it is a dupe, null otherwise.
     */
    public Dupe getDupe(String filename) {
        // since we check when we add files if they are allowed or not, we don't have to check here (since they won't be added if they are not allowed anyway)
        if (caseSensitive) {
            return dupes.get(filename);
        } else {
            return dupes.get(filename.toLowerCase());
        }
    }

    public void load() throws IOException {
        // see what is faster, this or normal serialization (granted, normal serialization doesn't exactly make the data in the log readable)
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(dupelogfile)));
            String line;
            Dupe d;
            while ((line = in.readLine()) != null) {
                String[] dupe = line.split(";", 3);
                d = new Dupe(Long.parseLong(dupe[0]), dupe[1], dupe[2]);
                long age = (System.currentTimeMillis() - d.getTime()) / 86400000;
                if (age <= days) { // only add if it is within the range of age set in the config
                    // Note: only be case sensitive or not for the keys. let the filenames themselves still be cased in the dupe
                    if (caseSensitive) {
                        dupes.put(d.getFilename(), d);
                    } else {
                        dupes.put(d.getFilename().toLowerCase(), d);
                    }
                }
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    /**
     * Saves the dupelog to disk completely.
     * This is invoked when something is unduped as well as every hour to maintain the length of the dupelog
     * (according to the number of days supported in the configuration)
     */
    public synchronized void save() {
        /*
        get a lock on the whole dupelog
        close the appender
        open for full writing
        clean out the cruft in the map
        write the map to disk
        close for full writing
        re-open the appender
        call save regularly, do the clean on each startup, as we are loading, and call it every hour or so from the timer
         */

        FileSystem.fastBackup(dupelogfile);
        final long now = System.currentTimeMillis();
        synchronized(dupes) {
            PrintWriter fullOut = null;
            try {
                fullOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dupelogfile)))); // do not append
                // close the appender
                //out.close();
                // open a new full one for us that doesn't append
                //fullOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dupelogfile)))); // do not append
                // clean out the cruft and write it to disk
                Iterator<Dupe> i = dupes.values().iterator();
                while (i.hasNext()) {
                    Dupe dupe = i.next();
                    long age = (now - dupe.getTime()) / 86400000;
                    if (age > days) {
                        // don't save old dupes, just discard them.
                        i.remove();
                    } else {
                        // write it to disk only if it hasn't expired.
                        fullOut.println(dupe);
                    }
                }
                // flush and close the full writer
                fullOut.flush();
                // re-open for append
                //out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dupelogfile, true))));
            } catch (FileNotFoundException e) {
                Logging.getErrorLog().reportCritical("Could not write dupelog to disk: " + e.getMessage());
            } finally {
                if (fullOut != null) {
                    fullOut.close();
                }
            }
        }
    }

    public void shutdown() {
        save();
        out.close();
    }
}

