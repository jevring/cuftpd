/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.xferlog;

import cu.ftpd.modules.Module;
import cu.ftpd.modules.xferlog.eventhandler.XferlogEventHandler;
import cu.ftpd.events.EventHandler;
import cu.ftpd.events.Event;
import cu.ftpd.commands.site.SiteCommandHandler;
import cu.settings.ConfigurationException;
import cu.settings.XMLSettings;

import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-okt-02 - 12:24:43
 * @version $Id: XferlogModule.java 258 2008-10-26 12:47:23Z jevring $
 */
public class XferlogModule implements Module {
    private Xferlog xferlog = null;

    public void initialize(XMLSettings settings) throws ConfigurationException {
        String log = settings.get("/xferlog/log");
        if (log == null || "".equals(log) || "none".equals(log)) {
            xferlog = new DummyXferlog();
        } else {
            try {
                xferlog = new Xferlog(log);
            } catch (IOException e) {
                throw new ConfigurationException("Could not load xferlog: " + e.getMessage(), e);
            }
        }
    }

    public void registerActions(SiteCommandHandler siteCommandHandler) {
    }

    public void registerEventHandlers(EventHandler eventHandler) {
        XferlogEventHandler xeh = new XferlogEventHandler(xferlog);
        eventHandler.addAfterEventHandler(Event.DOWNLOAD, xeh);
        eventHandler.addAfterEventHandler(Event.UPLOAD, xeh);
    }

    public void stop() {
        if (xferlog != null) {
            xferlog.shutdown();
        }
    }
}
