/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.dirlog;

import cu.ftpd.modules.Module;
import cu.ftpd.modules.dirlog.actions.Search;
import cu.ftpd.modules.dirlog.actions.SiteNew;
import cu.ftpd.modules.dirlog.eventhandler.DirlogHandler;
import cu.ftpd.modules.dirlog.eventhandler.Dirscript;
import cu.ftpd.Server;
import cu.ftpd.events.EventHandler;
import cu.ftpd.events.Event;
import cu.ftpd.commands.site.SiteCommandHandler;
import cu.ftpd.commands.site.actions.Action;
import cu.ftpd.persistence.Saver;
import cu.settings.ConfigurationException;
import cu.settings.XMLSettings;

import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-okt-01 - 17:59:09
 * @version $Id: DirlogModule.java 258 2008-10-26 12:47:23Z jevring $
 */
public class DirlogModule implements Module {
    private Dirlog dirlog = null;
    private Dirscript dirscript = null;
    private boolean enableDirscript = false;

    public void initialize(XMLSettings settings) throws ConfigurationException {
        String dirlogPath = settings.get("/dirlog/log");
        if (dirlogPath == null || "".equals(dirlogPath) || "none".equals(dirlogPath)) {
            dirlog = new DummyDirlog();
        } else {
            try {
                dirlog = new Dirlog(dirlogPath);
                dirlog.load();
                Server.getInstance().getTimer().schedule(new Saver(dirlog), 30*1000, 30*1000); // since this is a fairly large chunk of data, we stick to the saving scheme we have here

                enableDirscript = settings.getBoolean("/dirlog/enable_dirscript");
                if (enableDirscript) {
                    String dirscriptExclusions = settings.get("/dirlog/dirscript_exclusions");
                    dirscript = new Dirscript(dirlog, dirscriptExclusions);
                }

            } catch (IOException e) {
                throw new ConfigurationException("Failed to load dirlog: " + e.getMessage(), e);
            }
        }
    }

    public void registerActions(SiteCommandHandler siteCommandHandler) {
        Action search = new Search(dirlog);
        siteCommandHandler.registerAction("dupe", search);
        siteCommandHandler.registerAction("find", search);
        siteCommandHandler.registerAction("search", search);

        Action siteNew = new SiteNew(dirlog);
        siteCommandHandler.registerAction("new", siteNew);
    }

    public void registerEventHandlers(EventHandler eventHandler) {
        DirlogHandler dh = new DirlogHandler(dirlog);

        eventHandler.addAfterEventHandler(Event.CREATE_DIRECTORY, dh);  // add new (empty) dir to dirlog
        eventHandler.addAfterEventHandler(Event.REMOVE_DIRECTORY, dh);  // remove directory from dirlog (non-recursive)
        eventHandler.addAfterEventHandler(Event.DELETE_FILE, dh);       // remove n bytes and 1 file from dir in dirlog
        eventHandler.addAfterEventHandler(Event.RENAME_TO, dh);         // move file/dir in dirlog
        eventHandler.addAfterEventHandler(Event.UPLOAD, dh);            // add to dirlog

        if (enableDirscript) {
            eventHandler.addBeforeEventHandler(Event.CREATE_DIRECTORY, dirscript);
        }
    }

    public void stop() {
        if (dirlog != null) {
            dirlog.shutdown();
        }
        // no need to de-register the site commands, since they do not need to be stopped individually anyway
    }
}
