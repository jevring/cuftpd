/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.dirlog.eventhandler;

import cu.ftpd.events.BeforeEventHandler;
import cu.ftpd.events.Event;
import cu.ftpd.Connection;
import cu.ftpd.modules.dirlog.DirlogEntry;
import cu.ftpd.modules.dirlog.Dirlog;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Logging;

import java.util.Iterator;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.io.File;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-26 - 00:44:03
 * @version $Id: Dirscript.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Dirscript implements BeforeEventHandler {
    private final Dirlog dirlog;
    private final Pattern noCheck;
    private static final HashMap<String, DirlogEntry> entries = new HashMap<String, DirlogEntry>();

    public Dirscript(Dirlog dirlog, String noCheckPattern) {
        this.dirlog = dirlog;
        //noCheck = Pattern.compile("cd\\d|disc\\d");
        noCheck = Pattern.compile(noCheckPattern);
        Iterator<DirlogEntry> it = dirlog.getLatestDirs(Long.MAX_VALUE);
        while (it.hasNext()) {
            DirlogEntry dirlogEntry = it.next();
            String name = new File(dirlogEntry.getPath()).getName();
            entries.put(name, dirlogEntry);
        }
    }

    public boolean handleEvent(Event event, Connection connection) {
        String name = FileSystem.getNameFromPath(event.getProperty("file.path.ftp"));
        // NOTE: we don't need to check for CD1 etc, because before creating "cd1", the user has to create the
        // containing directory (the release), which would get caught by the dirscript
        if (noCheck.matcher(name).matches()) {
            // only check if it is not called "cd1" etc
            return true;
        } else {
            //DirlogEntry de = Logging.getDirlog().quickContains(name);
            DirlogEntry de = entries.get(name);
            if (de != null) {
                connection.respond("500 A directory by that name already exists: " + de.getPath());
                return false;
            }
            return true;
        }
    }
}
