/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.dirlog.eventhandler;

import cu.ftpd.events.*;
import cu.ftpd.filesystem.permissions.ActionPermission;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.modules.dirlog.Dirlog;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-26 - 10:54:04
 * @version $Id: DirlogHandler.java 258 2008-10-26 12:47:23Z jevring $
 */
public class DirlogHandler implements AfterEventHandler {
    private final Dirlog dirlog;

    public DirlogHandler(Dirlog dirlog) {
        this.dirlog = dirlog;
    }

    public void handleEvent(Event event) {
        if (event.getType() == Event.RENAME_TO) {
            String sourceParentDirPath = FileSystem.getParentDirectoryFromPath(event.getProperty("rename.source.path.ftp"));
            String targetParentDirPath = FileSystem.getParentDirectoryFromPath(event.getProperty("rename.target.path.ftp"));

            // this must also obey the logging check, for both source AND destination
            // it only gets logged if logging occurs in both (otherwise were create black hole moves)
            // always check the parent directory
            if (ServiceManager.getServices().getPermissions().shouldLog(ActionPermission.DIRLOG, sourceParentDirPath, event.getUser()) &&
                ServiceManager.getServices().getPermissions().shouldLog(ActionPermission.DIRLOG, targetParentDirPath, event.getUser())) {
                boolean directory = Boolean.parseBoolean(event.getProperty("file.isDirectory"));
                if (directory) {
                    dirlog.moveDirectory(event.getProperty("rename.source.path.ftp"), event.getProperty("rename.target.path.ftp"));
                } else {
                    long size = Long.parseLong(event.getProperty("file.size"));
                    dirlog.moveFile(event.getProperty("rename.source.path.ftp"), event.getProperty("rename.target.path.ftp"), size);
                }
            }
        } else {
            String path = event.getProperty("file.path.ftp");
            // check the permission in the parent dir.
            if (ServiceManager.getServices().getPermissions().shouldLog(ActionPermission.DIRLOG, FileSystem.getParentDirectoryFromPath(path), event.getUser())) {
                if (event.getType() == Event.UPLOAD) {
                    long bytes = Long.parseLong(event.getProperty("transfer.bytesTransferred"));
                    dirlog.update(FileSystem.getParentDirectoryFromPath(path), bytes, event.getUser().getUsername());
                } else if (event.getType() == Event.CREATE_DIRECTORY) {
                    dirlog.create(path, event.getUser().getUsername());
                } else if (event.getType() == Event.REMOVE_DIRECTORY) {
                    // while this indeed is recursive, RMD will not complete if there are files in the directory, thus we only need to look for the leaf here
                    boolean deleteRecursively = Boolean.parseBoolean(event.getProperty("dirlog.deleteRecursively")); // this is backwards compatible, because 'null' is interpreted as 'false'
                    dirlog.deleteDir(path, deleteRecursively);
                } else if (event.getType() == Event.DELETE_FILE) {
                    // since this happens after the fact, we need to have stored this data in the event somehow
                    long bytes = Long.parseLong(event.getProperty("file.size"));
                    dirlog.deleteFileInDir(path, bytes);
                }
            }
        }
    }
}
