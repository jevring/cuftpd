/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.dirlog;

import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.persistence.Persistent;
import cu.ftpd.logging.Logging;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-22 : 18:38:40
 * @version $Id: Dirlog.java 280 2008-11-24 18:52:18Z jevring $
 */
public class Dirlog implements Persistent {
    private Map<String, DirlogEntry> entries;
    private LinkedList<DirlogEntry> orderedEntries;
    private PrintWriter log;
    private File dirlogfile;
    public static final Object mutex = new Object();
    private long lastUpdate = 0;
    private long lastSave = 0;

    public Dirlog(String dirlogfile) throws IOException {
        this.dirlogfile = new File(dirlogfile);
        //System.out.println("Dirlog file size: " + dirlogfile.length());
        //System.out.println("Number of estimated entries: " + ((dirlogfile.length() / 85)));
        entries = new HashMap<String, DirlogEntry>((dirlogfile.length() / 85)); // estimate 75 bytes per entry was about right with an 8000 line test set
        // NOTE: we were GOING to use a LinkedHashMap here, but they can only be traversed in forward order, and we need to traverse them in the reverse direction
        orderedEntries = new LinkedList<DirlogEntry>();
        try {
            log =  new PrintWriter(new FileOutputStream(new File(dirlogfile), true));
        } catch (IOException e) {
            throw new IOException("Could not open dirlog for writing: " + e.getMessage(), e);
        }
    }

    protected Dirlog() {
        // only for extending classes, such as the DummyDirlog
    }
    /*
    NOTE: doing a 'contains' or 'exists' is useless, since we want to check for dirnames, not paths,
    and since the dirlog is keyed to paths, we might as well use findDupe() instead
     */
    /**
     * Searches for an explicit directory name. It searches through the entries backwards, in the hope that there
     * is a higher probability that the directory we are searching for was added later rather than sooner.
     * @param directory the name of the directory we want to find.
     * @return the dirlog entry if a path that ends with this diretory exists in the dupelog, null otherwise.
     */
    public DirlogEntry quickContains(String directory) {
        Iterator<DirlogEntry> it = orderedEntries.descendingIterator();
        while (it.hasNext()) {
            DirlogEntry dirlogEntry = it.next();
            if (dirlogEntry.getPath().endsWith(directory)) {
                return dirlogEntry;
            }
        }
        return null;
    }

    /**
     * Creates an empty post in the dirlog for the specified directory.
     * @param directoryPath the path to the directory that was created, chroot ftproot.
     * @param username the username of the user who created the directory.
     */
    public void create(String directoryPath, String username) {
        //System.out.println("dirlog create: " + directoryPath);
        synchronized(mutex) {
            if (directoryPath == null || "".equals(directoryPath)) {
                directoryPath = "/";
            }
            DirlogEntry de = entries.get(directoryPath);
            if (de == null) {
                de = new DirlogEntry(directoryPath, username);
                entries.put(de.getPath(), de);
                orderedEntries.addLast(de);
            }
        }
        lastUpdate = System.currentTimeMillis();
    }

    /**
     * Updates the specified dir with one more file of the specified length.
     * NOTE: This is the path of the DIRECTORY in which the file resides, not the file itself.
     *
     * @param directoryPath the dir to update (chroot path). IMPORTANT: just the directory, not the full path name.
     * @param bytes how many bytes to add to it.
     * @param username the name of the uploader. This is only needed when the dir is first created.
     */
    public void update(String directoryPath, long bytes, String username) {
        //System.out.println("dirlog update: " + directoryPath + ", bytes=" + bytes);
        synchronized(mutex) {
            if (directoryPath == null || "".equals(directoryPath)) {
                directoryPath = "/";
            }
            DirlogEntry de = entries.get(directoryPath);
            if (de == null) {
                de = new DirlogEntry(directoryPath, username);
                entries.put(de.getPath(), de);
                orderedEntries.addLast(de);
            }
            de.addFile(bytes);
        }
        lastUpdate = System.currentTimeMillis();
    }


    /**
     * Called when a file is deleted in a directory.
     * @param directoryPath the path of the directory in which the deleted file used to exist. (chroot ftp)
     * @param bytesToRemove how many bytes the deleted file contained.
     */
    public void deleteFileInDir(String directoryPath, long bytesToRemove) {
        synchronized(mutex) {
            DirlogEntry de = entries.get(directoryPath);
            if (de != null) {
                de.delFile(bytesToRemove);
                lastUpdate = System.currentTimeMillis();
                // only change the dirlog if we actually have this dir in the dirlog
                // we could be missing it if it was not created by the site, such as a section dir, or the root.
            }
        }
    }

    /**
     * Deletes a directory from the dirlog. This is called after a directory is deleted from the disk.
     *
     * @param directoryPath the path of the directory that was deleted. (chroot ftp)
     * @param recursive true if we should remove all dirs that start with <code>directoryPath</code>, false if we want do remove THE dir that equals <code>directoryPath</code>
     */
    public void deleteDir(String directoryPath, boolean recursive) {
        // _todo: we could have a "recursive" flag here that would delete all dirs that start with this path
        // in fact, don't we always want it to be recursive? our deletes are recursive, so we know that all dirs under it must disappear
        // the thing is, of course, and 'recursive' is only good when wiping, as RMD will not delete child directories
        // this, a wipe (recursive) would go over the whole list, whereas an RMD would only go over the one matching hit
        synchronized(mutex) {
            entries.remove(directoryPath);
            Iterator<DirlogEntry> it = orderedEntries.descendingIterator(); // it is more probable that the directory is at the back of the list.
            if (recursive) {
                while (it.hasNext()) {
                    DirlogEntry dirlogEntry = it.next();
                    if (dirlogEntry.getPath().startsWith(directoryPath)) {
                        // remove all directories that start with this path, i.e. all under it as well
                        it.remove();
                    }
                }
            } else {
                while (it.hasNext()) {
                    DirlogEntry dirlogEntry = it.next();
                    if (dirlogEntry.getPath().equals(directoryPath)) {
                        it.remove();
                        break;
                    }
                }
            }
            lastUpdate = System.currentTimeMillis();
        }
    }

    /**
     * Moves a file from one dir to another in the dirlog.
     *
     * @param sourceDir the chroot path of the source dir.
     * @param destinationDir the chroot path of the destination dir.
     * @param bytesToMove how many bytes to remove from the source dir and put in the destination dir.
     */
    public void moveFile(String sourceDir, String destinationDir, long bytesToMove) {
        synchronized(mutex) {
            DirlogEntry de = entries.get(sourceDir);
            if (de != null) {
                de.delFile(bytesToMove);
                de = entries.get(destinationDir);
                if (de != null) {
                    de.addFile(bytesToMove);
                    lastUpdate = System.currentTimeMillis();
                }
            }
        }
    }

    /**
     * Moves a directory (and all it's children) named in <code>sourceDir</code> to the directory named in <code>destinationDir</code>.
     *
     * @param sourceDir the full chrooted path to the source directory.
     * @param destinationDir the full chrooted path to the destination directory.
     */
    public void moveDirectory(String sourceDir, String destinationDir) {
        /*
        all entries that start with sourceDir shall end up in destinationDir

        since we remove things, we need to use an iterator
        once we have extraced the ones we want to remove and put them in a list of their own,
        we alter the paths in the DirlogEntry entries, and then we add them to the set again with the new path
         */
        List<DirlogEntry> entriesToMove = new LinkedList<DirlogEntry>();
        synchronized (mutex) {
            Iterator<DirlogEntry> i = orderedEntries.descendingIterator();
            while (i.hasNext()) {
                DirlogEntry entry = i.next();
                if (entry.getPath().startsWith(sourceDir)) {
                    // we've found a directory that matches, make with the moving
                    // remove it from the list
                    // remove it from the map
                    // add it to the list of moved entries
                    i.remove();
                    entries.remove(entry.getPath());
                    entriesToMove.add(entry);
                }
            }
            for (DirlogEntry entry : entriesToMove) {
                // this is the list of entries that need to be moved
                // alter their path
                // insert them into the map
                // insert them into the list
                /*
                -remove the while source path from the entries
                -add the destination path before the source path
                 */
                // Since we *know* that all paths begin with 'sourceDir', we can just remove sourceDir.length() characters
                String path = entry.getPath();
                path = path.substring(sourceDir.length());
                path = destinationDir + path;
                entry.setPath(path);
                entries.put(entry.getPath(), entry);
                orderedEntries.add(entry);
            }
        }
    }

    public LinkedList<DirlogEntry> search(String pattern) {
        final Pattern p = Pattern.compile(pattern.toLowerCase());
        LinkedList<DirlogEntry> matchingDirlogEntries = new LinkedList<DirlogEntry>();

        synchronized (mutex) {
            for (DirlogEntry dirlogEntry : orderedEntries) {
                if (p.matcher(dirlogEntry.getPath().toLowerCase()).matches()) {
                    matchingDirlogEntries.add(dirlogEntry);
                }
            }
        }
        return matchingDirlogEntries;
    }

    public Iterator<DirlogEntry> getLatestDirs(long howMany) {
        // bounded shallow copy. This is better than cloning the whole thing.
        LinkedList<DirlogEntry> ll = new LinkedList<DirlogEntry>();
        synchronized (mutex) {
            Iterator<DirlogEntry> it = orderedEntries.descendingIterator();
            long i = 0;
            while (it.hasNext()) {
                DirlogEntry dirlogEntry = it.next();
                i++;
                ll.add(dirlogEntry);
                if (i == howMany) {
                    break;
                }
            }
            return ll.iterator();
        }
    }

    public void shutdown() {
        save();
        log.close();
    }

    public void load() throws IOException {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(dirlogfile)));
            String line;
            //long start = System.currentTimeMillis();
            while ((line = in.readLine()) != null) {
                String[] data = line.split(";", 5);
                // path, time, size, files
                try {
                    DirlogEntry de = new DirlogEntry(Long.parseLong(data[0]), data[1], Integer.parseInt(data[2]), Long.parseLong(data[3]), data[4]);
                    entries.put(de.getPath(), de);
                    // this is ok, because altering objects in the HashMap means we're altering objects in the LinkedList as well, since the actualy object they reference is the same
                    orderedEntries.addLast(de);
                } catch (NumberFormatException e) {
                    Logging.getErrorLog().reportCritical("Parsing of dirlog line failed because something that was supposed to be a number wasn't: " + e.getMessage() + '(' + line + ')');
                }
            }
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }

    public void save() {
        // NOTE: the dirlog is saved by a saver every 30 seconds
        if (lastUpdate > lastSave) {
            synchronized (mutex) {
                FileSystem.fastBackup(dirlogfile);

                //System.out.println("STORING dirlog");
                PrintWriter out = null;
                try {
                    out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dirlogfile))));
                    for (DirlogEntry de : orderedEntries) {
                        out.println(de);
                    }
                    out.flush();
                } catch (IOException e) {
                    Logging.getErrorLog().reportError("Failed to write dirlog to file: " + e.getMessage());
                } finally {
                    if (out != null) {
                        out.close();
                    }
                }
                lastSave = System.currentTimeMillis();
            }
        }
    }
}
