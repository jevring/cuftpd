/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.requests.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.commands.site.actions.Action;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Formatter;
import cu.ftpd.modules.requests.Request;
import cu.ftpd.modules.requests.RequestLog;
import cu.ftpd.user.User;
import cu.ftpd.user.userbases.NoSuchUserException;

import java.util.List;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-27 : 16:44:28
 * @version $Id: Requests.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Requests extends Action {
    private final RequestLog requestLog;
    private static final String lineFormat = "200- %-1s %-6s %-16s %-9s %-37s %-1s";
    public Requests(RequestLog requestLog) {
        super("requests");
        this.requestLog = requestLog;
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        List<cu.ftpd.modules.requests.Request> requests = requestLog.getRequests();
        int i = 0;
        connection.respond(createHeader());
        connection.respond(String.format(lineFormat, Formatter.getBar(), "Number", "User", "Approved", "Request", Formatter.getBar()));
        connection.respond(Formatter.createLine(200));
        for (Request r : requests) {
            connection.respond(String.format(lineFormat, Formatter.getBar(), i, r.getRequester(), (r.isApproved() ? "OK" : "NOT OK"), r.getDescription(), Formatter.getBar()));
            i++;
        }
        connection.respond(Formatter.createFooter(200));
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
