package cu.ftpd.modules;

import cu.ftpd.events.EventHandler;
import cu.ftpd.commands.site.SiteCommandHandler;
import cu.settings.ConfigurationException;
import cu.settings.XMLSettings;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-okt-01 - 17:50:12
 * @version $Id: Module.java 292 2009-03-04 19:44:36Z jevring $
 */
public interface Module {
    // Constructor should generally contain an FtpSettings object and a reference to the SiteCommandHandler object in which the modules can register their actions

    /**
     * Initializes the component.
     *
     * @param settings the settings-object for the server.
     * @throws ConfigurationException if an error in configuration occurs.
     */
    public void initialize(XMLSettings settings) throws ConfigurationException;


    /**
     * Allows the module to register any actions (site commands) that it may have.
     *
     * @param siteCommandHandler the site command handler to register the actions with.
     */
    public void registerActions(SiteCommandHandler siteCommandHandler);

    /**
     * Allows the module to register any event handlers that it may have.
     *
     * @param eventHandler the central event handler to register enw handlers with.
     */
    public void registerEventHandlers(EventHandler eventHandler);

    /**
     * Allows the module to shut down cleanly. Called before the module is removed, for example just before a system shutdown.
     */
    public void stop();
}
