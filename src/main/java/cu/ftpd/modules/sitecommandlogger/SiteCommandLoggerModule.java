/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.modules.sitecommandlogger;

import cu.ftpd.modules.Module;
import cu.ftpd.modules.sitecommandlogger.eventhandler.SiteCommandLoggerEventHandler;
import cu.ftpd.events.EventHandler;
import cu.ftpd.events.Event;
import cu.ftpd.commands.site.SiteCommandHandler;
import cu.settings.ConfigurationException;
import cu.settings.XMLSettings;

import java.io.*;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version $Id: SiteCommandLoggerModule.java 280 2008-11-24 18:52:18Z jevring $
 * @since 2008-okt-03 - 17:39:52
 */
public class SiteCommandLoggerModule implements Module {
    private PrintWriter log = null;
    private SiteCommandLoggerEventHandler scleh = null;

    public void initialize(XMLSettings settings) throws ConfigurationException {
        try {
            log = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(settings.get("/site_command_logger/log")), true)), true);
            scleh = new SiteCommandLoggerEventHandler(log, settings.get("/site_command_logger/commands"));
        } catch (FileNotFoundException e) {
            throw new ConfigurationException("Could not open log for writing", e);
        }
    }

    public void registerActions(SiteCommandHandler siteCommandHandler) {
    }

    public void registerEventHandlers(EventHandler eventHandler) {
        eventHandler.addBeforeEventHandler(Event.SITE_COMMAND, scleh);
    }

    public void stop() {
        if(log != null) {
            log.close();
        }
    }
}
