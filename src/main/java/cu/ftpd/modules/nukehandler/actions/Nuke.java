/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.nukehandler.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.commands.site.actions.Action;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.filesystem.permissions.ActionPermission;
import cu.ftpd.logging.Formatter;
import cu.ftpd.modules.nukehandler.NukeException;
import cu.ftpd.modules.nukehandler.NukeHandler;
import cu.ftpd.user.User;
import cu.ftpd.user.userbases.NoSuchUserException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.AccessControlException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:28:31
 * @version $Id: Nuke.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Nuke extends Action {
    private final NukeHandler nukeHandler;
    protected final boolean nuke;

    public Nuke(NukeHandler nukeHandler, boolean nuke) {
        super((nuke ? "nuke" : "unnuke"));
        this.nukeHandler = nukeHandler;
        this.nuke = nuke;
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        String ftpPathToTarget = fs.resolveFtpPath(parameterList[1]);
        if (!ServiceManager.getServices().getPermissions().hasPermission(ActionPermission.NUKE, ftpPathToTarget, user)) {
            connection.respond("531 Permission denied");
        } else {
            // Note: both this and the multiplier for just nuke is very ugly. maybe look over it? (same as with the search and other string-based stuff.)
            try {
                boolean ok;
                if (nuke) {
                    ok = nuke(parameterList, connection, fs, user);
                } else {
                    ok = unnuke(parameterList, fs, user);
                }
                if (ok) {
                    connection.respond("200 Command successful.");
                } else {
                    connection.respond("500 Command failed.");
                }
            } catch (AccessControlException e) {
                connection.respond("531 Permission denied");
            } catch (FileNotFoundException e) {
                connection.respond("500 " + e.getMessage());
            } catch (IOException e) {
                connection.respond("500 " + e.getMessage());
            } catch (NukeException e) {
                connection.respond("500 " + e.getMessage());
            }
        }
    }

    private boolean nuke(String[] parameterList, Connection connection, FileSystem fs, User user) throws FileNotFoundException, NukeException {
        int multiplier = -1;
        try {
            multiplier = Integer.parseInt(parameterList[2]);
        } catch (NumberFormatException e) {}
        if (multiplier < 0) {
            connection.respond("500- " + parameterList[2] + " is not a valid multiplier");
            return false;
        } else {
            return nukeHandler.nuke(fs.getRealParentWorkingDirectoryPath(), fs.getCurrentSection().getName(), parameterList[1], multiplier, user.getUsername(), Formatter.join(parameterList, 3, parameterList.length, " "));
        }
    }

    private boolean unnuke(String[] parameterList, FileSystem fs, User user) throws IOException, NukeException {
        return nukeHandler.unnuke(fs.getRealParentWorkingDirectoryPath(), fs.getCurrentSection().getName(), parameterList[1], user.getUsername(), Formatter.join(parameterList, 2, parameterList.length, " "));
    }

    protected boolean currentUserIsGadminForUser(String username, User user) {
        try {
            User u = ServiceManager.getServices().getUserbase().getUser(username);
            //current user is gadmin for any of the groups the specified user is a member of
            for (String gadminGroup : user.getGadminGroups()) { // this is generally a smaller set, so it makes sense to go over that first, since we abort early
                if (u.isMemberOfGroup(gadminGroup)) {
                    return true;
                }
            }
        } catch (NoSuchUserException e) {
            // you can't be the gadmin of a user that doesn't exist
        }
        return false;
    }
}
