/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.nukehandler;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-10 : 14:25:04
 * @version $Id: Nuke.java 258 2008-10-26 12:47:23Z jevring $
 */
public class Nuke {
    // releasename, section, nuker, size, multiplier, reason, victims(formatted string, user1:group/bytesMB user1:group/bytesMB)
    private final String releaseName;
    private final String section;
    private final String nuker;
    private final long size;
    private final int multiplier;
    private final String reason;
    private final String victims;

    public Nuke(String releaseName, String section, String nuker, long size, int multiplier, String reason, String victims) {
        this.releaseName = releaseName;
        this.section = section;
        this.nuker = nuker;
        this.size = size;
        this.multiplier = multiplier;
        this.reason = reason;
        this.victims = victims;
    }


    public String getReleaseName() {
        return releaseName;
    }

    public String getSection() {
        return section;
    }

    public String getNuker() {
        return nuker;
    }

    public long getSize() {
        return size;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public String getReason() {
        return reason;
    }

    public String getVictims() {
        return victims;
    }
}
