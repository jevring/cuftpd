/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.zipscript;

import cu.ftpd.Connection;
import cu.ftpd.commands.transfer.TransferPostProcessor;
import cu.ftpd.user.User;
import cu.shell.ProcessResult;

import java.io.File;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-12 : 13:41:35
 * @version $Id: Zipscript.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface Zipscript extends TransferPostProcessor {

    public ProcessResult process(File file, long checksum, long bytesTransferred, long transferTime, User user, String sectionName, long speed);

    /**
     * For external zipscripts, this should probably trigger the postdel (/bin/postdel) script
     * @param file the file that was deleted.
     * @param user the user who deleted the file.
     * @param sectionName the section in which the event took place
     */
    public void delete(File file, User user, String sectionName);

    /**
     * Rescan re-checks the integrity of the files according to the sfv-file in the directory.
     * Maybe throw an IOException if the sfv-file wasn't found, or just a FileNotFoundException.
     * Currently no idea how the rescan would feed back from an external zipscript to the client, but our internal zipscript can just use a connection.
     * @param dir the directory to be rescanned.
     * @param sectionName the section in which the event took place
     * @param user the user who initiated the scan
     * @param connection the connection of the scanning user
     * @param rescanParameters the parameters sent to "site rescan"
     */
    public void rescan(String dir, String sectionName, User user, Connection connection, String rescanParameters);


    // should we make this an abstract superclass instead, that handles the metadata stuff?
    // (defaults to something weird, unless we're using our internal zipscript, in which case it checks the race dir for the info)
    // or maybe our zipscript should just register metadata with some other object that keeps track until the race is completed when the information is expunged
    // (or maybe we should bite the bullet and track user/group for all files)
    // (file is read from disk on list(disk access anyway), and kept in memory for lookups, and written to disk when a new file upload starts (and should be updated on delete/wipe, too)))
}
