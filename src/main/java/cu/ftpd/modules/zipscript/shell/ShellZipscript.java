/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.zipscript.shell;

import cu.ftpd.Connection;
import cu.ftpd.logging.Logging;
import cu.shell.ProcessResult;
import cu.shell.Shell;
import cu.ftpd.modules.zipscript.Zipscript;
import cu.ftpd.user.User;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-12 - 14:51:39
 * @version $Id: ShellZipscript.java 263 2008-10-30 21:30:44Z jevring $
 */
public class ShellZipscript implements Zipscript {
    protected String rescanPath;
    protected String uploadPath;
    protected String deletePath;

    protected ShellZipscript(){}

    public ShellZipscript(String rescanPath, String uploadPath, String deletePath) {
        this.rescanPath = rescanPath;
        this.uploadPath = uploadPath;
        this.deletePath = deletePath;
    }

    /**
     * Rescan re-checks the integrity of the files according to the sfv-file in the directory.
     * Maybe throw an IOException if the sfv-file wasn't found, or just a FileNotFoundException.
     * Currently no idea how the rescan would feed back from an external zipscript to the client, but our internal zipscript can just use a connection.
     * @param dir the directory to be rescanned.
     * @param sectionName the section in which the event took place
     * @param user the user who initiated the scan
     * @param connection the connection of the scanning user
     * @param rescanParameters the parameters sent to "site rescan"
     */
    public void rescan(String dir, String sectionName, User user, Connection connection, String rescanParameters) {
        if (rescanPath == null || "".equals(rescanPath)) return; // don't do anything if we don't have anything specified
        // ideally we'd want this to write each line "as it happens", but then we get high coupling, which is BAAAAD
        // we could pass some intermediary status object that is just a bridge
        // <user> <group> <tagline> <section> <cwd> <rescan arguments>
        ProcessBuilder pb = new ProcessBuilder(rescanPath, user.getUsername(), user.getPrimaryGroup(), user.getTagline(), sectionName, dir, rescanParameters);
        boolean atleastOne200MessageSent = false;
        BufferedReader stdout = null;
        Process p = null;
        try {
            p = pb.start();
            stdout = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = stdout.readLine()) != null) {
                connection.respond("200- " + line);
                atleastOne200MessageSent = true;
            }
            p.waitFor();
            connection.respond("200 rescan complete");
        } catch (InterruptedException e) {
            Logging.getErrorLog().reportError("External zipscript interrupted while doing rescan in directory: " + dir + " because: " + e.getMessage());
            connection.respond((atleastOne200MessageSent ? "200 " : "500 ") + "External zipscript interrupted while doing rescan in directory: " + dir + " because: " + e.getMessage());
        } catch (IOException e) {
            Logging.getErrorLog().reportError("External zipscript I/O error while doing rescan in directory: " + dir + " because: " + e.getMessage());
            connection.respond((atleastOne200MessageSent ? "200 " : "500 ") + "External zipscript I/O error while doing rescan in directory: " + dir + " because: " + e.getMessage());
        } finally {

            // _todo: maybe we have to close all the streams for this object, as described in here:
            // http://saloon.javaranch.com/cgi-bin/ubb/ultimatebb.cgi?ubb=get_topic&f=38&t=000997
            // bottom
            // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4523660
            // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6344176
            // so, yeah, they all need to be closed manually, in finalize blocks, just like all the other closes we are now moving to finalize blocks
            try {
                if (stdout != null) {
                    stdout.close();
                }
            } catch (IOException e) {
                Logging.getErrorLog().reportException("Failed to close input stream", e);
                //e.printStackTrace();
            }
            try {
                if (p != null) {
                    p.getInputStream().close();
                    p.getOutputStream().close();
                    p.getErrorStream().close();
                }
            } catch (IOException e) {
                Logging.getErrorLog().reportException("Failed to close native streams", e);
                //e.printStackTrace();
            }
        }
    }

    public ProcessResult process(File file, long checksum, long bytesTransferred, long transferTime, User user, String sectionName, long speed) {
        if (uploadPath == null || "".equals(uploadPath)) return ProcessResult.OK; // don't do anything if we don't have anything specified
        // this does the quoting all by itself! =)
        // <absolute filepath> <crc> <user> <group> <tagline> <speed> <section>
        try {
            return Shell.execute(uploadPath, file.getAbsolutePath(), Long.toHexString(checksum), user.getUsername(), user.getPrimaryGroup(), user.getTagline(), String.valueOf(speed), sectionName);
        } catch (InterruptedException e) {
            Logging.getErrorLog().reportError(e.getMessage());
        } catch (IOException e) {
            Logging.getErrorLog().reportError(e.getMessage());
        }
        return ProcessResult.FAILURE;
    }

    /**
     * For external zipscripts, this should probably trigger the postdel (/bin/postdel) script
     */
    public void delete(File file, User user, String sectionName) {
        if (deletePath == null || "".equals(deletePath)) return; // don't do anything if we don't have anything specified
        /*
        ./zipscript/src/postdel <user> <group> <tagline> <section> <DELE command>
         */
        try {
            Shell.execute(deletePath, user.getUsername(), user.getPrimaryGroup(), user.getTagline(), sectionName, file.getAbsolutePath());
        } catch (InterruptedException e) {
            Logging.getErrorLog().reportError(e.getMessage());
        } catch (IOException e) {
            Logging.getErrorLog().reportError(e.getMessage());
        }
    }
}
