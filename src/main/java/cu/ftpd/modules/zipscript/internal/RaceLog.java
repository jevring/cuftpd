/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.zipscript.internal;

import cu.ftpd.logging.Formatter;
import cu.ftpd.logging.Logging;
import cu.ftpd.user.User;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.Locale;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-22 : 19:25:38
 * @version $Id: RaceLog.java 258 2008-10-26 12:47:23Z jevring $
 */
public class RaceLog {
    protected static DateFormat time = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy ", Locale.ENGLISH);
    // filename, racename, nFiles, estimated_size, section, username, group
    protected static final MessageFormat sfv = new MessageFormat("SFV: \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\"");
    // path, username, groupname, tagline
    protected static final MessageFormat delfile = new MessageFormat("DELETE: \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\"");
    // path, username, groupname, tagline
    protected static final MessageFormat leader = new MessageFormat("NEWLEADER: \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\" \"{7}\" \"{8}\"");
    // racename, section, username, group, tagline, speed
    protected static final MessageFormat racefmt = new MessageFormat("RACE: \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\"");
    // racename, section, leading_user, leading_users_group, user_size, user_files, user_speed, leading_group, group_size, group_files, group_speed
    protected static final MessageFormat halfway = new MessageFormat("HALFWAY: \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\" \"{7}\" \"{8}\" \"{9}\" \"{10}\"");
    // racename, section, username, group, tagline, speed
    protected static final MessageFormat update = new MessageFormat("UPDATE: \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\"");
    // 0=racename, 1=section, 2=files, 3=size, 4=combined_speed, 5=time, 6=number_of_racers, 7=number_of_groups, 8=fastest_user/9=group/10=speed, 11=slowest_user/12=group/13=speed
    protected static final MessageFormat complete = new MessageFormat("COMPLETE: \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\" \"{7}\" \"{8}\" \"{9}\" \"{10}\" \"{11}\" \"{12}\" \"{13}\"");
    // racename, section, position, username, group, size, number_of_files, speed (percentage is missing. percentage of what, files or bytes?)
    protected static final MessageFormat ustats = new MessageFormat("USERSTATS: \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\" \"{7}\"");
    // racename, section, position, group, size, number_of_files, speed (percentage is missing. percentage of what, files or bytes?)
    protected static final MessageFormat gstats = new MessageFormat("GROUPSTATS: \"{0}\" \"{1}\" \"{2}\" \"{3}\" \"{4}\" \"{5}\" \"{6}\"");
    // username, groupname, releasename
    protected static final MessageFormat incomplete = new MessageFormat("INCOMPLETE: \"{0}\" \"{1}\" \"{2}\"");

    public void raceComplete(Race race) {
        LinkedList<Racer> racers = race.getRacersInWinningOrder();
        Racer winner = racers.getFirst();
        Racer looser = racers.getLast();

        // 0=racename, 1=section, 2=files, 3=final_size, 4=combined_speed, 5=time, 6=number_of_racers, 7=number_of_groups, 8=fastest_user/9=group/10=speed, 11=slowest_user/12=group/13=speed
        Logging.getEventLog().log(complete.format(new Object[] {
                race.getName(), //0
                race.getSectionName(),
                race.getNumberOfExpectedFiles(),
                String.valueOf(race.getSize()),
                Formatter.speedFromKBps(race.getFinalRaceSpeed()),
                Formatter.shortDuration((race.getEndtime() - race.getStarttime()) / 1000), // 5
                race.getNumberOfRacers(),
                race.getGroups().size(), // 7
                winner.getUsername(),
                winner.getGroup(),
                Formatter.speedFromKBps(winner.getSpeed()),  //10
                looser.getUsername(),
                looser.getGroup(),
                Formatter.speedFromKBps(looser.getSpeed()) // 13
        }));
    }

    public void userStats(Race race) {
        int i = 0;
        for (Racer racer : race.getRacersInWinningOrder()) {
            // racename, section, position, username, group, size, number_of_files, speed (percentage is missing. percentage of what, files or MB?)
            Logging.getEventLog().log(ustats.format(new Object[] {race.getName(), race.getSectionName(), i++, racer.getUsername(), racer.getGroup(), Formatter.size(racer.getBytesRaced()), racer.getNumberOfFiles(), Formatter.speedFromKBps(racer.getSpeed())}));
        }
    }

    public void groupStats(Race race) {
        int i = 0;
        for (RaceGroup group : race.getRaceGroupsInWinningOrder()) {
            // racename, section, position, username, group, size, number_of_files, speed (percentage is missing. percentage of what, files or MB?)
            Logging.getEventLog().log(gstats.format(new Object[] {race.getName(), race.getSectionName(), i++, group.getName(), Formatter.size(group.getBytesTransferred()), group.getNumberOfFiles(), Formatter.speedFromKBps(group.getSpeed())}));
       }
    }

    public void fileDeleted(String path, User user) {
        Logging.getEventLog().log(delfile.format(new Object[] {path, user.getUsername(), user.getPrimaryGroup(), user.getTagline()}));
    }

    public void firstRacer(Race race, User user) {
        Logging.getEventLog().log(update.format(new Object[] {race.getName(), race.getSectionName(), user.getUsername(), user.getPrimaryGroup(), user.getTagline(), Formatter.speedFromKBps(race.getRacer(user.getUsername()).getSpeed())}));
    }

    public void dirHalfway(Race race) {
        //triggered when 50+ percent of the sfv-enabled dir is filled
        Racer raceLeader = race.getRacer(race.getLeader());
        // racename, section, leading_user, leading_users_group, user_size, user_files, user_speed, leading_group, group_size, group_files, group_speed
        Logging.getEventLog().log(halfway.format(new Object[] {
                race.getName(),
                race.getSectionName(),
                raceLeader.getUsername(),
                raceLeader.getGroup(),
                Formatter.size(raceLeader.getBytesRaced()),
                raceLeader.getNumberOfFiles(),
                Formatter.speedFromKBps(raceLeader.getSpeed()),
                race.getLeadingGroup().getName(),
                Formatter.size(race.getLeadingGroup().getBytesTransferred()),
                race.getLeadingGroup().getNumberOfFiles(),
                Formatter.speedFromKBps(race.getLeadingGroup().getSpeed())
        }));
    }

    public void newRacer(Race race, User user) {
        // this is triggered when someone completes an upload in an sfv-enabled dir that hasn't uploaded there before
        Logging.getEventLog().log(racefmt.format(new Object[] {race.getName(), race.getSectionName(), user.getUsername(), user.getPrimaryGroup(), user.getTagline(), Formatter.speedFromKBps(race.getRacer(user.getUsername()).getSpeed())}));
    }

    public void newLeader(Race race, String oldLeader, String newLeader) {
        Racer oldLeaderRacer = race.getRacer(oldLeader);
        Racer newLeaderRacer = race.getRacer(newLeader);

        // this is triggered when one user surpasses the leader in the current race for the specified dir
        // racename, section, oldleader, oldLeaderGroup, newleader, newLeaderGroup, new_leader_size, new_leader_files, new_leader_speed
        Logging.getEventLog().log(leader.format(new Object[] {
                race.getName(),
                race.getSectionName(),
                oldLeaderRacer.getUsername(),
                oldLeaderRacer.getGroup(),
                newLeaderRacer.getUsername(),
                newLeaderRacer.getGroup(),
                Formatter.size(newLeaderRacer.getBytesRaced()),
                newLeaderRacer.getNumberOfFiles(),
                Formatter.speedFromKBps(newLeaderRacer.getSpeed())
        }));
    }

    public void newSfv(String filename, Race race, User user) {
        Logging.getEventLog().log(sfv.format(new Object[] {filename, race.getName(), race.getNumberOfExpectedFiles(), race.getEstimatedSize(), race.getSectionName(), user.getUsername(), user.getPrimaryGroup()}));
    }

    public void raceIncomplete(Race race, User user) {
        Logging.getEventLog().log(incomplete.format(new Object[] {user.getUsername(), user.getPrimaryGroup(), race.getName()}));
    }

}
