/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.modules.zipscript.internal;

import cu.ftpd.logging.Formatter;
import cu.ftpd.logging.Logging;

import java.io.*;
import java.util.ArrayList;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-26 : 23:56:48
 * @version $Id: RaceFormatter.java 258 2008-10-26 12:47:23Z jevring $
 */
public class RaceFormatter {
    protected static final ArrayList<String> format = new ArrayList<String>();

    public RaceFormatter(File template) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(template), "iso-8859-1"));
            String line;
            while ((line = in.readLine()) != null) {
                format.add(line);
            }
        } catch (IOException e) {
            Logging.getErrorLog().reportError("Error reading race template file: " + e.getClass() + ":" + e.getMessage());
            format.clear();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String format(Race race) {
        if (format.isEmpty()) {
            return getDefaultFormat(race);
        } else {
            return getTemplateFormat(race);
        }
    }

    protected String getTemplateFormat(Race race) {
        //http://java.sun.com/javase/6/docs/api/java/util/Formatter.html#syntax
        StringBuilder sb = new StringBuilder(750);
        sb.append(format.get(0)).append("\r\n");
        sb.append(String.format(format.get(1), race.getName())).append("\r\n"); 
        sb.append(format.get(2)).append("\r\n");
        sb.append(String.format(format.get(3), race.getSectionName())).append("\r\n");
        sb.append(String.format(format.get(4), race.getNumerOfCurrentFiles() + "/" + race.getNumberOfExpectedFiles())).append("\r\n");
        sb.append(String.format(format.get(5), Formatter.size(race.getSize()))).append("\r\n"); // MB
        sb.append(String.format(format.get(6), Formatter.speedFromKBps((race.isComplete() ? race.getFinalRaceSpeed() : race.getCurrentRaceSpeed())))).append("\r\n");
        sb.append(format.get(7)).append("\r\n");
        sb.append(format.get(8)).append("\r\n");
        sb.append(format.get(9)).append("\r\n");
        for (Racer racer : race.getRacers().values()) {
            sb.append(String.format(format.get(10), racer.getUsername() + '/' + racer.getGroup(), String.valueOf(racer.getNumberOfFiles()), Formatter.size(racer.getBytesRaced()), Formatter.speedFromKBps(racer.getSpeed()))).append("\r\n");
        }
        sb.append(format.get(11)).append("\r\n");
        sb.append(format.get(12)).append("\r\n");
        sb.append(format.get(13)).append("\r\n");
        for (RaceGroup group : race.getGroups().values()) {
            sb.append(String.format(format.get(14), group.getName(), String.valueOf(group.getNumberOfFiles()), Formatter.size(group.getBytesTransferred()), Formatter.speedFromKBps(group.getSpeed()))).append("\r\n");
        }
        sb.append(format.get(15)).append("\r\n");
        

        return sb.toString();
    }

    protected String getDefaultFormat(Race race) {
        String result = "----------------------------------------------\r\n";
        result += "Race: " + race.getName() + "\r\n";
        result += "Files: " + race.getNumerOfCurrentFiles() + '/' + race.getNumberOfExpectedFiles() + "\r\n";
        result += "Speed: " + Formatter.speedFromKBps((race.isComplete() ? race.getFinalRaceSpeed() : race.getCurrentRaceSpeed())) + "\r\n";
        result += "Racers: " + race.getNumberOfRacers() +  "\r\n";
        result += "Leader: " + race.getLeader() + "\r\n";
        result += "----------------------------------------------\r\n";
        return result;
    }
}
