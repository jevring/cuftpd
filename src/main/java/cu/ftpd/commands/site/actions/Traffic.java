/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.commands.site.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.logging.Formatter;
import cu.ftpd.user.User;
import cu.ftpd.user.UserPermission;
import java.util.Set;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:38:22
 * @version $Id: Traffic.java 262 2008-10-30 21:29:48Z jevring $
 */
public class Traffic extends Action {
    public Traffic() {
        super("traffic");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
         if (user.hasPermission(UserPermission.TRAFFIC)) {
            // check all the users that are connected
            // add the number of connections that are downloading and add their speed
            // add the connections that are uploading and add their speed
            long uploadSpeed = 0;
            int uploaders = 0;
            long downloadSpeed = 0;
            int downloaders = 0;
            Set<Connection> conns = Server.getInstance().getConnections();
            synchronized(conns) {
               for (Connection c : conns) {
                   // hide user if user has the property hidden, or the directory in which the user operates is hidden
                   User u = c.getUser();
                   if ((u.isHidden() && !user.hasPermission(UserPermission.SEE_HIDDEN)) || !ServiceManager.getServices().getPermissions().canSee(user, u, c.getCurrentDir())) {
                       continue;
                   }
                   if (c.isDownloading()) {
                       downloaders++;
                       downloadSpeed += c.getCurrentSpeed();
                   } else if (c.isUploading()) {
                       uploaders++;
                       uploadSpeed += c.getCurrentSpeed();
                   }
               }
            }
            connection.respond("200 " + uploaders + " uploads at " + Formatter.speedFromKBps(uploadSpeed) + ", " + downloaders + " downloads at " + Formatter.speedFromKBps(downloadSpeed));
        } else {
            connection.respond("531 Permission denied.");
        }
    }
}
