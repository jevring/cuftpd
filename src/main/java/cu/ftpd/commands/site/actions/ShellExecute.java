/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.commands.site.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.logging.Formatter;
import cu.ftpd.logging.Logging;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.ftpd.user.userbases.NoSuchUserException;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.File;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-04 - 16:47:35
 * @version $Id: ShellExecute.java 313 2011-10-30 12:37:39Z jevring $
 */
public class ShellExecute extends Action {
    private final String executablePath;
    private final boolean addResponseCode;

    public ShellExecute(String name, String executablePath, boolean addResponseCode) {
        super(name);
        this.executablePath = executablePath;
        this.addResponseCode = addResponseCode;
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        String siteCommandParameters = Formatter.join(parameterList, 0, parameterList.length, " ");
        // note: we include the name of the command, in case someone wants to use the same script of variable for different purposes.
        String group = user.getPrimaryGroup();
        // this will return "default" if the user has no primary group
        // NOTE: the section here is current, because we don't care about the command being issued, since it could have been (and likely is) custom, and then we don't know how to parse whatever is happening to get some other section
        ProcessBuilder pb = new ProcessBuilder(executablePath, user.getUsername(), group, user.getTagline(), fs.getCurrentSection().getName(), fs.getRealParentWorkingDirectoryPath(), siteCommandParameters);
        pb.directory(fs.getPwdFile()); // sets the working directory for the process

        Process p = null;
        try {
            p = pb.start();
            int rc = p.waitFor();
            String out = getStdOut(p);
            if (addResponseCode) {
                if (rc == 0) {
                    connection.reply(200, out, true);
                    connection.respond("200 OK");
                } else {
                    connection.reply(500, out, true);
                    connection.respond("500 FAILURE");
                }
            } else {
                connection.respond(out);
            }
        } catch (IOException e) {
            connection.respond("500 Something went wrong when executing: " + this.name + " Reason: " + e.getMessage());
        } catch (InterruptedException e) {
            connection.respond("500 Process was interrupted while executing " + this.name);
        } finally {
            try {
                if (p != null) {
                    p.getInputStream().close();
                    p.getOutputStream().close();
                    p.getErrorStream().close();
                }
            } catch (IOException e) {
                Logging.getErrorLog().reportException("Failed to close native streams", e);
                //e.printStackTrace();
            }
        }
    }
    
    private String getStdOut(Process p) throws IOException {
        // This is the same as in ShellZipscript, but until we (if we ever) create a helper class for external processes, we'll just keep two copies of this rather simple function
        BufferedReader stdout = new BufferedReader(new InputStreamReader(p.getInputStream(), "ISO-8859-1"));
        StringBuilder sb = new StringBuilder(4096);
        char[] cs = new char[8192];
        int len;

        try {
            while((len = stdout.read(cs)) != -1) {
                // this way we don't have to do readline, since we'll split it up in later processing anyway
                // this is the fastest way to read, plus we're using the same buffer size as the BufferedReader
                sb.append(cs,0, len);
            }
        } finally {
            stdout.close();
        }
        return sb.toString();
    }
}
