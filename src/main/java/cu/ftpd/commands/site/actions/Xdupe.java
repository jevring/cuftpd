/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote 
 *    products derived from this software without specific prior written 
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.commands.site.actions;

import cu.ftpd.Connection;
import cu.ftpd.Server;
import cu.ftpd.ServiceManager;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.user.User;
import cu.ftpd.user.userbases.NoSuchUserException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-okt-26 : 23:37:36
 * @version $Id: Xdupe.java 262 2008-10-30 21:29:48Z jevring $
 */
public class Xdupe extends Action {
    public Xdupe() {
        super("xdupe");
    }

    public void execute(String[] parameterList, Connection connection, User user, FileSystem fs) {
        if (parameterList.length > 1) {
            // we're setting the xdupe
            try {
                int txdupe = Integer.parseInt(parameterList[1]);
                if (txdupe < 5) {
                    connection.setXdupe(txdupe);
                    connection.respond("200 Activated extended dupe mode " + connection.getXdupe() + '.');
                } else {
                    connection.respond("500 Unsupported xdupe mode.");
                }
            } catch (NumberFormatException e) {
                connection.respond("500 Can't set xdupe mode. " + parameterList[1] + " is not a number.");
            }
        } else {
            // we're asking about the xdupe
            if (connection.getXdupe() == 0) {
                connection.respond("200 Extended dupe mode is disabled.");
            } else {
                connection.respond("200 Extended dupe mode " + connection.getXdupe() + " is enabled.");
            }
        }
    }
}
