/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.commands.transfer;

import cu.ftpd.Connection;
import cu.ftpd.NoCompatibleNetworkInterfaceFoundException;
import cu.ftpd.ServiceManager;
import cu.ftpd.logging.Logging;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.ServerSocket;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @version $Id: CommandPASV.java 310 2011-02-28 21:15:37Z jevring $
 * @since 2008-jan-09 - 11:36:45
 */
public class CommandPASV extends Thread {
    private boolean epsv = false;
    private final Connection connection;
    private final String parameters;

    public CommandPASV(boolean epsv, Connection connection, String parameters) {
        super(connection.getName() + ":PASV");
        this.epsv = epsv;
        this.connection = connection;
        this.parameters = parameters;
    }

    public void prepareAndStart() {
        // because we hold a lock while waiting for this to timeout, we have to abort any previous connections before the lock is held
        connection.stopListening();
        if (!epsv) {
            connection.dataConnectionSemaphore.acquireUninterruptibly();
            try {
                connection.createPassiveServerSocket(Connection.DataConnectionProtocolFamily.IPV4);
                String pasvString = connection.getPassiveModeString();
                connection.respond("227 Entering Passive Mode (" + pasvString + ')');
                this.start();
            } catch (IOException e) {
                // NOTE: maybe this should be 425 or 426 if it happens after the 227 has been sent
                connection.respond("500 Failed to create passive socket: " + e.getMessage());
                Logging.getErrorLog().reportException("Failed to create passive socket", e);
                connection.dataConnectionSemaphore.release();
                //e.printStackTrace();
            } catch (NoCompatibleNetworkInterfaceFoundException e) {
                // this is VERY unlikely.
                // this CAN, however, happen if we have an ipv6 ONLY server
                connection.respond("500 Failed to create passive socket: " + e.getMessage());
                Logging.getErrorLog().reportException("Failed to create passive socket", e);
                connection.dataConnectionSemaphore.release();
            }
            // can't unlock in a finally-clause here, as this is done when the thread dies.
            // we still have to unlock in the case of an exception, which we didn't do before
        } else {
            Connection.DataConnectionProtocolFamily protocolFamily;
            // the parameters can either be protocol type (1=ipv4, 2=ipv6). If this is the case, then that protocol MUST be used. If the protocol is not available, respond with a 522
            if ("ALL".equalsIgnoreCase(parameters)) {
                connection.setUseEpsvOnly();
                connection.respond("200 EPSV ALL OK");
                return;
            } else if ("EXTENDED".equalsIgnoreCase(parameters)) {
                if ("deny".equalsIgnoreCase(ServiceManager.getServices().getSettings().get("/epsv/extended_epsv_response"))) {
                    connection.respond("501 Extended EPSV mode denied.");
                } else {
                    connection.setExtendedEpsvResponse(true);
                    connection.respond("200 EPSV will return extra information.");
                }
                return;
            } else if ("NORMAL".equalsIgnoreCase(parameters)) {
                if ("force".equalsIgnoreCase(ServiceManager.getServices().getSettings().get("/epsv/extended_epsv_response"))) {
                    connection.respond("501 Normal EPSV mode denied.");
                } else {
                    connection.setExtendedEpsvResponse(false);
                    connection.respond("200 EPSV will abide by the rfc.");
                }
                return;
            } else if ("MODE".equalsIgnoreCase(parameters)) {
                connection.respond("200 " + ServiceManager.getServices().getSettings().get("/epsv/extended_epsv_response") + " (" + (connection.useExtendedEpsvResponse() ? "EXTENDED" : "NORMAL") + ")");
                return;
            } else if ("1".equals(parameters) && ServiceManager.getServices().getSettings().get("/epsv/valid_protocol_families").contains("1")) {
                protocolFamily = Connection.DataConnectionProtocolFamily.IPV4;
            } else if ("2".equals(parameters) && ServiceManager.getServices().getSettings().get("/epsv/valid_protocol_families").contains("2")) {
                protocolFamily = Connection.DataConnectionProtocolFamily.IPV6;
            } else if (parameters == null || "".equals(parameters)) {
                // default to whatever the control connection is
                protocolFamily = Connection.DataConnectionProtocolFamily.DEFAULT;
            } else {
                connection.respond("522 Network protocol not supported, use (" + ServiceManager.getServices().getSettings().get("/epsv/valid_protocol_families") + ")");
                return;
            }
            connection.dataConnectionSemaphore.acquireUninterruptibly();
            try {
                ServerSocket passiveServerSocket = connection.createPassiveServerSocket(protocolFamily);
                String protocolVersion;
                if (passiveServerSocket.getInetAddress() instanceof Inet4Address) {
                    protocolVersion = "1";
                } else {
                    protocolVersion = "2";
                }

                if (connection.useExtendedEpsvResponse()) {
                    String hostAddress = passiveServerSocket.getInetAddress().getHostAddress();
                    int i = hostAddress.indexOf("%");
                    if (i > -1) {
                        // this trims the "scope" identifier. What is sometimes referred to as the zone.
                        // I don't know whether "scope" or "zone" is correct, but it violates the ipv6 address format.
                        // also, since the address identified is correct even without the this identifier, we're fine.
                        // furthermore, there's a good chance this won't resolve when we try to connect from outside.
                        // and finally, it doesn't seem like Socket.connect() accepts it, as per http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6800096,
                        // so I think it is safe to leave it out. Should this change in the future, we know what to do.
                        // the strange thing is that it only shows up when we connect to the ipv4 interface of the ftpd.
                        // if we connect via ipv6 on the control connection, the scope is never included.
                        hostAddress = hostAddress.substring(0, i);
                    }
                    connection.respond("229 Entering Extended Passive Mode (|" + protocolVersion + "|" + hostAddress + "|" + passiveServerSocket.getLocalPort() + "|)");
                } else {
                    connection.respond("229 Entering Extended Passive Mode (|||" + passiveServerSocket.getLocalPort() + "|)");
                }

                this.start();
            } catch (IOException e) {
                connection.respond("500 EPSV command failed: " + e.getMessage());
                connection.dataConnectionSemaphore.release();
            } catch (NoCompatibleNetworkInterfaceFoundException e) {
                connection.respond("522 Network protocol not supported, use (" + ServiceManager.getServices().getSettings().get("/epsv/valid_protocol_families") + ")");
                connection.dataConnectionSemaphore.release();
            }
            // can't unlock in a finally-clause here, as this is done when the thread dies.
            // we still have to unlock in the case of an exception, which we didn't do before
        }
    }

    /*

    We will use this both as a pre-stage and as a thread. instead of transfer.start(), we'll do this.start(); when we want to wait for the connection

     */

    public void run() {
        try {
            connection.enterPassiveMode();
        } catch (IOException e) {
            // don't do anything here. We want to end up here, since .accept() waits for it to happen on ABOR.
            // Connection takes care of sending the 226
            //connection.respond("425 Failed to create passive socket: " + e.getMessage());
            //e.printStackTrace();
        } finally {
            connection.dataConnectionSemaphore.release();
        }
    }
}
