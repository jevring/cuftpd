/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.commands.transfer;

import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-08 - 18:41:41
 * @version $Id: TransferThread.java 300 2010-03-09 20:48:19Z jevring $
 */
public abstract class TransferThread extends Thread {
    protected final TransferController controller;
    protected int bufferSize = 8192;
    protected long bytesTransferred = 0;
    protected long transferTime = 0;
    protected long start = 0;

    protected TransferThread(TransferController controller) {
        super("TransferThread");
        this.controller = controller;
    }

    protected TransferThread(TransferController controller, int bufferSize) {
        this(controller);
        this.bufferSize = bufferSize;
    }

    public void run() {
        try {
            start = System.currentTimeMillis();
            transfer();
            transferTime = System.currentTimeMillis() - start;
            controller.complete(bytesTransferred, transferTime);
        } catch (IOException e) {
            transferTime = System.currentTimeMillis() - start;
            controller.error(e, bytesTransferred, transferTime);
        } 
    }

    /**
     * Gets the speed of this transfer expresses in kilobytes per second as a long.
     * @return the speed of this transfer expresses in kilobytes per second as a long.
     */
    public long getSpeed() {
        double dtime = (double)transferTime / 1000.0d; // seconds, not milliseconds
        return (long)(((double)bytesTransferred/ 1024.0d) / dtime);
    }

    public abstract void transfer() throws IOException;

    /**
     * Gets a snapshot of the average speed so far.
     * @return the current speed in kilobytes per second.
     */
    public long getCurrentSpeed() {
        transferTime = System.currentTimeMillis() - start;
        return getSpeed();
    }
}
