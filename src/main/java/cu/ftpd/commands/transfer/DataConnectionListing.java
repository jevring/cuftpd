/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.commands.transfer;

import cu.ftpd.Connection;
import cu.ftpd.logging.Logging;
import cu.ftpd.filesystem.FileSystem;
import cu.ftpd.filesystem.permissions.PermissionDeniedException;

import java.io.*;
import java.net.Socket;
import java.security.AccessControlException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-09 - 11:03:18
 * @version $Id: DataConnectionListing.java 300 2010-03-09 20:48:19Z jevring $
 */
public class DataConnectionListing implements TransferController {
    // todo: prime candidates for being turned in to enums! make it so, and find others, too!
    public static final int LIST = 0;
    public static final int NLST = 1;
    public static final int MLSD = 2;
    // while this does do LIST, or also does NLST and will probably do MLST/D in the future, so we might have to rename it then
    private TransferThread transfer;
    private final FileSystem fs;
    private final Connection connection;
    private final String path;
    private final boolean encryptedDataConnection;
    private Socket dataConnection;
    private int listType;

    public DataConnectionListing(int listType, FileSystem fs, Connection connection, String path, boolean encryptedDataConnection) {
        this.listType = listType;
        this.fs = fs;
        this.connection = connection;
        this.path = path;
        this.encryptedDataConnection = encryptedDataConnection;
    }

    public void start() {

        connection.dataConnectionSemaphore.acquireUninterruptibly();
        // note: we must collect the data connection here, because if we don't, then it will be null when we close due to an error, but won't be closed in Connection

        try {
            this.dataConnection = connection.getDataConnection();
            //synchronized (connection.dataConnectionSemaphore) {
            try {
                // NOTE: dir not actually denoting a directory is handled by FileSystem.nlst()
                //UserManager.getMetadataHandler().enterDirectory(dir);
                java.util.List<String> lines;
                if (listType == LIST) {
                    lines = fs.list(path);
                    lines.add(0, "total 1337");
                } else if (listType == NLST) {
                    lines = fs.nlst(path == null ? "" : path);
                } else if (listType == MLSD) {
                    lines = fs.mlsd(path == null ? "" : path);
                } else {
                    close();
                    connection.respond("500 No such list type");
                    return;
                }
                transfer = new CharacterListTransferThread(this, lines, new BufferedWriter(new OutputStreamWriter(dataConnection.getOutputStream(), "ISO-8859-1")));
                connection.respond("150 Opening " + fs.getType() + " mode data connection for list" + (encryptedDataConnection ? " with SSL/TLS encryption." : "."));
                transfer.start();
            } catch (FileNotFoundException e) {
                // it's ok to echo the entire message here, because it just contains the normalized ftp-path
                close();
                connection.respond("550 File not found: " + e.getMessage());
            } catch (PermissionDeniedException e) {
                close();
                connection.respond("531 Forbidden: " + e.getMessage());
            } catch (IOException e) {
                close();
                connection.respond("550 " + e.getMessage());
            }
            //}
        } catch (AccessControlException e) {
            close();
            connection.respond("531 " + e.getMessage());
        } catch (IllegalStateException e) {
            /*
                425 Can't open data connection.
                426 Connection closed; transfer aborted.
             */
            // this is thrown when there is no data connection socket available
            close();
            connection.respond("425 " + e.getMessage());
            // NOTE: was 500, but 425 is much more appropriate.
        }
    }

    private void close() {
        if (dataConnection != null) {
            try {
                dataConnection.close();
            } catch (IOException e) {
                Logging.getErrorLog().reportException("Failed to close data connection", e);
                //e.printStackTrace();
            }
        }
        // NOTE: if we reactivate this, we will have to make sure that we check if dir != null first, otherwise we get an NPE deeper inside
        //UserManager.getMetadataHandler().exitDirectory(dir);
        connection.dataConnectionSemaphore.release();
    }

    public void error(Exception e, long bytesTransferred, long transferTime) {
        // close(); // close is called by complete(..) anyway
        connection.reportTransferFailure(e.getMessage());
    }

    public void complete(long bytesTransferred, long transferTime) {
        close();
        //connection.respond("226- " + type + " transfer complete.");
        connection.statline(transfer.getSpeed());
    }

    public boolean isRunning() {
        return transfer != null && transfer.isAlive();
    }

    public long getSpeed() {
        if (transfer != null) {
            return transfer.getCurrentSpeed();
        } else {
            return 0;
        }
    }

}
