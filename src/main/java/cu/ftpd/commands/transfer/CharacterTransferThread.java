/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.commands.transfer;

import java.io.Reader;
import java.io.Writer;
import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-08 - 18:52:26
 * @version $Id: CharacterTransferThread.java 272 2008-11-07 23:16:08Z jevring $
 */
public class CharacterTransferThread extends TransferThread {
    protected final char[] buf;
    protected final Reader in;
    protected final Writer out;

    public CharacterTransferThread(TransferController controller, Reader in, Writer out) {
        super(controller);
        this.in = in;
        this.out = out;
        this.buf = new char[bufferSize];
    }

    public CharacterTransferThread(TransferController controller, int bufferSize, Reader in, Writer out) {
        super(controller, bufferSize);
        this.in = in;
        this.out = out;
        this.buf = new char[bufferSize];
    }

    public void transfer() throws IOException {
        int len;
        while ((len = in.read(buf)) != -1) {
            out.write(buf, 0, len);
            bytesTransferred += len;
        }
        out.flush();
    }
}
