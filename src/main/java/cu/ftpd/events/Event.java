/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.events;

import cu.ftpd.user.User;

import java.util.Map;
import java.util.HashMap;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-25 - 14:11:40
 * @version $Id: Event.java 294 2009-03-04 22:10:42Z jevring $
 */
public class Event {
    /**
     * The type of the event, such as Event.CREATE_DIRECTORY (0).
     */
    protected final int type;
    /**
     * The executing user.
     */
    protected final User user;


    protected final Map<String, String> properties = new HashMap<>();

    // All events have an environment (except shutdown and system events.)
    // Those will simply not be events that can be handled externally
    protected final String realPwd;
    protected final String ftpPwd;

    public static final int CREATE_DIRECTORY = 0;
    public static final int REMOVE_DIRECTORY = 1;
    public static final int DELETE_FILE = 2;
    public static final int RENAME_FROM = 3;
    public static final int RENAME_TO = 4;

    public static final int SITE_COMMAND = 100;

    public static final int UPLOAD = 1000;
    public static final int DOWNLOAD = 1001;

    private static final Map<String, Integer> names = new HashMap<>();
    static {
        names.put("createdirectory", CREATE_DIRECTORY);
        names.put("removedirectory", REMOVE_DIRECTORY);
        names.put("deletefile", DELETE_FILE);
        names.put("renamefrom", RENAME_FROM);
        names.put("renameto", RENAME_TO);
        names.put("sitecommand", SITE_COMMAND);
        names.put("upload", UPLOAD);
        names.put("download", DOWNLOAD);
    }

    // todo: this is a prime candidate for remaking into an 'enum'. Look for others as well

    public static int resolve(String event) {
        // it this throws a NullPointerException, it is because the object didn't exist in the map, and the implicit integer evaluation (or somesuch) fails
        return names.get(event);
    }

    public Event(int type, User user, String realPwd, String ftpPwd) {
        this.type = type;
        this.user = user;
        this.realPwd = realPwd;
        this.ftpPwd = ftpPwd;
    }

    public int getType() {
        return type;
    }

    public User getUser() {
        return user;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperty(String property, String value) {
        properties.put(property, value);
    }

    public String getRealPwd() {
        return realPwd;
    }

    public String getFtpPwd() {
        return ftpPwd;
    }

    public String getProperty(String property) {
        return properties.get(property);
    }

	public String getProperty(String property, String valueIfNotPresent) {
		String value = properties.get(property);
		if (value != null) {
			return value;	
		} else {
			return valueIfNotPresent;
		}
		
	}
}
