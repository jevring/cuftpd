/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.ftpd.events;

import cu.ftpd.Connection;
import cu.ftpd.logging.Logging;
import cu.shell.ProcessResult;
import cu.shell.Shell;

import java.io.IOException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-25 - 22:37:33
 * @version $Id: ShellEventHandler.java 294 2009-03-04 22:10:42Z jevring $
 */
public class ShellEventHandler implements BeforeEventHandler, AfterEventHandler {
    private final String executablePath;

    public ShellEventHandler(String executablePath) {
        this.executablePath = executablePath;
    }

    public void handleEvent(Event event) {
        try {
            execute(event, "after");
        } catch (IOException | InterruptedException e) {
            Logging.getErrorLog().reportError(e.getMessage());
        }
    }

    public boolean handleEvent(Event event, Connection connection) {
        try {

            ProcessResult pcr = execute(event, "before");
            if (pcr.message != null && !"".equals(pcr.message)) {
                connection.respond(pcr.message);
            }
            return (pcr.exitvalue == ProcessResult.OK.exitvalue);
        } catch (IOException | InterruptedException e) {
            connection.respond("500 " + e.getMessage());
            return false;
        }
    }

    private ProcessResult execute(Event event, String time) throws IOException, InterruptedException {
        ProcessResult pcr;
        if (event.getType() == Event.SITE_COMMAND) {
            pcr = Shell.execute(
                    executablePath,
                    time,
                    String.valueOf(event.getType()),
                    event.getUser().getUsername(),
                    event.getUser().getPrimaryGroup(),
                    event.getUser().getTagline(),
                    event.getRealPwd(),
                    event.getFtpPwd(),
                    // ---
                    event.getProperty("site.command")
            );
        } else if (event.getType() == Event.UPLOAD || event.getType() == Event.DOWNLOAD) {
            pcr = Shell.execute(
                    executablePath,
                    time,
                    String.valueOf(event.getType()),
                    event.getUser().getUsername(),
                    event.getUser().getPrimaryGroup(),
                    event.getUser().getTagline(),
                    event.getRealPwd(),
                    event.getFtpPwd(),
                    // ---
                    event.getProperty("transfer.state"),
                    event.getProperty("transfer.bytesTransferred"),
                    event.getProperty("transfer.timeTaken"),
                    event.getProperty("filesystem.transfer.type"),
                    event.getProperty("connection.remote.host")
            );
        } else if (event.getType() == Event.RENAME_TO || event.getType() == Event.RENAME_FROM) {
            pcr = Shell.execute(
                    executablePath,
                    time,
                    String.valueOf(event.getType()),
                    event.getUser().getUsername(),
                    event.getUser().getPrimaryGroup(),
                    event.getUser().getTagline(),
                    event.getRealPwd(),
                    event.getFtpPwd(),
                    // ---
                    event.getProperty("rename.source.path.real"),
                    event.getProperty("rename.source.path.ftp"),
                    event.getProperty("rename.target.path.real", ""),
                    event.getProperty("rename.target.path.ftp", ""),
                    event.getProperty("file.isDirectory"),
                    event.getProperty("file.size", "")
            );
        } else if (event.getType() == Event.DELETE_FILE) {
            pcr = Shell.execute(
                    executablePath,
                    time,
                    String.valueOf(event.getType()),
                    event.getUser().getUsername(),
                    event.getUser().getPrimaryGroup(),
                    event.getUser().getTagline(),
                    event.getRealPwd(),
                    event.getFtpPwd(),
                    // ---
                    event.getProperty("file.path.real"),
                    event.getProperty("file.path.ftp"),
                    event.getProperty("file.size")
            );
        } else {
            pcr = Shell.execute(
                    executablePath,
                    time,
                    String.valueOf(event.getType()),
                    event.getUser().getUsername(),
                    event.getUser().getPrimaryGroup(),
                    event.getUser().getTagline(),
                    event.getRealPwd(),
                    event.getFtpPwd(),
                    // ---
                    event.getProperty("file.path.real"),
                    event.getProperty("file.path.ftp")
            );
        }
        return pcr;
    }
}
