package cu.ftpd.persistence;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-nov-20 : 00:57:21
 * @version $Id: Persistent.java 258 2008-10-26 12:47:23Z jevring $
 */
public interface Persistent {
    public void save();
}
