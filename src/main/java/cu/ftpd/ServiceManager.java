package cu.ftpd;

/**
 * @author captain
 * @version $Id: ServiceManager.java 280 2008-11-24 18:52:18Z jevring $
 * @since 2008-okt-25 - 01:26:28
 */
public class ServiceManager {
    private static Services services = null;

    public static Services getServices() {
        return services;
    }

    public static void setServices(Services services) {
        ServiceManager.services = services;
    }

    public static void shutdown() {
        if (services != null) {
            // this can happen if we encounter a problem during configuration
            services.shutdown();
        }
    }
}
