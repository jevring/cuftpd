/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.logging;

import cu.ftpd.FtpdSettings;

import java.text.DateFormat;
import java.util.Date;
import java.io.*;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-jul-13 : 13:17:55
 * @version $Id: Formatter.java 280 2008-11-24 18:52:18Z jevring $
 */
public class Formatter {
    public static int offset; // where in the string below to insert the name of the command
    public static String headerTemplate;
    public static String footer;
    public static String line;
    public static String bar;

    public static void initialize(FtpdSettings settings) throws IOException {
        File file = new File(settings.getDataDirectory(), "/templates/lines.txt");

        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            BufferedReader in;
            in = new BufferedReader(new InputStreamReader(fis, "ISO-8859-1"));
            headerTemplate = in.readLine();
            line = in.readLine();
            footer = in.readLine();
            bar = in.readLine();
            int tOffset = 8;
            try {
                tOffset = Integer.parseInt(in.readLine());
            } catch (NumberFormatException e) {
                // this happens if people don't update lines.txt and we end up with null or "" as a parameter to Interger.parseInt()
                System.err.println("Please put a number at the last line to indicate the header offset. Using default (8)");
            }
            // since this claimed it couldn't be final if I assigned it in the catch clause, we'll have to put it here (because I do want it to be final)
            offset = tOffset;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getBar() {
        return bar;
    }

    public static String createFooter(int number) {
        return number + " " + footer;
    }

    public static String createLine(int number) {
        return number + "-" + line;
    }

    public static String createHeader(int number, String title) {
        // we COULD cache these headers, but I don't think that the impact on performance from this particular piece is that big
        char[] nameArray = (" [ " + title + " ] ").toCharArray();
        char[] headerTemplateArray = (number + "-" + headerTemplate).toCharArray();
        System.arraycopy(nameArray, 0, headerTemplateArray, offset, nameArray.length);
        return String.valueOf(headerTemplateArray);
    }

    public static String createHeader(String title) {
        return createHeader(200, title);
    }

    /**
     * Joins the strings in the provided array together with the string <code>spacer</code> between each string.
     * @param strings the array of strings to be joined
     * @param spacer the spacer to be used.
     * @return the joined string.
     * @param start where in the array to start
     * @param end where in the array to stop (not inclusive)
     */
    public static String join(String[] strings, int start, int end, String spacer) {
        String s = "";
        for (int i = start; i < end; i++) {
            s+= strings[i] + spacer;
        }
        int i = s.length() - spacer.length();
        if (i > 0) {
            return s.substring(0, s.length() - spacer.length());
        } else {
            return s;
        }
    }
    
    /**
     * Generates a human-readable line of duration based on a number of seconds.
     *
     * @param seconds the number of seconds to parse
     * @return a human-readable string based on the number of seconds supplied.
     */
    public static String duration(long seconds) {
        long days = 0;
        if (seconds >= 86400) {
            days = seconds / 86400;
            seconds -= days * 86400;
        }
        long hours = 0;
        if (seconds >= 3600) {
            hours = seconds / 3600;
            seconds -= hours * 3600;
        }
        long minutes = 0;
        if (seconds >= 60) {
            minutes = seconds / 60;
            seconds -= minutes * 60;
        }
        String duration = seconds + (seconds != 1 ? " Seconds" : " Second");
        if (minutes > 0 || hours > 0 || days > 0) {
            duration = minutes + (minutes != 1 ? " Minutes " : " Minute ") + duration;
        }
        if (hours > 0 || days > 0) {
            duration = hours + (hours != 1 ? " Hours " : " Hour ") + duration;
        }
        if (days > 0) {
            duration = days + (days != 1 ? " Days " : " Day ") + duration;
        }
        return duration;
    }

    /**
     * Delivers a duration in a short format, i.e. 1d2h3m4s.
     * NOTE: the time needs to be supplied in seconds, not milliseconds!
     * Where
     * d = days.
     * h = hours.
     * m = minutes.
     * s = seconds.
     * @param seconds the duration in SECONDS.
     * @return a formatted string representing the duration.
     */
    public static String shortDuration(long seconds) {
        long days = 0;
        if (seconds >= 86400) {
            days = seconds / 86400;
            seconds -= days * 86400;
        }
	    long hours = 0;
        if (seconds >= 3600) {
            hours = seconds / 3600;
            seconds -= hours * 3600;
        }
	    long minutes = 0;
        if (seconds >= 60) {
            minutes = seconds / 60;
            seconds -= minutes * 60;
        }
	    if (days > 0) {
		    return days + "d" + hours + "h" + minutes + "m" + seconds + "s";
	    }
	    if (hours > 0) {
		    return hours + "h" + minutes + "m" + seconds + "s";
	    }
	    if (minutes > 0) {
		    return minutes + "m" + seconds + "s";
	    }
	    return seconds + "s";
    }

    public static String speed(long size, long time) {
        double dtime = (double)time / 1000.0d; // seconds, not milliseconds
        if (dtime == 0) {
            return "0";
        }
        double speedInKBps = ((double)size / dtime) / 1024.0D;
        double speedInMBps = speedInKBps / 1024.0D;
        if (speedInKBps > 1024*1024) {
            String speed = (String.valueOf(speedInMBps)) + ".0"; // the addition of the ".0" doesn't cause a problem, because if it already has that precision, the original is used. if it doesn't, the added one is
            return speed.substring(0, speed.indexOf('.') + 2) + "MB/s";
        } else {
            String speed = (String.valueOf(speedInKBps)) + ".0"; // the addition of the ".0" doesn't cause a problem, because if it already has that precision, the original is used. if it doesn't, the added one is;
            return speed.substring(0, speed.indexOf('.') + 2) + "KB/s";
        }
    }

    public static String speedFromKBps(long speedInKBps) {
        double speedInMBps = (double)speedInKBps / 1024.0d;
        if (speedInKBps > 1024) {
            String speed = (String.valueOf(speedInMBps)) + ".0"; // the addition of the ".0" doesn't cause a problem, because if it already has that precision, the original is used. if it doesn't, the added one is;
            return speed.substring(0, speed.indexOf('.') + 2) + "MB/s";
        } else {
            String speed = (String.valueOf(speedInKBps)) + ".0"; // the addition of the ".0" doesn't cause a problem, because if it already has that precision, the original is used. if it doesn't, the added one is;
            return speed.substring(0, speed.indexOf('.') + 2) + "KB/s";
        }
    }
    private static final double kilobyte = 1024.0d;
    private static final double megabyte = 1048576.0d;
    private static final double gigabyte = 1073741824.0d;
    private static final double terrabyte = 1099511627776.0d;
    public static String size(long bytes) {
        String size;
        String sign = "";
        double b = (double)bytes;
        if (bytes < 0) {
            sign = "-";
            b = Math.abs(b);
        }
        int i;
        if (b >= terrabyte) {
            size = String.valueOf((b / terrabyte)) + ".0"; // the addition of the ".0" doesn't cause a problem, because if it already has that precision, the original is used. if it doesn't, the added one is;
            return sign + size.substring(0, size.indexOf('.') + 2) + "TB";
        } else if (b >= gigabyte) {
            size = String.valueOf((b / gigabyte)) + ".0"; // the addition of the ".0" doesn't cause a problem, because if it already has that precision, the original is used. if it doesn't, the added one is;
            return sign + size.substring(0, size.indexOf('.') + 2) + "GB";
        } else if (b >= megabyte) {
            size = String.valueOf((b / megabyte)) + ".0"; // the addition of the ".0" doesn't cause a problem, because if it already has that precision, the original is used. if it doesn't, the added one is;
            return sign + size.substring(0, size.indexOf('.') + 2) + "MB";
        } else {
            size = String.valueOf((b / kilobyte)) + ".0"; // the addition of the ".0" doesn't cause a problem, because if it already has that precision, the original is used. if it doesn't, the added one is;
            return sign + size.substring(0, size.indexOf('.') + 2) + "KB";
        }
    }

    public static String time(long timeInMs) {
        return DateFormat.getTimeInstance().format(new Date(timeInMs));
    }

    public static String datetime(long timeInMs) {
        return DateFormat.getDateTimeInstance().format(new Date(timeInMs));
    }

    public static long size(String value) {
        int multiplier = 1;
        if (value.toUpperCase().endsWith("K")) {
            multiplier = 1024;
            value = value.substring(0, value.length() - 1);
        } else if (value.toUpperCase().endsWith("M")) {
            multiplier = 1024 * 1024;
            value = value.substring(0, value.length() - 1);
        } else if (value.toUpperCase().endsWith("G")) {
            multiplier = 1024 * 1024 * 1024;
            value = value.substring(0, value.length() - 1);
        } // the default is handled because of multiplier = 1
        return Long.parseLong(value) * multiplier;
    }
}
