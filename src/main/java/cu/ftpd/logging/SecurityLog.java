/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.logging;

import cu.authentication.AuthenticationRequest;
import cu.ftpd.user.User;

import java.io.*;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-maj-27 : 17:44:20
 * @version $Id: SecurityLog.java 282 2008-11-26 18:15:43Z jevring $
 */
public class SecurityLog {
    protected PrintWriter log;
    protected static final DateFormat time = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy ", Locale.ENGLISH);
    protected static final MessageFormat logoff = new MessageFormat("LOGOFF: {0}");
    protected static final MessageFormat tooManyLoginsForUser = new MessageFormat("Logon failed (TOO MANY LOGINS FOR USER): {0} {1}@{2}:{3}");
    protected static final MessageFormat logonSuccessful = new MessageFormat("LOGON: {0} {1}@{2}:{3}");
    protected static final MessageFormat badIdentOrHost = new MessageFormat("Logon failed (BAD IDENT OR HOST): {0} {1}@{2}:{3}");
    protected static final MessageFormat userSuspended = new MessageFormat("Logon failed (USER SUSPENDED): {0} {1}@{2}:{3}");
    protected static final MessageFormat userNotFound = new MessageFormat("Logon failed (USER NOT FOUND): {0} {1}@{2}:{3}");
    protected static final MessageFormat badPassword = new MessageFormat("Logon failed (BAD PASSWORD): {0} {1}@{2}:{3}");
    protected static final MessageFormat siteFull = new MessageFormat("Logon failed (SITE FULL): {0} {1}");
    protected static final MessageFormat restart = new MessageFormat("RESTART: {0}");
    protected static final MessageFormat start = new MessageFormat("STARTUP: [SYSTEM]");
    protected static final MessageFormat stop = new MessageFormat("SHUTDOWN: {0}");
    // 0=username, 1=ident, 2=hostip, 3=hostaddress

    public SecurityLog(String logfile) throws IOException {
        try {
	        final File file = new File(logfile);
	        file.getParentFile().mkdirs();
	        log = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file, true))));
        } catch (FileNotFoundException e) {
            throw new IOException("Could not open security log for writing", e);
        }
    }

    protected SecurityLog() {
    }

    private synchronized void log(String message) {
        log.println(time.format(new Date()) + message);
        log.flush();
    }

    private String[] extractParams(AuthenticationRequest auth) {
        return new String[]{auth.getUsername(), auth.getIdent(), auth.getHost().getHostAddress(), auth.getHost().getHostName()};
    }

    public void badPassword(AuthenticationRequest auth) {
        log(badPassword.format(extractParams(auth)));
    }

    public void badIdentOrHost(AuthenticationRequest auth) {
        log(badIdentOrHost.format(extractParams(auth)));
    }

    public void userSuspended(AuthenticationRequest auth) {
        log(userSuspended.format(extractParams(auth)));
    }

    public void logonSuccessful(AuthenticationRequest auth) {
        log(logonSuccessful.format(extractParams(auth)));
    }

    public void userNotFound(AuthenticationRequest auth) {
        log(userNotFound.format(extractParams(auth)));
    }

    public void tooManyLoginsForUser(AuthenticationRequest auth) {
        log(tooManyLoginsForUser.format(extractParams(auth)));
    }

    public void siteFull(int connectionLimit, int currentNumberOfConnections) {
        log(siteFull.format(new String[] {String.valueOf(connectionLimit), String.valueOf(currentNumberOfConnections)}));
    }

    public void logoff(User user) {
        log(logoff.format(new String[] {user.getUsername()}));
    }

    public void restart(User user) {
        log(restart.format(new String[]{user.getUsername()}));
    }

    public void stop(User user) {
        if (user == null) {
            log(stop.format(new String[]{"[SYSTEM]"}));
        } else  {
            log(stop.format(new String[]{user.getUsername()}));
        }
    }
    public void start() {
        log(start.format(null));
    }

    public void shutdown() {
        log.flush();
        log.close();
    }

}
