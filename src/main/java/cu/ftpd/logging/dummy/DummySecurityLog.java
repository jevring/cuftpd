/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.logging.dummy;

import cu.authentication.AuthenticationRequest;
import cu.ftpd.logging.SecurityLog;
import cu.ftpd.user.User;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-aug-30 : 21:23:09
 * @version $Id: DummySecurityLog.java 258 2008-10-26 12:47:23Z jevring $
 */
public class DummySecurityLog extends SecurityLog {
    public void badPassword(AuthenticationRequest auth) {
    }
    public void badIdentOrHost(AuthenticationRequest auth) {
    }
    public void userSuspended(AuthenticationRequest auth) {
    }
    public void logonSuccessful(AuthenticationRequest auth) {
    }
    public void userNotFound(AuthenticationRequest auth) {
    }
    public void restart(User user) {
    }
    public void shutdown() {
    }
    public void stop(User user) {
    }
    public void start() {
    }
    public void logoff(User user) {
    }
}
