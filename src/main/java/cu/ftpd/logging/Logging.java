/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.logging;

import cu.ftpd.FtpdSettings;
import cu.ftpd.logging.dummy.*;
import cu.settings.ConfigurationException;

import java.io.IOException;
import java.io.FileNotFoundException;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-aug-19 : 21:59:18
 * @version $Id: Logging.java 280 2008-11-24 18:52:18Z jevring $
 */
public class Logging {
    private static EventLog eventLog;
    private static ErrorLog errorLog;
    private static SecurityLog securityLog;
    private static CommandLog commandLog;

    public static CommandLog getCommandLog() {
        return commandLog;
    }

    public static EventLog getEventLog() {
        return eventLog;
    }

    public static ErrorLog getErrorLog() {
        return errorLog;
    }

    public static SecurityLog getSecurityLog() {
        return securityLog;
    }

    public static void shutdown() {
        // _todo: check if these are null
        // they shouldn't be, and now that we don't do restart anymore, this won't be a problem.
        // it only becomes a problem it if crashes in mid-shutdown, before ever having been started.
        // i.e. a snowball's chance in hell
        if (securityLog != null) {
            securityLog.shutdown();
        }
        if (eventLog != null) {
            eventLog.shutdown();
        }
        if (errorLog != null) {
            errorLog.shutdown();
        }
    }

    public static void initializeErrorLog(String errorLogPath) throws IOException {
        // NOTE: we need to keep this, so that the RmiDelegateUserbaseServer can use it when it needs an error log
        // this is used by the remote userbase when it needs to initialize the error log, since it doesn't have access to the settings-object.
        if (errorLogPath == null || "".equals(errorLogPath) || "none".equals(errorLogPath)) {
            errorLog = new DummyErrorLog();
        } else {
            errorLog = new ErrorLog(errorLogPath);
        }
    }

    private static void initializeEventLog(String logPath) throws IOException {
        if (logPath == null || "".equals(logPath) || "none".equals(logPath)) {
            eventLog = new DummyEventLog();
        } else {
            eventLog = new EventLog(logPath);
        }
    }

    private static void initializeSecurityLog(String logPath) throws IOException {
        if (logPath == null || "".equals(logPath) || "none".equals(logPath)) {
            securityLog = new DummySecurityLog();
        } else {
            securityLog = new SecurityLog(logPath);
        }
    }

    public static void recreateCommandLog(int verbosity, int fileMode, String directory) throws FileNotFoundException {
        // create it temporarily, so we don't nuke an existing one without having a replacement
        CommandLog tCommandLog;
        if (verbosity == 0) {
            tCommandLog = new DummyCommandLog();
        } else {
            tCommandLog = new CommandLog(verbosity, fileMode, directory);
        }
        CommandLog oldCommandLog = commandLog;
        commandLog = tCommandLog;
        if (oldCommandLog != null) {
            oldCommandLog.shutdown();
        }
    }

    public static void initialize(FtpdSettings settings) throws IOException, ConfigurationException {
        initializeErrorLog(settings.get("/logging/errorlog"));
        initializeSecurityLog(settings.get("/logging/securitylog"));
        initializeEventLog(settings.get("/logging/eventlog"));
        // _todo: how do we check if it exists? (if we should load it?)
        // it's turned off if verbosity is set to 0
        recreateCommandLog(settings.getInt("/logging/command_log/verbosity"), settings.getInt("/logging/command_log/file_mode"), settings.get("/logging/command_log/directory"));

    }
}
