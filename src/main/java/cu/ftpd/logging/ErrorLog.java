/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

package cu.ftpd.logging;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2007-aug-30 : 21:28:56
 * @version $Id: ErrorLog.java 280 2008-11-24 18:52:18Z jevring $
 */
public class ErrorLog {
    protected static final DateFormat time = new SimpleDateFormat("EEE MMM d HH:mm:ss yyyy ", Locale.ENGLISH);
    private PrintWriter errorlog;
    private PrintWriter dlpw;


    public ErrorLog(String errorlog) throws IOException {
        try {
	        final File file = new File(errorlog);
	        file.getParentFile().mkdirs();
	        FileOutputStream stream = new FileOutputStream(file, true);
	        this.errorlog = new PrintWriter(stream);

            this.dlpw = new PrintWriter(stream) {
                @Override
                public void println(String x) {
                    log(x);
                }

                @Override
                public void println(Object x) {
                    log(String.valueOf(x));
                }
            };
        } catch (FileNotFoundException e) {
            throw new IOException("Could not open error log for writing", e);
        }
    }

    protected ErrorLog() {
    }

    public void reportError(String error) {
        log("ERROR: " + error);
    }

    public void reportCritical(String criticalError) {
        log("CRITICAL: " + criticalError);
        System.err.println("CRITICAL: " + criticalError);
    }

    private void log(String s) {
        errorlog.println(time.format(new Date()) + s);
        errorlog.flush();
    }

    public void shutdown() {
        errorlog.close();
    }

    public void reportException(String description, Exception e) {
        log("EXCEPTION: Thread=" + Thread.currentThread().getName()  + ":" + description);
        e.printStackTrace(dlpw);
    }
}
