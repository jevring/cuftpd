/**
 * Copyright (c) 2007, Markus Jevring <markus@jevring.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The names of the contributors may not be used to endorse or promote
 *    products derived from this software without specific prior written
 *    permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
package cu.shell;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * @author Markus Jevring <markus@jevring.net>
 * @since 2008-jan-27 - 23:09:43
 * @version $Id: Shell.java 280 2008-11-24 18:52:18Z jevring $
 */
public class Shell {
    public static ProcessResult execute(String... args) throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder(args);

        Process p = null;
        try {
            p = pb.start();
            int rc = p.waitFor();
            String out = getStdOut(p);
            return new ProcessResult(rc, out);
        } catch (IOException e) {
            throw new IOException("Something went wrong when executing: " + Arrays.asList(args) + " Reason: " + e.getMessage(), e);
        } catch (InterruptedException e) {
            throw new InterruptedException("Process was interrupted while executing " + Arrays.asList(args) + " Reason. " + e.getMessage());
        } finally {
            try {
                if (p != null) {
                    p.getInputStream().close();
                    p.getOutputStream().close();
                    p.getErrorStream().close();
                }
            } catch (IOException e) {
                // lib classes don't use stuff from the main app
                //Logging.getErrorLog().reportException("Faield to close native streams", e);
                e.printStackTrace();
            }
        }
    }

    private static String getStdOut(Process p) throws IOException {
        // This is the same as in ShellZipscript, but until we (if we ever) create a helper class for external processes, we'll just keep two copies of this rather simple function
        BufferedReader stdout = new BufferedReader(new InputStreamReader(p.getInputStream(), "ISO-8859-1"));
        StringBuilder sb = new StringBuilder(4096);
        char[] cs = new char[8192];
        int len;

        try {
            while((len = stdout.read(cs)) != -1) {
                // this way we don't have to do readline, since we'll split it up in later processing anyway
                // this is the fastest way to read, plus we're using the same buffer size as the BufferedReader
                sb.append(cs,0, len);
            }
        } finally {
            stdout.close();
        }
        return sb.toString();
    }
}
